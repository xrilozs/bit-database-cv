<?php
defined('BASEPATH') OR exit('No direct script access allowed');

//Import PHPMailer classes into the global namespace
//These must be at the top of your script, not inside a function
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;

//Load Composer's autoloader
require 'vendor/autoload.php';

class Send_mail{
    public function __construct() { 
        // parent::__construct(); 
    }

    function send($email, $subject, $message, $attachment=null, $filename=null, $sender_email=null){
        //Create an instance; passing `true` enables exceptions
        $mail = new PHPMailer(true);

        try {
            //Server settings
            // $mail->SMTPDebug  = SMTP::DEBUG_SERVER;            //Enable verbose debug output
            $mail->isSMTP();                                   //Send using SMTP
            $mail->Host       = SMTP_HOST;                     //Set the SMTP server to send through
            $mail->SMTPAuth   = true;                          //Enable SMTP authentication
            $mail->Username   = SMTP_USERNAME;                 //SMTP username
            $mail->Password   = SMTP_PASSWORD;                 //SMTP password
            $mail->SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS;//Enable implicit TLS encryption
            $mail->Port       = 587;                           //TCP port to connect to; use 587 if you have set `SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS`

            //Recipients
            $sender_name  = $sender_email ? "" : COMPANY_NAME;
            $sender_email = $sender_email ? $sender_email : COMPANY_NOREPLY_EMAIL;
            $mail->setFrom($sender_email, $sender_name);
            $mail->addAddress($email);                                              //Add a recipient
            // $mail->addAddress('ellen@example.com');                              //Name is optional
            // $mail->addReplyTo('info@example.com', 'Information');
            // $mail->addCC('cc@example.com');
            // $mail->addBCC('bcc@example.com');

            //Attachments
            if($attachment && $filename){
                $mail->addStringAttachment($attachment, $filename);
            }
            // $mail->addAttachment('/var/tmp/file.tar.gz');         //Add attachments
            // $mail->addAttachment('/tmp/image.jpg', 'new.jpg');    //Optional name

            //Content
            $mail->isHTML(true);                                     //Set email format to HTML
            $mail->Subject = $subject;
            $mail->Body    = $message;
            // $mail->AltBody = 'This is the body in plain text for non-HTML mail clients';

            // Send email
            if(!$mail->send()){
                logging("error", "Send mail error:".$mail->ErrorInfo);
                return 'Mailer Error: ' . $mail->ErrorInfo;
            }else{
                logging("debug", "Send mail success");
                return 'Message has been sent';
            }
        } catch (Exception $e) {
            logging("error", "Message could not be sent. Mailer Error:".$mail->ErrorInfo, $mail);
            return "Message could not be sent. Mailer Error: {$mail->ErrorInfo}";
        }
    }
}
?>
