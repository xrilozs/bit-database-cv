<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1>Self-Service</h1>
        </div>
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="<?=base_url('dashboard');?>">Dashboard</a></li>
            <li class="breadcrumb-item"><a href="<?=base_url('employee');?>">Karyawan</a></li>
            <li class="breadcrumb-item active">Self-Service</li>
          </ol>
        </div>
      </div>
    </div><!-- /.container-fluid -->
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-12">
          <div class="card">
            <div class="card-header">
                <div class="row">
                    <div class="col-6">
                        <i class="fas fa-user"></i>
                    </div>
                    <div class="col-6 text-right">
                        <button class="btn btn-primary disabled-button" id="print-cv-button" disabled>
                            <i class="nav-icon fas fa-file-word"></i>&nbsp;&nbsp;Cetak CV
                        </button>
                    </div>
                </div>
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col-12">
                        <form id="employee-form">
                            <div class="row">
                                <div class="col-lg-6 col-12">
                                    <div class="form-group">
                                        <label>Nama Lengkap</label>
                                        <input type="text" name="fullname" class="form-control" placeholder="Nama lengkap.." required>
                                    </div>
                                    <div class="form-group">
                                        <label>Tanggal Lahir</label>
                                        <input type="text" name="dob" class="form-control datepicker" id="employee-dob-field" placeholder="Tanggal Lahir.." required>
                                    </div>
                                    <div class="form-group">
                                        <label>Kota</label>
                                        <input type="text" name="city" class="form-control" placeholder="Kota.." required>
                                    </div>
                                    <div class="form-group">
                                        <label>Jenis Kelamin</label><br>
                                        <div class="custom-control custom-radio custom-control-inline">
                                            <input type="radio" id="gender-l" name="gender" class="custom-control-input" value="L">
                                            <label class="custom-control-label" for="gender-l">Laki-laki</label>
                                        </div>
                                        <div class="custom-control custom-radio custom-control-inline">
                                            <input type="radio" id="gender-p" name="gender" class="custom-control-input" value="P">
                                            <label class="custom-control-label" for="gender-p">Perempuan</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-6 col-12">
                                    <input 
                                        type="file" 
                                        id="employee-image"
                                        data-allowed-file-extensions="jpeg jpg png"
                                    >
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-12">
                                    <div class="form-group">
                                        <label>Alamat</label>
                                        <textarea name="address" class="form-control" style="resize:none;" rows="3" placeholder="Alamat.." required></textarea>
                                    </div>
                                </div>
                                <div class="col-6">
                                    <div class="form-group">
                                        <label>Telepon</label>
                                        <input type="text" name="phone" class="form-control" placeholder="Telepon.." required>
                                    </div>
                                </div>
                                <div class="col-6">
                                    <div class="form-group">
                                        <label>Email</label>
                                        <input type="text" name="email" class="form-control" placeholder="Email.." required>
                                    </div>
                                </div>
                                <div class="col-12">
                                    <div class="form-group">
                                        <label>Kewarganegaraan</label>
                                        <input type="text" name="nationality" class="form-control" placeholder="Kewarganegaraan.." required>
                                    </div>
                                </div>
                                <div class="col-12">
                                    <div class="form-group">
                                        <label>Edukasi</label>
                                        <input type="text" name="education" class="form-control" placeholder="Edukasi.." required>
                                    </div>
                                </div>
                                <div class="col-12">
                                    <button type="submit" class="btn btn-primary btn-block" id="employee-button">Simpan</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="row mt-5" id="education-section">
                    <div class="col-12">
                        <h2>Riwayat Pendidikan</h2>
                    </div>
                    <div class="col-12 text-right">
                        <button class="btn btn-primary disabled-button" id="education-create-toggle" data-toggle="modal" data-target="#education-create-modal" disabled>
                            <i class="fas fa-plus"></i> Tambah
                        </button>
                    </div>
                    <div class="col-12 mt-4" id="education-history-table">
                        <p class="font-weight-bold text-center bg-light py-5">
                            <i class="text-muted">Belum ada data</i>
                        </p>
                    </div>
                </div>
                <div class="row mt-5" id="training-section">
                    <div class="col-12">
                        <h2>Riwayat Pelatihan</h2>
                    </div>
                    <div class="col-12 text-right">
                        <button class="btn btn-primary disabled-button" id="training-create-toggle" data-toggle="modal" data-target="#training-create-modal" disabled>
                            <i class="fas fa-plus"></i> Tambah
                        </button>
                    </div>
                    <div class="col-12 mt-4" id="training-history-table">
                        <p class="font-weight-bold text-center bg-light py-5">
                            <i class="text-muted">Belum ada data</i>
                        </p>
                    </div>
                </div>
                <div class="row mt-5" id="working-section">
                    <div class="col-12">
                        <h2>Riwayat Pekerjaan</h2>
                    </div>
                    <div class="col-12 text-right">
                        <button class="btn btn-primary disabled-button" id="working-create-toggle" data-toggle="modal" data-target="#working-create-modal" disabled>
                            <i class="fas fa-plus"></i> Tambah
                        </button>
                    </div>
                    <div class="col-12 mt-4" id="working-history-table">
                        <p class="font-weight-bold text-center bg-light py-5">
                            <i class="text-muted">Belum ada data</i>
                        </p>
                    </div>
                </div>
                <div class="row mt-5" id="project-section">
                    <div class="col-12">
                        <h2>Riwayat Projek</h2>
                    </div>
                    <div class="col-12 text-right">
                        <button class="btn btn-primary disabled-button" id="project-create-toggle" data-toggle="modal" data-target="#project-create-modal" disabled>
                            <i class="fas fa-plus"></i> Tambah
                        </button>
                    </div>
                    <div class="col-12 mt-4" id="project-history-table">
                        <p class="font-weight-bold text-center bg-light py-5">
                            <i class="text-muted">Belum ada data</i>
                        </p>
                    </div>
                </div>
                <div class="row mt-5" id="certificate-section">
                    <div class="col-12">
                        <h2>Sertifikat Kompetensi</h2>
                    </div>
                    <div class="col-12 mt-4">
                        <input 
                            type="file" 
                            id="certificate-file"
                        >
                    </div>
                    <div class="col-12 mt-4">
                        <div class="row" id="certiciate-list">
                        </div>
                    </div>
                </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
</div>