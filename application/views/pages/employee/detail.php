<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1>Karyawan</h1>
        </div>
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="<?=base_url('dashboard');?>">Dashboard</a></li>
            <li class="breadcrumb-item"><a href="<?=base_url('employee');?>">Karyawan</a></li>
            <li class="breadcrumb-item active">Detail</li>
          </ol>
        </div>
      </div>
    </div><!-- /.container-fluid -->
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-12">
          <div class="card">
            <div class="card-header">
                <div class="row">
                    <div class="col-6">
                        <a href="<?=base_url('employee');?>">Kembali</a>
                    </div>
                    <div class="col-6 text-right">
                        <button type="button" class="btn btn-primary print-cv-button">
                            <i class="nav-icon fas fa-file-word"></i>&nbsp;&nbsp;Cetak CV
                        </button>
                    </div>
                </div>
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col-12">
                        <form id="employee-form">
                            <div class="row">
                                <div class="col-lg-6 col-12">
                                    <div class="form-group">
                                        <label>Nama Lengkap</label>
                                        <input type="text" name="fullname" class="form-control" readonly>
                                    </div>
                                    <div class="form-group">
                                        <label>Tanggal Lahir</label>
                                        <input type="text" name="dob" class="form-control" readonly>
                                    </div>
                                    <div class="form-group">
                                        <label>Kota</label>
                                        <input type="text" name="city" class="form-control" readonly>
                                    </div>
                                    <div class="form-group">
                                        <label>Jenis Kelamin</label>
                                        <input type="text" name="gender" class="form-control" readonly>
                                    </div>
                                </div>
                                <div class="col-lg-6 col-12">
                                    <img src="https://placehold.co/500x350?text=Profil" class="img-responsive" id="employee-image" style="width:100% !important; height:350px; object-fit:cover;">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-12">
                                    <div class="form-group">
                                        <label>Alamat</label>
                                        <textarea name="address" class="form-control" style="resize:none;" rows="3" readonly></textarea>
                                    </div>
                                </div>
                                <div class="col-6">
                                    <div class="form-group">
                                        <label>Telepon</label>
                                        <input type="text" name="phone" class="form-control" readonly>
                                    </div>
                                </div>
                                <div class="col-6">
                                    <div class="form-group">
                                        <label>Email</label>
                                        <input type="text" name="email" class="form-control" readonly>
                                    </div>
                                </div>
                                <div class="col-12">
                                    <div class="form-group">
                                        <label>Kewarganegaraan</label>
                                        <input type="text" name="nationality" class="form-control" readonly>
                                    </div>
                                </div>
                                <div class="col-12">
                                    <div class="form-group">
                                        <label>Edukasi</label>
                                        <input type="text" name="education" class="form-control" readonly>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="row mt-5" id="education-section">
                    <div class="col-12">
                        <h2>Riwayat Pendidikan</h2>
                    </div>
                    <div class="col-12 text-right">
                        <button class="btn btn-primary" id="education-create-toggle" data-toggle="modal" data-target="#education-create-modal">
                            <i class="fas fa-plus"></i> Tambah
                        </button>
                    </div>
                    <div class="col-12 mt-4" id="education-history-table">
                        <p class="font-weight-bold text-center bg-light py-5">
                            <i class="text-muted">Belum ada data</i>
                        </p>
                    </div>
                </div>
                <div class="row mt-5" id="training-section">
                    <div class="col-12">
                        <h2>Riwayat Pelatihan</h2>
                    </div>
                    <div class="col-12 text-right">
                        <button class="btn btn-primary" id="training-create-toggle" data-toggle="modal" data-target="#training-create-modal">
                            <i class="fas fa-plus"></i> Tambah
                        </button>
                    </div>
                    <div class="col-12 mt-4" id="training-history-table">
                        <p class="font-weight-bold text-center bg-light py-5">
                            <i class="text-muted">Belum ada data</i>
                        </p>
                    </div>
                </div>
                <div class="row mt-5" id="working-section">
                    <div class="col-12">
                        <h2>Riwayat Pekerjaan</h2>
                    </div>
                    <div class="col-12 text-right">
                        <button class="btn btn-primary" id="working-create-toggle" data-toggle="modal" data-target="#working-create-modal">
                            <i class="fas fa-plus"></i> Tambah
                        </button>
                    </div>
                    <div class="col-12 mt-4" id="working-history-table">
                        <p class="font-weight-bold text-center bg-light py-5">
                            <i class="text-muted">Belum ada data</i>
                        </p>
                    </div>
                </div>
                <div class="row mt-5" id="project-section">
                    <div class="col-12">
                        <h2>Riwayat Projek</h2>
                    </div>
                    <div class="col-12 text-right">
                        <button class="btn btn-primary" id="project-create-toggle" data-toggle="modal" data-target="#project-create-modal">
                            <i class="fas fa-plus"></i> Tambah
                        </button>
                    </div>
                    <div class="col-12 mt-4" id="project-history-table">
                        <p class="font-weight-bold text-center bg-light py-5">
                            <i class="text-muted">Belum ada data</i>
                        </p>
                    </div>
                </div>
                <div class="row mt-5" id="certificate-section">
                    <div class="col-12">
                        <h2>Sertifikat Kompetensi</h2>
                    </div>
                    <div class="col-12 mt-4">
                        <input 
                            type="file" 
                            id="certificate-file"
                        >
                    </div>
                    <div class="col-12 mt-4">
                        <div class="row" id="certiciate-list">
                        </div>
                    </div>
                </div>
            </div>
            <div class="overlay" id="employee-detail-overlay">
                <i class="fas fa-2x fa-sync fa-spin"></i>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
</div>