<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1>Karyawan</h1>
        </div>
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="<?=base_url('dashboard');?>">Dashboard</a></li>
            <li class="breadcrumb-item active">Karyawan</li>
          </ol>
        </div>
      </div>
    </div><!-- /.container-fluid -->
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-12">
          <div class="card">
            <div class="card-body">
              <div class="row d-flex justify-content-end mb-3">
                <button type="button" class="btn btn-primary" id="employee-create-toggle" data-toggle="modal" data-target="#employee-create-modal">
                  <i class="fas fa-plus"></i> Tambah
                </button>
              </div>
              <div class="table-responsive">
                <table class="table table-bordered table-hover" id="employee-datatable">
                  <thead>
                    <tr>
                      <th>Gambar</th>
                      <th>Nama Lengkap</th>
                      <th>Tanggal Lahir</th>
                      <th>Kota</th>
                      <th>Jenis Kelamin</th>
                      <th>Alamat</th>
                      <th>Telepon</th>
                      <th>Email</th>
                      <th>Negara</th>
                      <th>Pendidikan</th>
                      <th>Tanggal Dibuat</th>
                      <th>Aksi</th>
                    </tr>
                  </thead>
                  <tbody>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
</div>

<!-- Modal -->
<div class="modal fade" id="employee-create-modal" data-backdrop="static" data-keyboard="false" tabindex="-1" style="overflow-y: auto !important;">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Tambah Karyawan</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form id="employee-create-form">
        <div class="form-group">
          <label>Gambar</label>
          <input 
            type="file" 
            class="dropify"
            id="employee-create-image"
            data-allowed-file-extensions="jpeg jpg png"
          >
        </div>
        <div class="form-group">
          <label for="employee-fullname-create-field">Nama Lengkap:</label>
          <input type="text" name="fullname" class="form-control" id="employee-fullname-create-field" placeholder="Nama Lengkap.." required>
        </div>
        <div class="form-group">
          <label for="employee-dob-create-field">Tanggal Lahir:</label>
          <input type="text" name="dob" class="form-control datepicker" id="employee-dob-create-field" placeholder="Tanggal Lahir.." required>
        </div>
        <div class="form-group">
          <label for="employee-city-create-field">Kota:</label>
          <input type="text" name="city" class="form-control" id="employee-city-create-field" placeholder="Kota.." required>
        </div>
        <div class="form-group">
          <label for="employee-city-create-field">Jenis Kelamin:</label><br>
          <div class="custom-control custom-radio custom-control-inline">
            <input type="radio" id="gender-create-l" name="gender" class="custom-control-input" value="L">
            <label class="custom-control-label" for="gender-create-l">Laki-laki</label>
          </div>
          <div class="custom-control custom-radio custom-control-inline">
            <input type="radio" id="gender-create-p" name="gender" class="custom-control-input" value="P">
            <label class="custom-control-label" for="gender-create-p">Perempuan</label>
          </div>
          <!-- <input type="radio" name="gender" class="form-control" id="employee-gender-create-field" value="L" required> Laki-laki
          <input type="radio" name="gender" class="form-control" id="employee-gender-create-field" value="P" required> Perempuan -->
        </div>
        <div class="form-group">
          <label for="employee-address-create-field">Alamat:</label>
          <textarea name="address" class="form-control" id="employee-address-create-field" placeholder="Alamat.." required></textarea>
        </div>
        <div class="form-group">
          <label for="employee-phone-create-field">Telepon:</label>
          <input type="text" name="phone" class="form-control" id="employee-phone-create-field" placeholder="Telepon.." required>
        </div>
        <div class="form-group">
          <label for="employee-email-create-field">Email:</label>
          <input type="text" name="email" class="form-control" id="employee-email-create-field" placeholder="Email.." required>
        </div>
        <div class="form-group">
          <label for="employee-nationality-create-field">Kewarganegaraan:</label>
          <input type="text" name="nationality" class="form-control" id="employee-nationality-create-field" placeholder="Kewarganegaraan.." required>
        </div>
        <div class="form-group">
          <label for="employee-education-create-field">Pendidikan:</label>
          <input type="text" name="education" class="form-control" id="employee-education-create-field" placeholder="Pendidikan.." required>
        </div>
      </div>
      <div class="modal-footer justify-content-between">
        <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
        <button type="submit" class="btn btn-primary" id="employee-create-button">Simpan</button>
        </form>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="employee-update-modal" data-backdrop="static" data-keyboard="false" tabindex="-1" style="overflow-y: auto !important;">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="overlay" id="employee-update-overlay">
          <i class="fas fa-2x fa-sync fa-spin"></i>
      </div>
      <div class="modal-header">
        <h4 class="modal-title">Ubah Karyawan</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form id="employee-update-form">
        <div class="form-group">
          <label>Gambar</label>
          <input 
            type="file" 
            class="dropify"
            id="employee-update-image"
            data-allowed-file-extensions="jpeg jpg png"
          >
        </div>
        <div class="form-group">
          <label for="employee-fullname-update-field">Nama Lengkap:</label>
          <input type="text" name="fullname" class="form-control" id="employee-fullname-update-field" placeholder="Nama Lengkap.." required>
        </div>
        <div class="form-group">
          <label for="employee-dob-update-field">Tanggal Lahir:</label>
          <input type="text" name="dob" class="form-control datepicker" id="employee-dob-update-field" placeholder="Tanggal Lahir.." required>
        </div>
        <div class="form-group">
          <label for="employee-city-update-field">Kota:</label>
          <input type="text" name="city" class="form-control" id="employee-city-update-field" placeholder="Kota.." required>
        </div>
        <div class="form-group">
          <label for="employee-city-update-field">Jenis Kelamin:</label><br>
          <div class="custom-control custom-radio custom-control-inline">
            <input type="radio" id="gender-update-l" name="gender" class="custom-control-input" value="L">
            <label class="custom-control-label" for="gender-update-l">Laki-laki</label>
          </div>
          <div class="custom-control custom-radio custom-control-inline">
            <input type="radio" id="gender-update-p" name="gender" class="custom-control-input" value="P">
            <label class="custom-control-label" for="gender-update-p">Perempuan</label>
          </div>
        </div>
        <div class="form-group">
          <label for="employee-address-update-field">Alamat:</label>
          <textarea name="address" class="form-control" id="employee-address-update-field" placeholder="Alamat.." required></textarea>
        </div>
        <div class="form-group">
          <label for="employee-phone-update-field">Telepon:</label>
          <input type="text" name="phone" class="form-control" id="employee-phone-update-field" placeholder="Telepon.." required>
        </div>
        <div class="form-group">
          <label for="employee-email-update-field">Email:</label>
          <input type="text" name="email" class="form-control" id="employee-email-update-field" placeholder="Email.." required>
        </div>
        <div class="form-group">
          <label for="employee-nationality-update-field">Kewarganegaraan:</label>
          <input type="text" name="nationality" class="form-control" id="employee-nationality-update-field" placeholder="Kewarganegaraan.." required>
        </div>
        <div class="form-group">
          <label for="employee-education-update-field">Pendidikan:</label>
          <input type="text" name="education" class="form-control" id="employee-education-update-field" placeholder="Pendidikan.." required>
        </div>
      </div>
      <div class="modal-footer justify-content-between">
        <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
        <button type="submit" class="btn btn-primary" id="employee-update-button">Simpan</button>
        </form>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="employee-delete-modal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Hapus Karyawan</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        Apakah Anda yakin ingin menghapus karyawan tersebut?
      </div>
      <div class="modal-footer justify-content-between">
        <button type="button" class="btn btn-default" data-dismiss="modal">Tidak</button>
        <button type="button" class="btn btn-danger" id="employee-delete-button">Ya</button>
      </div>
    </div>
  </div>
</div>

