<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1>Pencarian Karyawan</h1>
        </div>
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="<?=base_url('dashboard');?>">Dashboard</a></li>
            <li class="breadcrumb-item"><a href="<?=base_url('employee');?>">Karyawan</a></li>
            <li class="breadcrumb-item active">Pencarian</li>
          </ol>
        </div>
      </div>
    </div><!-- /.container-fluid -->
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-12 d-flex justify-content-center">
          <div class="card" style="width:50%">
            <div class="card-body">
                <div class="row">
                    <div class="col-12 d-flex justify-content-center">
                        <form class="form w-100" id="search-form">
                            <div class="input-group">
                                <div class="custom-file">
                                    <select class="select2" id="training-option" multiple="multiple" data-placeholder="Select a State" data-dropdown-css-class="select2-blue" style="width: 100%;" required>
                                      <option readonly>--Pilih Tipe Pelatihan--</option>
                                    </select>
                                </div>
                                <div class="input-group-append">
                                    <button class="btn btn-primary" type="submit" id="search-button" disabled>
                                        <i class="fas fa-search"></i> Cari
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
          </div>
        </div>
      </div>
      <div class="row mt-5" id="employee-table" style="display:none;">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-bordered table-hover" id="employee-datatable">
                        <thead>
                            <tr>
                            <th>Gambar</th>
                            <th>Nama Lengkap</th>
                            <th>Tahun Kerja</th>
                            <th>Tanggal Lahir</th>
                            <th>Kota</th>
                            <th>Jenis Kelamin</th>
                            <th>Alamat</th>
                            <th>Telepon</th>
                            <th>Email</th>
                            <th>Negara</th>
                            <th>Pendidikan</th>
                            <th>Tanggal Dibuat</th>
                            <th>Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
      </div>
    </div>
  </section>
</div>
