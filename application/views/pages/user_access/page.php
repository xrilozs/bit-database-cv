<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1>User Access</h1>
        </div>
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="<?=base_url('dashboard');?>">Dashboard</a></li>
            <li class="breadcrumb-item active">User Access</li>
          </ol>
        </div>
      </div>
    </div><!-- /.container-fluid -->
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-12">
          <div class="card">
            <div class="card-body">
              <div class="row d-flex justify-content-end mb-3">
                <button type="button" class="btn btn-primary" id="user-access-create-toggle" data-toggle="modal" data-target="#user-access-create-modal">
                  <i class="fas fa-plus"></i> Tambah
                </button>
              </div>
              <div class="table-responsive">
                <table class="table table-bordered table-hover" id="user-access-datatable">
                  <thead>
                    <tr>
                      <th>Role</th>
                      <th>Modul User</th>
                      <th>Modul Karyawan</th>
                      <th>Modul Pelatihan</th>
                      <th>Modul Pencarian</th>
                      <th>Modul Self-service</th>
                      <th>Modul Pengaturan</th>
                      <th>Tanggal Dibuat</th>
                      <th>Aksi</th>
                    </tr>
                  </thead>
                  <tbody>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
</div>

<!-- Modal -->
<div class="modal fade" id="user-access-create-modal" data-backdrop="static" data-keyboard="false" tabindex="-1" >
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Tambah User Access</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form id="user-access-create-form">
        <div class="row">
          <div class="col-12">
            <div class="form-group">
              <label for="user-access-role-create-field">Role:</label>
              <select name="role_id" class="form-control role-option" id="user-access-role-create-field" required></select>
            </div>
          </div>
          <div class="col-4">
            <div class="form-group">
              <label for="user-access-user-module-create-field">Modul User</label>
              <input type="checkbox" name="user_module" id="user-access-user-module-create-field" class="form-control">
            </div>
          </div>
          <div class="col-4">
            <div class="form-group">
              <label for="user-access-employee-module-create-field">Modul Karyawan</label>
              <input type="checkbox" name="employee_module" id="user-access-employee-module-create-field" class="form-control">
            </div>
          </div>
          <div class="col-4">
            <div class="form-group">
              <label for="user-access-training-module-create-field">Modul Pelatihan</label>
              <input type="checkbox" name="training_module" id="user-access-training-module-create-field" class="form-control">
            </div>
          </div>
          <div class="col-4">
            <div class="form-group">
              <label for="user-access-search-module-create-field">Modul Pencarian</label>
              <input type="checkbox" name="search_module" id="user-access-search-module-create-field" class="form-control">
            </div>
          </div>
          <div class="col-4">
            <div class="form-group">
              <label for="user-access-self-module-create-field">Modul Self-service</label>
              <input type="checkbox" name="self_module" id="user-access-self-module-create-field" class="form-control">
            </div>
          </div>
          <div class="col-4">
            <div class="form-group">
              <label for="user-access-config-module-create-field">Modul Pengaturan</label>
              <input type="checkbox" name="config_module" id="user-access-config-module-create-field" class="form-control">
            </div>
          </div>
        </div>
      </div>
      <div class="modal-footer justify-content-between">
        <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
        <button type="submit" class="btn btn-primary" id="user-access-create-button">Simpan</button>
        </form>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="user-access-update-modal" data-backdrop="static" data-keyboard="false" tabindex="-1" >
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="overlay" id="user-access-update-overlay">
          <i class="fas fa-2x fa-sync fa-spin"></i>
      </div>
      <div class="modal-header">
        <h4 class="modal-title">Update User Access</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form id="user-access-update-form">
        <div class="row">
          <div class="col-12">
            <div class="form-group">
              <label for="user-access-role-update-field">Role:</label>
              <select name="role_id" class="form-control role-option" id="user-access-role-update-field" required></select>
            </div>
          </div>
          <div class="col-4">
            <div class="form-group">
              <label for="user-access-user-module-update-field">Modul User</label>
              <input type="checkbox" name="user_module" id="user-access-user-module-update-field" class="form-control">
            </div>
          </div>
          <div class="col-4">
            <div class="form-group">
              <label for="user-access-employee-module-update-field">Modul Karyawan</label>
              <input type="checkbox" name="employee_module" id="user-access-employee-module-update-field" class="form-control">
            </div>
          </div>
          <div class="col-4">
            <div class="form-group">
              <label for="user-access-training-module-update-field">Modul Pelatihan</label>
              <input type="checkbox" name="training_module" id="user-access-training-module-update-field" class="form-control">
            </div>
          </div>
          <div class="col-4">
            <div class="form-group">
              <label for="user-access-search-module-update-field">Modul Pencarian</label>
              <input type="checkbox" name="search_module" id="user-access-search-module-update-field" class="form-control">
            </div>
          </div>
          <div class="col-4">
            <div class="form-group">
              <label for="user-access-self-module-update-field">Modul Self-service</label>
              <input type="checkbox" name="self_module" id="user-access-self-module-update-field" class="form-control">
            </div>
          </div>
          <div class="col-4">
            <div class="form-group">
              <label for="user-access-config-module-update-field">Modul Pengaturan</label>
              <input type="checkbox" name="config_module" id="user-access-config-module-update-field" class="form-control">
            </div>
          </div>
        </div>
      </div>
      <div class="modal-footer justify-content-between">
        <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
        <button type="submit" class="btn btn-success" id="user-access-update-button">Simpan</button>
        </form>
      </div>
    </div>
  </div>
</div>
