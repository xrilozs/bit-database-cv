<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1>User</h1>
        </div>
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="<?=base_url('dashboard');?>">Dashboard</a></li>
            <li class="breadcrumb-item active">User</li>
          </ol>
        </div>
      </div>
    </div><!-- /.container-fluid -->
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="container-fluid">
      <div class="row">
        <div class="col-12">
          <div class="card">
            <div class="card-body">
              <div class="row d-flex justify-content-end mb-3">
                <button type="button" class="btn btn-primary" id="user-create-toggle" data-toggle="modal" data-target="#user-create-modal">
                  <i class="fas fa-plus"></i> Tambah
                </button>
              </div>
              <div class="table-responsive">
                <table class="table table-bordered table-hover" id="user-datatable">
                  <thead>
                    <tr>
                      <th>Nama Lengkap</th>
                      <th>Username</th>
                      <th>Role</th>
                      <th>Status</th>
                      <th>Tanggal Dibuat</th>
                      <th>Aksi</th>
                    </tr>
                  </thead>
                  <tbody>
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
</div>

<!-- Modal -->
<div class="modal fade" id="user-create-modal" data-backdrop="static" data-keyboard="false" tabindex="-1" >
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Tambah User</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form id="user-create-form" autocomplete="off">          
        <div class="form-group">
          <label for="user-fullname-create-field">Nama Lengkap:</label>
          <input type="text" name="fullname" class="form-control" id="user-fullname-create-field" placeholder="fullname.." required>
        </div>
          <div class="form-group">
          <label for="user-username-create-field">Username:</label>
          <input type="text" name="username" class="form-control" id="user-username-create-field" placeholder="username.." autocomplete="off" required>
        </div>
        <div class="form-group">
          <label for="user-role-create-field">Role:</label>
          <select name="role_id" class="form-control role-option" id="user-role-create-field" required></select>
        </div>
        <div class="form-group">
          <label for="user-password-create-field">Password:</label>
          <input type="password" name="password" class="form-control" id="user-password-create-field" placeholder="password.." autocomplete="new-password" required>
        </div>
      </div>
      <div class="modal-footer justify-content-between">
        <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
        <button type="submit" class="btn btn-primary" id="user-create-button">Simpan</button>
        </form>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="user-update-modal" data-backdrop="static" data-keyboard="false" tabindex="-1" >
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="overlay" id="user-update-overlay">
          <i class="fas fa-2x fa-sync fa-spin"></i>
      </div>
      <div class="modal-header">
        <h4 class="modal-title">Update User</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form id="user-update-form">          
        <div class="form-group">
          <label for="user-fullname-update-field">Nama Lengkap:</label>
          <input type="text" name="fullname" class="form-control" id="user-fullname-update-field" placeholder="fullname.." required>
        </div>
          <div class="form-group">
          <label for="user-username-update-field">Username:</label>
          <input type="text" name="username" class="form-control" id="user-username-update-field" placeholder="username.." required>
        </div>
        <div class="form-group">
          <label for="user-role-update-field">Role:</label>
          <select name="role_id" class="form-control role-option" id="user-role-update-field" required></select>
        </div>
        <div class="form-group">
          <label for="user-status-update-field">Status:</label>
          <span id="user-status-update-field"></span>
        </div>
      </div>
      <div class="modal-footer justify-content-between">
        <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
        <button type="submit" class="btn btn-success" id="user-update-button">Simpan</button>
        </form>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="user-inactive-modal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Nonaktifasi User</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        Apakah Anda yakin ingin menonaktifkan User tersebut?
      </div>
      <div class="modal-footer justify-content-between">
        <button type="button" class="btn btn-default" data-dismiss="modal">Tidak</button>
        <button type="button" class="btn btn-danger" id="user-inactive-button">Ya</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="user-active-modal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Aktifasi User</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        Apakah Anda yakin ingin mengaktifkan User tersebut?
      </div>
      <div class="modal-footer justify-content-between">
        <button type="button" class="btn btn-default" data-dismiss="modal">Tidak</button>
        <button type="button" class="btn btn-info" id="user-active-button">Ya</button>
      </div>
    </div>
  </div>
</div>
