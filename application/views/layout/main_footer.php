<footer class="main-footer" style="padding:12px; text-align:center;">
    BIT Database CV
    <p class="mb-0" style="font-size:9px;">
      <strong>Copyright &copy; 2014-2020 AdminLTE.io. All rights reserved.</strong>
    </p>
</footer>
<!-- /.control-sidebar -->

  <!-- Common Modal -->
  <div class="modal fade" id="change-password-modal" data-backdrop="static" data-keyboard="false" tabindex="-1" style="overflow-y: auto !important;">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">Ganti Password</h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <form id="change-password-form">
          <div class="form-group">
            <label>Password Baru:</label>
            <input type="password" name="new-password" class="form-control" required>
          </div>
          <div class="form-group">
            <label>Konfirmasi Password:</label>
            <input type="password" name="confirm-password" class="form-control" required>
          </div>
        </div>
        <div class="modal-footer justify-content-between">
          <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
          <button type="submit" class="btn btn-primary" id="change-password-button">Simpan</button>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- ./wrapper -->

<!-- jQuery -->
<script src="<?=base_url('assets/vendor/adminlte/plugins/jquery/jquery.min.js');?>"></script>
<!-- jQuery UI 1.11.4 -->
<script src="<?=base_url('assets/vendor/adminlte/plugins/jquery-ui/jquery-ui.min.js');?>"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button)
</script>
<!-- Bootstrap 4 -->
<script src="<?=base_url('assets/vendor/adminlte/plugins/bootstrap/js/bootstrap.bundle.min.js');?>"></script>
<!-- ChartJS -->
<script src="<?=base_url('assets/vendor/adminlte/plugins/chart.js/Chart.min.js');?>"></script>
<!-- DataTables  & Plugins -->
<script src="<?=base_url('assets/vendor/adminlte/plugins/datatables/jquery.dataTables.min.js');?>"></script>
<script src="<?=base_url('assets/vendor/adminlte/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js');?>"></script>
<script src="<?=base_url('assets/vendor/adminlte/plugins/datatables-responsive/js/dataTables.responsive.min.js');?>"></script>
<script src="<?=base_url('assets/vendor/adminlte/plugins/datatables-responsive/js/responsive.bootstrap4.min.js');?>"></script>
<script src="<?=base_url('assets/vendor/adminlte/plugins/datatables-buttons/js/dataTables.buttons.min.js');?>"></script>
<script src="<?=base_url('assets/vendor/adminlte/plugins/datatables-buttons/js/buttons.bootstrap4.min.js');?>"></script>
<script src="<?=base_url('assets/vendor/adminlte/plugins/jszip/jszip.min.js');?>"></script>
<script src="<?=base_url('assets/vendor/adminlte/plugins/pdfmake/pdfmake.min.js');?>"></script>
<script src="<?=base_url('assets/vendor/adminlte/plugins/pdfmake/vfs_fonts.js');?>"></script>
<script src="<?=base_url('assets/vendor/adminlte/plugins/datatables-buttons/js/buttons.html5.min.js');?>"></script>
<script src="<?=base_url('assets/vendor/adminlte/plugins/datatables-buttons/js/buttons.print.min.js');?>"></script>
<script src="<?=base_url('assets/vendor/adminlte/plugins/datatables-buttons/js/buttons.colVis.min.js');?>"></script>
<script src="<?=base_url('assets/vendor/adminlte/plugins/moment/moment.min.js');?>"></script>
<script src="<?=base_url('assets/vendor/adminlte/plugins/daterangepicker/daterangepicker.js');?>"></script>
<!-- datepicker -->
<script src="<?=base_url('assets/vendor/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js');?>"></script>
<!-- Tempusdominus Bootstrap 4 -->
<script src="<?=base_url('assets/vendor/adminlte/plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js');?>"></script>
<!-- Summernote -->
<script src="<?=base_url('assets/vendor/adminlte/plugins/summernote/summernote-bs4.min.js');?>"></script>
<!-- overlayScrollbars -->
<script src="<?=base_url('assets/vendor/adminlte/plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js');?>"></script>
<!-- AdminLTE App -->
<script src="<?=base_url('assets/vendor/adminlte/dist/js/adminlte.js');?>"></script>
<!-- Select2 -->
<script src="<?=base_url('assets/vendor/adminlte/plugins/select2/js/select2.full.min.js');?>"></script>
<!-- SweetAlert2 -->
<script src="<?=base_url('assets/vendor/adminlte/plugins/sweetalert2/sweetalert2.min.js');?>"></script>
<!-- Dropify -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/Dropify/0.2.2/js/dropify.min.js" integrity="sha512-8QFTrG0oeOiyWo/VM9Y8kgxdlCryqhIxVeRpWSezdRRAvarxVtwLnGroJgnVW9/XBRduxO/z1GblzPrMQoeuew==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<!-- Tagify -->
<script src="https://cdn.jsdelivr.net/npm/@yaireo/tagify"></script>
<script src="https://cdn.jsdelivr.net/npm/@yaireo/tagify/dist/tagify.polyfills.min.js"></script>
<!-- CodeMirror -->
<script src="<?=base_url('assets/vendor/adminlte/plugins/codemirror/codemirror.js');?>"></script>
<script src="<?=base_url('assets/vendor/adminlte/plugins/codemirror/mode/css/css.js');?>"></script>
<script src="<?=base_url('assets/vendor/adminlte/plugins/codemirror/mode/xml/xml.js');?>"></script>
<script src="<?=base_url('assets/vendor/adminlte/plugins/codemirror/mode/htmlmixed/htmlmixed.js');?>"></script>
<!-- Custom JS -->
<script>
    const WEB_URL = "<?=base_url()?>"
    const API_URL = "<?=base_url('api')?>"
</script>
<script src="<?=base_url('assets/js/common.js');?>"></script>
<?php
  if(isset($js_file)){
    foreach ($js_file as $file) {
        echo "<script src='".$file."'></script>";
    }
  }
?>
</body>
</html>
