  <!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-light-primary elevation-1">
    <!-- Brand Logo -->
    <a href="<?=base_url('dashboard');?>" class="brand-link">
      <img src="<?=base_url('assets/img/default-logo.png');?>" alt="Logo" class="brand-image" style="opacity: .8">
      <span class="font-weight-bold" style="font-size:12px">BIT Database CV</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <li class="nav-item" id="sidebar-dashboard-menu">
            <a href="<?=base_url('dashboard');?>" class="nav-link <?=$title == 'Dashboard' ? 'active' : '';?>">
              <i class="nav-icon fas fa-tachometer-alt"></i>
              <p>
                Dashboard
              </p>
            </a>
          </li>
          <li id="user-module-menu" class="nav-item <?=$title == 'User' || $title == 'User Role' ? 'menu-is-opening menu-open' : '';?>">
            <a href="#" class="nav-link">
              <i class="nav-icon fas fa-users"></i>
              <p>
                User
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item" id="sidebar-user-menu">
                <a href="<?=base_url('user');?>" class="nav-link <?=$title == 'User' ? 'active' : '';?>">
                  <i class="nav-icon fas fa-users-cog"></i>
                  <p>
                    User
                  </p>
                </a>
              </li>
              <li class="nav-item" id="sidebar-user-role-menu">
                <a href="<?=base_url('user-role');?>" class="nav-link <?=$title == 'User Role' ? 'active' : '';?>">
                  <i class="nav-icon fas fa-user-tag"></i>
                  <p>Role</p>
                </a>
              </li>
              <li class="nav-item" id="sidebar-user-access-menu">
                <a href="<?=base_url('user-access');?>" class="nav-link <?=$title == 'User Access' ? 'active' : '';?>">
                  <i class="nav-icon fas fa-address-card"></i>
                  <p>Akses</p>
                </a>
              </li>
            </ul>
          </li>
          <li id="employee-module-menu" class="nav-item">
            <a href="<?=base_url('employee');?>" class="nav-link <?=$title == 'Karyawan' ? 'active' : '';?>">
              <i class="nav-icon fas fa-file-word"></i>
              <p>
                Karyawan
              </p>
            </a>
          </li>
          <li class="nav-item" id="training-module-menu">
            <a href="<?=base_url('training-type');?>" class="nav-link <?=$title == 'Jenis Pelatihan' ? 'active' : '';?>">
              <i class="nav-icon fas fa-user-tag"></i>
              <p>
                Jenis Pelatihan
              </p>
            </a>
          </li>
          <li class="nav-item" id="search-module-menu">
            <a href="<?=base_url('advance-search');?>" class="nav-link <?=$title == 'Pencarian Karyawan' ? 'active' : '';?>">
              <i class="nav-icon fas fa-search"></i>
              <p>
                Pencarian
              </p>
            </a>
          </li>
          <li class="nav-item" id="self-module-menu">
            <a href="<?=base_url('self-service');?>" class="nav-link <?=$title == 'Self Service' ? 'active' : '';?>">
              <i class="nav-icon fas fa-edit"></i>
              <p>
                Self-service
              </p>
            </a>
          </li>
          <li class="nav-item" id="config-module-menu">
            <a href="<?=base_url('config');?>" class="nav-link <?=$title == 'Pengaturan' ? 'active' : '';?>">
              <i class="nav-icon fas fa-cog"></i>
              <p>
                Pengaturan
              </p>
            </a>
          </li>
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>
