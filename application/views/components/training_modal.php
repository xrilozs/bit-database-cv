<div class="modal fade" id="training-create-modal" data-backdrop="static" data-keyboard="false" tabindex="-1" style="overflow-y: auto !important;">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Tambah Pelatihan</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form id="training-create-form">
        <div class="form-group">
          <label for="training-title-create-field">Nama Pelatihan:</label>
          <input type="text" name="title" class="form-control" id="training-title-create-field" placeholder="Nama Pelatihan.." required>
        </div>
        <div class="form-group">
          <label for="training-type-create-field">Jenis:</label>
          <select name="type" class="form-control type-option" id="training-type-create-field" required>
            <option disabled>--Pilih Jenis Pelatihan--</option>
          </select><br>
          <input type="text" name="type-other" class="form-control type-text" placeholder="Jenis Lainnya.." style="display:none;">
        </div>
        <div class="form-group">
          <label for="training-institution-create-field">Institusi:</label>
          <input type="text" name="institution" class="form-control" id="training-institution-create-field" placeholder="Institusi.." required>
        </div>
        <div class="form-group">
          <label for="training-training-date-create-field">Tanggal Pelatihan:</label>
          <input type="text" name="training_date" class="form-control datepicker" id="training-training-date-create-field" placeholder="Tanggal Pelatihan.." required>
        </div>
        <div class="form-group">
          <label for="training-expired-date-create-field">Tanggal Expired:</label>
          <input type="text" name="expired_date" class="form-control datepicker" id="training-expired-date-create-field" placeholder="Tanggal Expired.." required>
        </div>
      </div>
      <div class="modal-footer justify-content-between">
        <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
        <button type="submit" class="btn btn-primary" id="training-create-button">Simpan</button>
        </form>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="training-update-modal" data-backdrop="static" data-keyboard="false" tabindex="-1" style="overflow-y: auto !important;">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="overlay" id="training-update-overlay">
          <i class="fas fa-2x fa-sync fa-spin"></i>
      </div>
      <div class="modal-header">
        <h4 class="modal-title">Ubah Pelatihan</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form id="training-update-form">
        <div class="form-group">
          <label for="training-title-update-field">Nama Pelatihan:</label>
          <input type="text" name="title" class="form-control" id="training-title-update-field" placeholder="Nama Pelatihan.." required>
        </div>
        <div class="form-group">
          <label for="training-type-update-field">Jenis:</label>
          <select name="type" class="form-control type-option" id="training-type-update-field" required>
            <option disabled>--Pilih Jenis Pelatihan--</option>
          </select><br>
          <input type="text" name="type-other" class="form-control type-text" placeholder="Jenis Lainnya.." style="display:none;">
        </div>
        <div class="form-group">
          <label for="training-institution-update-field">Institusi:</label>
          <input type="text" name="institution" class="form-control" id="training-institution-update-field" placeholder="Institusi.." required>
        </div>
        <div class="form-group">
          <label for="training-training-date-update-field">Tanggal Pelatihan:</label>
          <input type="text" name="training_date" class="form-control training-training-date" id="training-training-date-update-field" placeholder="Tanggal Pelatihan.." required>
        </div>
        <div class="form-group">
          <label for="training-expired-date-update-field">Tanggal Expired:</label>
          <input type="text" name="expired_date" class="form-control training-expired-date" id="training-expired-date-update-field" placeholder="Tanggal Expired.." required>
        </div>
      </div>
      <div class="modal-footer justify-content-between">
        <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
        <button type="submit" class="btn btn-primary" id="training-update-button">Simpan</button>
        </form>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="training-delete-modal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Hapus Pelatihan</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        Apakah Anda yakin ingin menghapus data projek tersebut?
      </div>
      <div class="modal-footer justify-content-between">
        <button type="button" class="btn btn-default" data-dismiss="modal">Tidak</button>
        <button type="button" class="btn btn-danger" id="training-delete-button">Ya</button>
      </div>
    </div>
  </div>
</div>