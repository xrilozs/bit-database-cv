<div class="modal fade" id="working-create-modal" data-backdrop="static" data-keyboard="false" tabindex="-1" style="overflow-y: auto !important;">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Tambah Pekerjaan</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form id="working-create-form">
        <div class="form-group">
          <label for="working-company-create-field">Nama Perusahaan:</label>
          <input type="text" name="company" class="form-control" id="working-company-create-field" placeholder="Nama Perusahaan.." required>
        </div>
        <div class="form-group">
          <label for="working-position-create-field">Posisi:</label>
          <input type="text" name="position" class="form-control" id="working-position-create-field" placeholder="Posisi.." required>
        </div>
        <div class="form-group">
          <label for="working-start-year-create-field">Tahun Mulai Pekerjaan:</label>
          <input type="number" name="start_year" class="form-control" id="working-start-year-create-field" placeholder="Mulai Pekerjaan.." required>
        </div>
        <div class="form-group" id="end-year-create-section">
          <label for="working-end-year-create-field">Tahun Selesai Pekerjaan:</label>
          <input type="text" name="end_year" class="form-control" id="working-end-year-create-field" placeholder="Selesai Pekerjaan.." required>
        </div>
        <div class="custom-control custom-checkbox">
          <input type="checkbox" class="custom-control-input" id="still-working-create">
          <label class="custom-control-label" for="still-working-create">Check this custom checkbox</label>
        </div>
      </div>
      <div class="modal-footer justify-content-between">
        <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
        <button type="submit" class="btn btn-primary" id="working-create-button">Simpan</button>
        </form>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="working-update-modal" data-backdrop="static" data-keyboard="false" tabindex="-1" style="overflow-y: auto !important;">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="overlay" id="working-update-overlay">
          <i class="fas fa-2x fa-sync fa-spin"></i>
      </div>
      <div class="modal-header">
        <h4 class="modal-title">Ubah Pekerjaan</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form id="working-update-form">
        <div class="form-group">
          <label for="working-compant-update-field">Nama Perusahaan:</label>
          <input type="text" name="company" class="form-control" id="working-compant-update-field" placeholder="Nama Perusahaan.." required>
        </div>
        <div class="form-group">
          <label for="working-position-update-field">Posisi:</label>
          <input type="text" name="position" class="form-control" id="working-position-update-field" placeholder="Posisi.." required>
        </div>
        <div class="form-group">
          <label for="working-start-year-update-field">Tahun Mulai Pekerjaan:</label>
          <input type="text" name="start_year" class="form-control" id="working-start-year-update-field" placeholder="Mulai Pekerjaan.." required>
        </div>
        <div class="form-group" id="end-year-update-section">
          <label for="working-end-year-update-field">Tahun Selesai Pekerjaan:</label>
          <input type="text" name="end_year" class="form-control" id="working-end-year-update-field" placeholder="Selesai Pekerjaan.." required>
        </div>
        <div class="custom-control custom-checkbox">
          <input type="checkbox" class="custom-control-input" id="still-working-update">
          <label class="custom-control-label" for="still-working-update">Masih Bekerja</label>
        </div>
      </div>
      <div class="modal-footer justify-content-between">
        <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
        <button type="submit" class="btn btn-primary" id="working-update-button">Simpan</button>
        </form>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="working-delete-modal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Hapus Pekerjaan</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        Apakah Anda yakin ingin menghapus data pekerjaan tersebut?
      </div>
      <div class="modal-footer justify-content-between">
        <button type="button" class="btn btn-default" data-dismiss="modal">Tidak</button>
        <button type="button" class="btn btn-danger" id="working-delete-button">Ya</button>
      </div>
    </div>
  </div>
</div>