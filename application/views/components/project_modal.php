<div class="modal fade" id="project-create-modal" data-backdrop="static" data-keyboard="false" tabindex="-1" style="overflow-y: auto !important;">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Tambah Projek</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form id="project-create-form">
        <div class="form-group">
          <label for="project-title-create-field">Nama Projek:</label>
          <input type="text" name="title" class="form-control" id="project-title-create-field" placeholder="Nama Projek.." required>
        </div>
        <div class="form-group">
          <label for="project-location-create-field">Lokasi:</label>
          <input type="text" name="location" class="form-control" id="project-location-create-field" placeholder="Lokasi.." required>
        </div>
        <div class="form-group">
          <label for="project-position-create-field">Posisi:</label>
          <input type="text" name="position" class="form-control" id="project-position-create-field" placeholder="Posisi.." required>
        </div>
        <div class="form-group">
          <label for="project-start-project-create-field">Mulai Projek:</label>
          <input type="text" name="start_project" class="form-control datepicker" id="project-start-project-create-field" placeholder="Mulai Projek.." required>
        </div>
        <div class="form-group">
          <label for="project-end-project-create-field">Selesai Projek:</label>
          <input type="text" name="end_project" class="form-control datepicker" id="project-end-project-create-field" placeholder="Selesai Projek.." required>
        </div>
        <div class="form-group">
          <label for="project-jobdesk-create-field">Jobdesk:</label>
          <div id="jobdesk-create">
            <input type="text" name="jobdesk-create-0" class="form-control mb-3" id="project-jobdesk-create-field-0" placeholder="Jobdesk.." required>
          </div>
          <div class="text-right mt-2">
            <button type="button" class="btn btn-primary" id="add-jobdesk-create">
              <i class="fas fa-plus"></i>
            </button>
          </div>        
        </div>
      </div>
      <div class="modal-footer justify-content-between">
        <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
        <button type="submit" class="btn btn-primary" id="project-create-button">Simpan</button>
        </form>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="project-update-modal" data-backdrop="static" data-keyboard="false" tabindex="-1" style="overflow-y: auto !important;">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="overlay" id="project-update-overlay">
          <i class="fas fa-2x fa-sync fa-spin"></i>
      </div>
      <div class="modal-header">
        <h4 class="modal-title">Ubah Projek</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form id="project-update-form">
        <div class="form-group">
          <label for="project-title-update-field">Nama Projek:</label>
          <input type="text" name="title" class="form-control" id="project-title-update-field" placeholder="Nama Projek.." required>
        </div>
        <div class="form-group">
          <label for="project-location-update-field">Lokasi:</label>
          <input type="text" name="location" class="form-control" id="project-location-update-field" placeholder="Lokasi.." required>
        </div>
        <div class="form-group">
          <label for="project-position-update-field">Posisi:</label>
          <input type="text" name="position" class="form-control" id="project-position-update-field" placeholder="Posisi.." required>
        </div>
        <div class="form-group">
          <label for="project-start-project-update-field">Mulai Projek:</label>
          <input type="text" name="start_project" class="form-control" id="project-start-project-update-field" placeholder="Mulai Projek.." required>
        </div>
        <div class="form-group">
          <label for="project-end-project-update-field">Selesai Projek:</label>
          <input type="text" name="end_project" class="form-control" id="project-end-project-update-field" placeholder="Selesai Projek.." required>
        </div>
        <div class="form-group">
          <label for="project-jobdesk-update-field">Jobdesk:</label>
          <div id="jobdesk-update">
            <input type="text" name="jobdesk-update-0" class="form-control mb-3" id="project-jobdesk-update-field-0" placeholder="Jobdesk.." required>
          </div>
          <div class="text-right mt-2">
            <button type="button" class="btn btn-primary" id="add-jobdesk-update">
              <i class="fas fa-plus"></i>
            </button>
          </div>
        </div>
      </div>
      <div class="modal-footer justify-content-between">
        <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
        <button type="submit" class="btn btn-primary" id="project-update-button">Simpan</button>
        </form>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="project-delete-modal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Hapus Projek</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        Apakah Anda yakin ingin menghapus data projek tersebut?
      </div>
      <div class="modal-footer justify-content-between">
        <button type="button" class="btn btn-default" data-dismiss="modal">Tidak</button>
        <button type="button" class="btn btn-danger" id="project-delete-button">Ya</button>
      </div>
    </div>
  </div>
</div>