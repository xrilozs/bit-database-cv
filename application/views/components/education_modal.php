<div class="modal fade" id="education-create-modal" data-backdrop="static" data-keyboard="false" tabindex="-1" style="overflow-y: auto !important;">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Tambah Edukasi</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form id="education-create-form">
        <div class="form-group">
          <label for="education-school-create-field">Nama Sekolah:</label>
          <input type="text" name="school" class="form-control" id="education-school-create-field" placeholder="Nama Sekolah.." required>
        </div>
        <div class="form-group">
          <label for="education-level-create-field">Tingkat:</label>
          <select name="level" class="form-control" id="education-level-create-field" required>
            <option disabled>--Pilih Tingkat--</option>
            <option value="SD">SD</option>
            <option value="SMP">SMP</option>
            <option value="SMA">SMA</option>
            <option value="D1">D1</option>
            <option value="D3">D3</option>
            <option value="D4">D4</option>
            <option value="S1">S1</option>
            <option value="S2">S2</option>
            <option value="S3">S3</option>
          </select>
        </div>
        <div class="form-group">
          <label for="education-major-create-field">Jurusan:</label>
          <input type="text" name="major" class="form-control" id="education-major-create-field" placeholder="Jurusan.." required>
        </div>
        <div class="form-group">
          <label for="education-start-year-create-field">Tahun Masuk:</label>
          <input type="number" name="start_year" class="form-control" id="education-start-year-create-field" placeholder="Tahun Masuk.." required>
        </div>
        <div class="form-group">
          <label for="education-end-year-create-field">Tahun Lulus:</label>
          <input type="text" name="end_year" class="form-control" id="education-end-year-create-field" placeholder="Tahun Lulus.." required>
        </div>
      </div>
      <div class="modal-footer justify-content-between">
        <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
        <button type="submit" class="btn btn-primary" id="education-create-button">Simpan</button>
        </form>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="education-update-modal" data-backdrop="static" data-keyboard="false" tabindex="-1" style="overflow-y: auto !important;">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="overlay" id="education-update-overlay">
          <i class="fas fa-2x fa-sync fa-spin"></i>
      </div>
      <div class="modal-header">
        <h4 class="modal-title">Ubah Edukasi</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form id="education-update-form">
        <div class="form-group">
          <label for="education-school-update-field">Nama Sekolah:</label>
          <input type="text" name="school" class="form-control" id="education-school-update-field" placeholder="Nama Sekolah.." required>
        </div>
        <div class="form-group">
          <label for="education-level-update-field">Tingkat:</label>
          <select name="level" class="form-control"  id="education-level-update-field" required>
            <option disabled>--Pilih Tingkat--</option>
            <option value="SD">SD</option>
            <option value="SMP">SMP</option>
            <option value="SMA">SMA</option>
            <option value="D1">D1</option>
            <option value="D3">D3</option>
            <option value="D4">D4</option>
            <option value="S1">S1</option>
            <option value="S2">S2</option>
            <option value="S3">S3</option>
          </select>
        </div>
        <div class="form-group">
          <label for="education-major-update-field">Jurusan:</label>
          <input type="text" name="major" class="form-control" id="education-major-update-field" placeholder="Jurusan.." required>
        </div>
        <div class="form-group">
          <label for="education-start-year-update-field">Tahun Masuk:</label>
          <input type="number" name="start_year" class="form-control" id="education-start-year-update-field" placeholder="Tahun Masuk.." required>
        </div>
        <div class="form-group">
          <label for="education-end-year-update-field">Tahun Lulus:</label>
          <input type="text" name="end_year" class="form-control" id="education-end-year-update-field" placeholder="Tahun Lulus.." required>
        </div>
      </div>
      <div class="modal-footer justify-content-between">
        <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
        <button type="submit" class="btn btn-primary" id="education-update-button">Simpan</button>
        </form>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="education-delete-modal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Hapus Edukasi</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        Apakah Anda yakin ingin menghapus data edukasi tersebut?
      </div>
      <div class="modal-footer justify-content-between">
        <button type="button" class="btn btn-default" data-dismiss="modal">Tidak</button>
        <button type="button" class="btn btn-danger" id="education-delete-button">Ya</button>
      </div>
    </div>
  </div>
</div>