<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Curriculum Vitae</title>
    <style>
        table {
            border-collapse: collapse;
        }

        thead {
            background-color: #0580bc; 
            color: white;
        }

        tr, th, td {
            padding: 12px;
            border: 1px solid black;
            border-collapse: collapse;
        }

        .page-break {
            page-break-after: always;
        }

        img {
            width: 100px;
            height: 100px;
            object-fit: cover;
        }
    </style>
</head>
<body style="font-family: 'Calibri', Arial, sans-serif;">
    <br><br><br>
    <table style="width:100%;">
        <tr>
            <td style="width:10%;"></td>
            <td>
                <table style="width:100%">
                    <tr>
                        <td style="width:20%"></td>
                        <td style="width:60%; text-align:center;">
                            <h1 style="text-decoration:underline; text-align:center;">CURICULUM VITAE</h1>
                        </td>
                        <td style="width:20%"></td>
                    </tr>
                </table>
                <br>
                <h3>DATA PRIBADI</h3>
                <table style="width:100%;">
                    <tr>
                        <td style="width:70%; vertical-align:top;">
                            <table style="width: 100%;">
                                <tr>
                                    <td style="width:40%">Nama</td>
                                    <td style="width:60%">: <?=$employee->fullname;?></td>
                                </tr>
                                <tr>
                                    <td style="width:40%">Tempat / Tanggal Lahir</td>
                                    <td style="width:60%">: <?=$employee->city;?>, <?=$employee->dob;?></td>
                                </tr>
                                <tr>
                                    <td style="width:40%">Jenis Kelamin</td>
                                    <td style="width:60%">: <?=$employee->gender == 'L' ? 'Laki-laki' : 'Perempuan';?></td>
                                </tr>
                                <tr>
                                    <td style="width:40%">Alamat</td>
                                    <td style="width:60%">: <?=$employee->address;?></td>
                                </tr>
                                <tr>
                                    <td style="width:40%">No Telpon / HP</td>
                                    <td style="width:60%">: <?=$employee->phone;?></td>
                                </tr>
                                <tr>
                                    <td style="width:40%">E-mail</td>
                                    <td style="width:60%">: <?=$employee->email;?></td>
                                </tr>
                                <tr>
                                    <td style="width:40%">Kewarganegaraan</td>
                                    <td style="width:60%">: <?=$employee->nationality;?></td>
                                </tr>
                                <tr>
                                    <td style="width:40%">Pendidikan</td>
                                    <td style="width:60%">: <?=$employee->education;?></td>
                                </tr>
                            </table>
                        </td>
                        <td style="width:30%; vertical-align:top; text-align:right;">
                            <img src="<?=base_url($employee->img_url);?>" alt="" style="width:150px; height:150px; object-fit:cover;">
                        </td>
                    </tr>
                </table>
                <br>
                <h3>RIWAYAT PENDIDIKAN</h3>
                <table style="width:100%;">
                    <thead>
                        <tr>
                            <th>Tahun Mulai</th>
                            <th>Tahun Selesai</th>
                            <th>Strata Pendidikan</th>
                            <th>Sekolah / Universitas</th>
                            <th>Jurusan</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($education as $item): ?>
                        <tr>
                            <td><?=$item->start_year;?></td>
                            <td><?=$item->end_year;?></td>
                            <td><?=$item->level;?></td>
                            <td><?=$item->school;?></td>
                            <td><?=$item->major;?></td>
                        </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
                <br class="page-break">
                <h3>RIWAYAT PELATIHAN</h3>
                <table style="width:100%;">
                    <thead>
                        <tr>
                            <th>Nama Pelatihan</th>
                            <th>Jenis Pelatihan</th>
                            <th>Lembaga Sertifikasi</th>
                            <th>Tanggal Pelatihan</th>
                            <th>Tanggal Expired Sertifikat</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($training as $item): ?>
                        <tr>
                            <td><?=$item->title;?></td>
                            <td><?=$item->type;?></td>
                            <td><?=$item->institution;?></td>
                            <td><?=$item->training_date;?></td>
                            <td><?=$item->expired_date;?></td>
                        </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
                <br class="page-break">
                <h3>RIWAYAT PEKERJAAN</h3>
                <table style="width:100%;">
                    <thead>
                        <tr>
                            <th>Nama Perusahaan</th>
                            <th>Posisi / Jabatan</th>
                            <th>Tahun Mulai Bekerja</th>
                            <th>Tahun Selesai Bekerja</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($working as $item): ?>
                        <tr>
                            <td><?=$item->company;?></td>
                            <td><?=$item->position;?></td>
                            <td><?=$item->start_year;?></td>
                            <td><?=$item->end_year;?></td>
                        </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
                <br class="page-break">
                <h3>RIWAYAT PROJECT</h3>
                <table style="width:100%;">
                    <thead>
                        <tr>
                            <th>Nama Project</th>
                            <th>Lokasi Project</th>
                            <th>Posisi / Jabatan</th>
                            <th>Mulai Project</th>
                            <th>Selesai Project</th>
                            <th>Jobdesk</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($project as $item): ?>
                        <tr>
                            <td><?=$item->title;?></td>
                            <td><?=$item->location;?></td>
                            <td><?=$item->position;?></td>
                            <td><?=$item->start_project;?></td>
                            <td><?=$item->end_project;?></td>
                            <td><?=$item->jobdesk;?></td>
                        </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
                <br class="page-break">
                <h3>SERTIFIKAT KOMPETENSI</h3>
                <?php foreach ($certificate as $item): ?>
                <p><img src="<?=base_url($item->file_url);?>" alt="<?=$item->file_name;?>" style="width:100%; height:300px; object-fit:cover;"></p>
                <?php endforeach; ?>
            </td>
            <td style="width:10%;"></td>
        </tr>
    </table>
</body>
</html>
