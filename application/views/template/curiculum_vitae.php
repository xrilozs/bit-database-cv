<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
</head>
<body style="font-family: 'Calibri', Arial, sans-serif;">
    <div class="container">
        <div class="row mb-4">
            <div class="col-lg-12 text-center">
                <h1 style="text-align:center; text-decoration:underline;">CURRICULUM VITAE</h1>
            </div>
        </div>
        <div class="row mb-4">
            <div class="col-lg-12">
                <table style="width:100%;">
                    <tr>
                        <td style="width:70%; vertical-align:top;">
                            <table style="width: 100%;">
                                <tr>
                                    <td style="width:40%">Nama</td>
                                    <td style="width:60%">: <?=$employee->fullname;?></td>
                                </tr>
                                <tr>
                                    <td style="width:40%">Tempat / Tanggal Lahir</td>
                                    <td style="width:60%">: <?=$employee->city;?>, <?=$employee->dob;?></td>
                                </tr>
                                <tr>
                                    <td style="width:40%">Jenis Kelamin</td>
                                    <td style="width:60%">: <?=$employee->gender == 'L' ? 'Laki-laki' : 'Perempuan';?></td>
                                </tr>
                                <tr>
                                    <td style="width:40%">Alamat</td>
                                    <td style="width:60%">: <?=$employee->address;?></td>
                                </tr>
                                <tr>
                                    <td style="width:40%">No Telpon / HP</td>
                                    <td style="width:60%">: <?=$employee->phone;?></td>
                                </tr>
                                <tr>
                                    <td style="width:40%">E-mail</td>
                                    <td style="width:60%">: <?=$employee->email;?></td>
                                </tr>
                                <tr>
                                    <td style="width:40%">Kewarganegaraan</td>
                                    <td style="width:60%">: <?=$employee->nationality;?></td>
                                </tr>
                                <tr>
                                    <td style="width:40%">Pendidikan</td>
                                    <td style="width:60%">: <?=$employee->education;?></td>
                                </tr>
                            </table>
                        </td>
                        <td style="width:30%; vertical-align:top; text-align:right;">
                            <img src="<?=base_url($employee->img_url);?>" alt="" style="width:150px; height:150px; object-fit:cover !important;">
                        </td>
                    </tr>
                </table>
            </div>
        </div>

        <div class="row" style="page-break-inside: avoid;">
            <div class="col-lg-12">
                <h3 style="text-align:center; page-break-after: avoid;">RIWAYAT PENDIDIKAN</h3>
                <table class="table table-bordered">
                    <thead style="background-color:#0580bc !important; color:white;">
                        <tr>
                            <th style="background-color:#0580bc !important; color:white;">Tahun Mulai</th>
                            <th style="background-color:#0580bc !important; color:white;">Tahun Selesai</th>
                            <th style="background-color:#0580bc !important; color:white;">Strata Pendidikan</th>
                            <th style="background-color:#0580bc !important; color:white;">Sekolah / Universitas</th>
                            <th style="background-color:#0580bc !important; color:white;">Jurusan</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($education as $item): ?>
                        <tr>
                            <td><?=$item->start_year;?></td>
                            <td><?=$item->end_year;?></td>
                            <td><?=$item->level;?></td>
                            <td><?=$item->school;?></td>
                            <td><?=$item->major;?></td>
                        </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
        </div>

        <div class="row" style="page-break-inside: avoid;">
            <div class="col-lg-12">
                <h3 style="text-align:center; page-break-after: avoid;">RIWAYAT PELATIHAN</h3>
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th style="background-color:#0580bc !important; color:white;">Nama Pelatihan</th>
                            <th style="background-color:#0580bc !important; color:white;">Jenis Pelatihan</th>
                            <th style="background-color:#0580bc !important; color:white;">Lembaga Sertifikasi</th>
                            <th style="background-color:#0580bc !important; color:white;">Tanggal Pelatihan</th>
                            <th style="background-color:#0580bc !important; color:white;">Tanggal Expired Sertifikat</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($training as $item): ?>
                        <tr>
                            <td><?=$item->title;?></td>
                            <td><?=$item->type;?></td>
                            <td><?=$item->institution;?></td>
                            <td><?=$item->training_date;?></td>
                            <td><?=$item->expired_date;?></td>
                        </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
        </div>
        
        <div class="row" style="page-break-inside: avoid;">
            <div class="col-lg-12">
                <h3 style="text-align:center; page-break-after: avoid;">RIWAYAT PEKERJAAN</h3>
                <table class="table table-bordered">
                    <thead style="background-color:#0580bc !important; color:white;">
                        <tr>
                            <th style="background-color:#0580bc !important; color:white;">Nama Perusahaan</th>
                            <th style="background-color:#0580bc !important; color:white;">Posisi / Jabatan</th>
                            <th style="background-color:#0580bc !important; color:white;">Tahun Mulai Bekerja</th>
                            <th style="background-color:#0580bc !important; color:white;">Tahun Selesai Bekerja</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($working as $item): ?>
                        <tr>
                            <td><?=$item->company;?></td>
                            <td><?=$item->position;?></td>
                            <td><?=$item->start_year;?></td>
                            <td><?=$item->end_year;?></td>
                        </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>
            </div>
        </div>

        <div class="row" style="page-break-inside: avoid;">
            <div class="col-lg-12">
                <h3 style="text-align:center; page-break-after: avoid;">RIWAYAT PROJECT</h3>
                <?php foreach ($project as $item): ?>
                    <?php $jobdesk_arr = explode("<br>", $item->jobdesk); ?>
                    <table class="table table-bordered">
                        <thead>
                        <tr>
                            <th style="background-color:#0580bc !important; color:white;">Detail</th>
                            <th style="background-color:#0580bc !important; color:white;">Deskripsi</th>
                        </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>Nama Projek</td>
                                <td><?=$item->title;?></td>
                            </tr>
                            <tr>
                                <td>Lokasi Projek</td>
                                <td><?=$item->location;?></td>
                            </tr>
                            <tr>
                                <td>Posisi</td>
                                <td><?=$item->position;?></td>
                            </tr>
                            <tr>
                                <td>Mulai Projek</td>
                                <td><?=$item->start_project;?></td>
                            </tr>
                            <tr>
                                <td>Selesai Projek</td>
                                <td><?=$item->end_project;?></td>
                            </tr>
                            <tr>
                                <td rowspan="<?=count($jobdesk_arr)+1;?>">Jobdesk</td>
                            </tr>
                            <?php foreach ($jobdesk_arr as $jobdesk): ?>
                                <tr><td><?=$jobdesk;?></td></tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                <?php endforeach; ?>
            </div>
        </div>

        <div class="row" style="page-break-before: always;">
            <div class="col-lg-12">
                <h3 style="text-align:center;">SERTIFIKAT KOMPETENSI</h3>
                <?php foreach ($certificate as $index => $item): ?>
                    <p>
                        <img src="<?=base_url($item->file_url);?>" alt="<?=$item->file_name;?>" class="img-fluid" style="width: 100%; height: 300px; object-fit:cover; display:block; margin-bottom:10px;">
                    </p>
                <?php endforeach; ?>
            </div>
        </div>
    </div>
</body>
</html>
