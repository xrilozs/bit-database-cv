<body style="background-color:#e7e7e7;">
    <table style="border-collapse: collapse; width: 100%;">
        <tbody>
            <tr>
                <td style="width: 10%;">&nbsp;</td>
                <td style="width: 80%; background-color:white;" align="center">
                    <div style="width: 100%; padding-top: 25px; padding-bottom: 25px;">
                        <p>
                            Berikut adalah daftar sertifikat pelatihan yang sebentar lagi akan <i><b>expired</b></i>
                        </p>
                        <?php if(count($data) > 0): ?>
                        <div style="margin: 10px;">
                            <table style="font-family: Arial, Helvetica, sans-serif; border-collapse: collapse; width: 100%;">
                                <tr>
                                    <th style="padding-top: 12px; padding-bottom: 12px; text-align: left; background-color: #0580bc; color: white; border: 1px solid #ddd;">
                                        Nama Karyawan
                                    </th>
                                    <th style="padding-top: 12px; padding-bottom: 12px; text-align: left; background-color: #0580bc; color: white; border: 1px solid #ddd;">
                                        Nama Pelatihan
                                    </th>
                                    <th style="padding-top: 12px; padding-bottom: 12px; text-align: left; background-color: #0580bc; color: white; border: 1px solid #ddd;">
                                        Jenis Pelatihan
                                    </th>
                                    <th style="padding-top: 12px; padding-bottom: 12px; text-align: left; background-color: #0580bc; color: white; border: 1px solid #ddd;">
                                        Tanggal Pelatihan
                                    </th>
                                    <th style="padding-top: 12px; padding-bottom: 12px; text-align: left; background-color: #0580bc; color: white; border: 1px solid #ddd;">
                                        Tanggal Expired
                                    </th>
                                </tr>
                                <?php foreach ($data as $index => $item): ?>
                                <tr style="<?= $index % 2 == 0 ? 'background-color: #f2f2f2;' : ''; ?>">
                                    <td style="border: 1px solid #ddd; padding: 8px;"><?= htmlspecialchars($item->fullname); ?></td>
                                    <td style="border: 1px solid #ddd; padding: 8px;"><?= htmlspecialchars($item->title); ?></td>
                                    <td style="border: 1px solid #ddd; padding: 8px;"><?= htmlspecialchars($item->type); ?></td>
                                    <td style="border: 1px solid #ddd; padding: 8px;"><?= htmlspecialchars($item->training_date); ?></td>
                                    <td style="border: 1px solid #ddd; padding: 8px;"><?= htmlspecialchars($item->expired_date); ?></td>
                                </tr>
                                <?php endforeach; ?>
                            </table>
                        </div>
                        <?php else: ?>
                        <p>
                            <b>-Tidak ada-</b>
                        </p>
                        <?php endif; ?>
                    </div>
                </td>
                <td style="width: 10%;">&nbsp;</td>
            </tr>
        </tbody>
    </table>
</body>
