<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'common';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;

/* CMS Route */
#Home
$route['login']         = 'common/login';
$route['dashboard']     = 'common/dashboard';
#User
$route['user']          = 'user/page';
#User Role
$route['user-role']     = 'user_role/page';
#User Access
$route['user-access']   = 'user_access/page';
#Config
$route['config']        = 'config/page';
#Employee
$route['employee']          = 'employee/page';
$route['employee/(:any)']   = 'employee/detail';
#Advance Search
$route['advance-search']    = 'employee/advance_search';
#Self Service
$route['self-service']      = 'employee/self_service';
#Training Type
$route['training-type']     = 'training_type/page';

/* API Route */
#Common
$route['api/test']          = 'api/common/test';
$route['api/test-email']    = 'api/common/test_email';
$route['api/test-pdf']      = 'api/common/test_pdf';
#User
$route['api/user']['GET']                   = 'api/user/get_user';
$route['api/user/id/(:any)']['GET']         = 'api/user/get_user_by_id/$1';
$route['api/user']['POST']                  = 'api/user/create_user';
$route['api/user']['PUT']                   = 'api/user/update_user';
$route['api/user/active/(:any)']['PUT']     = 'api/user/active_user/$1';
$route['api/user/inactive/(:any)']['PUT']   = 'api/user/inactive_user/$1';
$route['api/user/password']['PUT']          = 'api/user/change_password_user';
$route['api/user/login']['POST']            = 'api/user/login';
$route['api/user/refresh']['POST']          = 'api/user/login_user';
$route['api/user/profile']['GET']           = 'api/user/get_user_profile';
#User Role
$route['api/user-role']['GET']                  = 'api/user_role/get_user_role';
$route['api/user-role/id/(:any)']['GET']        = 'api/user_role/get_user_role_by_id/$1';
$route['api/user-role']['POST']                 = 'api/user_role/create_user_role';
$route['api/user-role']['PUT']                  = 'api/user_role/update_user_role';
$route['api/user-role/delete/(:any)']['DELETE'] = 'api/user_role/delete_user_role/$1';
#User Access
$route['api/user-access']['GET']                = 'api/user_access/get_user_access';
$route['api/user-access/role/(:any)']['GET']    = 'api/user_access/get_user_access_by_role/$1';
$route['api/user-access/id/(:any)']['GET']      = 'api/user_access/get_user_access_by_id/$1';
$route['api/user-access']['POST']               = 'api/user_access/create_user_access';
$route['api/user-access']['PUT']                = 'api/user_access/update_user_access';
#Config
$route['api/config']['GET']         = 'api/config/get_config';
$route['api/config']['POST']        = 'api/config/save_config';
$route['api/config/upload']['POST'] = 'api/config/upload_config_image';
#Employee
$route['api/employee']['GET']                    = 'api/employee/get_employee';
$route['api/employee']['POST']                   = 'api/employee/create_employee';
$route['api/employee']['PUT']                    = 'api/employee/update_employee';
$route['api/employee/delete/(:any)']['DELETE']   = 'api/employee/delete_employee/$1';
$route['api/employee/id/(:any)']['GET']          = 'api/employee/get_employee_by_id/$1';
$route['api/employee/user']['GET']               = 'api/employee/get_employee_by_user';
$route['api/employee/total']['GET']              = 'api/employee/get_employee_total';
$route['api/employee/print/(:any)']['GET']       = 'api/employee/generate_cv/$1';
#Certificate
$route['api/certificate/upload']['POST']           = 'api/certificate/upload_certificate';
$route['api/certificate']['POST']                  = 'api/certificate/add_certificate';
$route['api/certificate/delete/(:any)']['DELETE']  = 'api/certificate/remove_certificate/$1';
$route['api/certificate/employee/(:any)']['GET']   = 'api/certificate/get_certificate_by_employee/$1';
#Education History
$route['api/education-history']['POST']                  = 'api/education_history/add_education_history';
$route['api/education-history']['PUT']                   = 'api/education_history/edit_education_history';
$route['api/education-history/delete/(:any)']['DELETE']  = 'api/education_history/remove_education_history/$1';
$route['api/education-history/employee/(:any)']['GET']   = 'api/education_history/get_education_history_by_employee/$1';
$route['api/education-history/id/(:any)']['GET']         = 'api/education_history/get_education_history_by_id/$1';
#Project History
$route['api/project-history']['POST']                  = 'api/project_history/add_project_history';
$route['api/project-history']['PUT']                   = 'api/project_history/edit_project_history';
$route['api/project-history/delete/(:any)']['DELETE']  = 'api/project_history/remove_project_history/$1';
$route['api/project-history/employee/(:any)']['GET']   = 'api/project_history/get_project_history_by_employee/$1';
$route['api/project-history/id/(:any)']['GET']         = 'api/project_history/get_project_history_by_id/$1';
#Training History
$route['api/training-history']['POST']                  = 'api/training_history/add_training_history';
$route['api/training-history']['PUT']                   = 'api/training_history/edit_training_history';
$route['api/training-history/delete/(:any)']['DELETE']  = 'api/training_history/remove_training_history/$1';
$route['api/training-history/employee/(:any)']['GET']   = 'api/training_history/get_training_history_by_employee/$1';
$route['api/training-history/id/(:any)']['GET']         = 'api/training_history/get_training_history_by_id/$1';
$route['api/training-history/option']['GET']            = 'api/training_history/get_training_history_option';
#Working History
$route['api/working-history']['POST']                  = 'api/working_history/add_working_history';
$route['api/working-history']['PUT']                   = 'api/working_history/edit_working_history';
$route['api/working-history/delete/(:any)']['DELETE']  = 'api/working_history/remove_working_history/$1';
$route['api/working-history/employee/(:any)']['GET']   = 'api/working_history/get_working_history_by_employee/$1';
$route['api/working-history/id/(:any)']['GET']         = 'api/working_history/get_working_history_by_id/$1';
#Training Type
$route['api/training-type']['GET']                  = 'api/training_type/get_training_type';
$route['api/training-type/id/(:any)']['GET']        = 'api/training_type/get_training_type_by_id/$1';
$route['api/training-type']['POST']                 = 'api/training_type/create_training_type';
$route['api/training-type']['PUT']                  = 'api/training_type/update_training_type';
$route['api/training-type/delete/(:any)']['DELETE'] = 'api/training_type/delete_training_type/$1';

#CRON
$route['api/cron/employee-notification']['GET']        = 'api/common/cron_employee_notification';
