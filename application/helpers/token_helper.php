<?php
  require 'vendor/autoload.php';
  use Firebase\JWT\JWT;

  function verify_user_token($header, $allowed_access=[]){
    $CI       =& get_instance();
    $resp_obj = new Response_api();
    
    #check header
    if(isset($header['Authorization'])){
      list(, $token) = explode(' ', $header['Authorization']);
    }else{
      $resp_obj->set_response(401, "failed", "Please use token to access this resource.");
      $resp = $resp_obj->get_response();
      return $resp;
    }
    
    #check token
    try {
      $jwt = JWT::decode($token, ACCESS_TOKEN_SALT, ['HS256']);
    } catch (Exception $e) {
      $resp_obj->set_response(401, "failed", "Invalid requested token");
      $resp = $resp_obj->get_response();
      return $resp;
    }
    
    $username = $jwt->username;
    $user     = $CI->user_model->get_user_by_username($username);
    
    #check User exist
    if(is_null($user)){
      $resp_obj->set_response(401, "failed", "User not found");
      $resp = $resp_obj->get_response();
      return $resp;
    }
    if(!$user->is_active){
      $resp_obj->set_response(401, "failed", "User is Inactive");
      $resp = $resp_obj->get_response();
      return $resp;
    }
    
    #check allowed access
    if(count($allowed_access) > 0){
      $user_access = $CI->user_access_model->get_user_access_by_role_id($user->role_id);
      if($user_access){
        foreach ($allowed_access as $access) {
          if(intval($user_access->{$access})){
            $data = array('user'=>$user);
            $resp_obj->set_response(200, "success", "Authorized access", $data);
            $resp = $resp_obj->get_response();
            return $resp;
          }
        }
        $resp_obj->set_response(401, "failed", "Unauthorized role access", $allowed_access);
        $resp = $resp_obj->get_response();
        return $resp;
      }else{
        $resp_obj->set_response(401, "failed", "Unauthorized role access", $allowed_access);
        $resp = $resp_obj->get_response();
        return $resp;
      }
    }else{
      $data = array('user'=>$user);
      $resp_obj->set_response(200, "success", "Authorized access", $data);
      $resp = $resp_obj->get_response();
      return $resp;
    }
  }

  function create_token($username){
    $token_expired  = time() + (10 * 60);
    $payload        = array(
      'username'  => $username,
      'exp'       => $token_expired
    );
    #access token
    $access_token   = JWT::encode($payload, ACCESS_TOKEN_SALT);
    #refresh token
    $payload['exp'] = time() + (60 * 60);
    $refresh_token  = JWT::encode($payload, REFRESH_TOKEN_SALT);
    return array(
      'access_token'  => $access_token,
      'refresh_token' => $refresh_token
    );
  }
?>