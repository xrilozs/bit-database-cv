<?php
  function _upload_file($file, $destination) {
    $filename       = explode(".", $file["name"]);
    $file_extension = $filename[count($filename) - 1];
    $file_weight    = $file['size'];
    $file_type      = $file['type'];
    if($file['error']){
      $data = array(
        'status' 	=> 	'failed',
        'message'	=>	'Upload file failed!'
      ); 
    }
    if($file_weight > 20097152){
      $data = array(
        'status' 	=> 	'failed',
        'message'	=>	'Max file size is 20MB!'
      );
    }
    $file_new_name = str_replace(" ", "_", strtolower(uniqid())) . '.' . $file_extension;
    if (!move_uploaded_file($file['tmp_name'], $destination . $file_new_name)) {
      $data = array(
        'status' 	=> 	'failed',
        'message'	=>	'Upload file failed!'
      );
    }
    $origin_file = $destination . $file_new_name;

    $data = array(
      'status' => 'success',
      'data' => array(
        'file_url' => $origin_file
      )
    );
    return $data;
  }

  function convertByte($size) {
	  $unit = 'KB';
	  $fileSize = round($size / 1024,4);
	  if($fileSize >= 1000){
	    $unit = 'MB';
	    $fileSize = round($size / 1024 / 1024,4);
	  }
	  if($fileSize >= 1000){
	    $unit = 'GB';
	    $fileSize = round($size / 1024 / 1024 / 1024,4);
	  }
	  return round($fileSize, 2) . " " . $unit;
  }
?>