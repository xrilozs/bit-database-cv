<?php
require_once APPPATH . '../vendor/autoload.php';

function test_generate_pdf(){
    $mpdf = new \Mpdf\Mpdf();
    $mpdf->WriteHTML('<h1>Hello world!</h1>');
    $mpdf->Output('assets/cv/test.pdf', 'F');
}

function generate_cv($data, $employee_name){
    $CI   =& get_instance();

    $mpdf = new \Mpdf\Mpdf([        
        'mode'              => 'utf-8'
    ]);
    $html = $CI->load->view('template/curiculum_vitae', $data, true);

    $headerHtml = '<div style="text-align: right;">
        <img src="'.base_url('assets/img/default-logo.png').'" style="width: 100px; height: 70px;" />
    </div>';

    // Set the header using the HTML content
    $stylesheet = file_get_contents('assets/css/kv-mpdf-bootstrap.css');
    $mpdf->SetMargins(0, 0, 30);
    $mpdf->SetHTMLHeader($headerHtml);
    $mpdf->WriteHTML($stylesheet, 1);
    $mpdf->WriteHTML($html, 2);
    $mpdf->Output("assets/cv/".$employee_name."_cv.pdf", 'F');

    return "assets/cv/".$employee_name."_cv.pdf";
}
?>
