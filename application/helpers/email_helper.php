<?php
    function send_notification_email($email, $data){
        $CI                 =& get_instance();
        $message['data']    = $data;

        $html       = $CI->load->view('template/notification_email', $message, true);
        $mail       = new Send_mail();
        $emailResp  = $mail->send($email, 'Notifikasi Sertifikat Pelatihan Expired', $html);
        return $emailResp;
    }
?>