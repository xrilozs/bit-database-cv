<?php
  class Employee_model extends CI_Model{
    public $id;
    public $user_id;
    public $img_url;
    public $fullname;
    public $dob;
    public $city;
    public $gender;
    public $address;
    public $phone;
    public $email;
    public $nationality;
    public $education;

    function get_employee($search=null, $type=null, $order=null, $limit=null){
      if($search){
        $where_search = "CONCAT_WS(',', e.fullname, e.dob, e.city, e.gender, e.address, e.phone, e.email, e.nationality, e.education) LIKE '%".$search."%'";
        $this->db->where($where_search);
      }
      if($type){
        $typesArray = explode(',', $type);

        $this->db->select("e.*, SUM(IFNULL(end_year, YEAR(CURDATE())) - start_year) AS total_work_year");
        $this->db->join("training_history t", "t.employee_id = e.id", "LEFT");
        $this->db->join("working_history w", "w.employee_id = e.id", "LEFT");
        $this->db->group_by("e.id");
        $this->db->where_in("t.type", $typesArray);
        $this->db->having('COUNT(DISTINCT t.type) = ', count($typesArray));
      }else{
        $this->db->select("e.*");
      }
      if($order){
        if($type){
          $this->db->order_by("total_work_year", "DESC"); 
        }else{
          $this->db->order_by("e.{$order['field']}", $order['order']); 
        }
      }
      if($limit){
        $this->db->limit($limit['size'], $limit['start']);
      }
      $query = $this->db->get('employee e');
      return $query->result();
    }

    function count_employee($search=null, $type=null){
      $this->db->select("e.*");
      if($search){
        $where_search = "CONCAT_WS(',', e.fullname, e.dob, e.city, e.gender, e.address, e.phone, e.email, e.nationality, e.education) LIKE '%".$search."%'";
        $this->db->where($where_search);
      }
      if($type){
        $this->db->join("training_history t", "t.employee_id = e.id", "LEFT");
        $this->db->group_by("e.id");
        $this->db->where("t.type", $type);
      }
      $this->db->from('employee e');
      return $this->db->count_all_results();
    }

    function get_employee_by_id($id, $is_assoc=false){
      $this->db->where("id", $id);
      $query = $this->db->get('employee');
      if($is_assoc){
        return $query->num_rows() ? $query->row_array() : null;
      }else{
        return $query->num_rows() ? $query->row() : null;
      }
    }

    function get_employee_by_user_id($user_id, $is_assoc=false){
      $this->db->where("user_id", $user_id);
      $query = $this->db->get('employee');
      if($is_assoc){
        return $query->num_rows() ? $query->row_array() : null;
      }else{
        return $query->num_rows() ? $query->row() : null;
      }
    }

    function create_employee($data){
      $this->id           = $data['id'];
      $this->user_id      = array_key_exists("user_id", $data) ? $data['user_id'] : null;
      $this->img_url      = $data['img_url'];
      $this->fullname     = $data['fullname'];
      $this->dob          = $data['dob'];
      $this->city         = $data['city'];
      $this->gender       = $data['gender'];
      $this->address      = $data['address'];
      $this->phone        = $data['phone'];
      $this->email        = $data['email'];
      $this->nationality  = $data['nationality'];
      $this->education    = $data['education'];
      $this->created_at   = date('Y-m-d H:i:s');

      $this->db->insert('employee', $this);
      return $this->db->affected_rows();
    }

    function update_employee($data, $id){
      $this->db->update('employee', $data, array('id' => $id));
      return $this->db->affected_rows();
    }

    function delete_employee($id){
      $this->db->where('id', $id);
      $this->db->delete('employee');
      return $this->db->affected_rows();
    }
  }
?>
