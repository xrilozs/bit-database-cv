<?php
  class Working_history_model extends CI_Model{
    public $id;
    public $employee_id;
    public $company;
    public $position;
    public $start_year;
    public $end_year;
    public $is_still_working;

    function get_working_history($search=null, $employee_id=null, $order=null, $limit=null){
      if($search){
        $where_search = "CONCAT_WS(',', company, position, start_year, end_year) LIKE '%".$search."%'";
        $this->db->where($where_search);
      }
      if($employee_id){
        $this->db->where("employee_id", $employee_id);
      }
      if($order){
        $this->db->order_by($order['field'], $order['order']); 
      }
      if($limit){
        $this->db->limit($limit['size'], $limit['start']);
      }
      $this->db->from("working_history");
      $query = $this->db->get();
      return $query->result();
    }

    function count_working_history($search=null, $employee_id=null){
      if($search){
        $where_search = "CONCAT_WS(',', company, position, start_year, end_year) LIKE '%".$search."%'";
        $this->db->where($where_search);
      }
      if($employee_id){
        $this->db->where("employee_id", $employee_id);
      }
      $this->db->from('working_history');
      return $this->db->count_all_results();
    }

    function get_working_history_by_id($id, $is_assoc=false){
      $this->db->where("id", $id);
      $this->db->from("working_history");
      $query = $this->db->get();
      if($is_assoc){
        return $query->num_rows() > 0 ? $query->row_array() : null;
      }else{
        return $query->num_rows() > 0 ? $query->row() : null;
      }
    }

    function create_working_history($data){
      $this->id               = $data['id'];
      $this->employee_id            = $data['employee_id'];
      $this->company          = $data['company'];
      $this->position         = $data['position'];
      $this->start_year       = $data['start_year'];
      $this->end_year         = array_key_exists("end_year", $data) ?  $data['end_year'] : null;
      $this->is_still_working = $data['is_still_working'];
      $this->created_at       = date('Y-m-d H:i:s');

      $this->db->insert('working_history', $this);
      return $this->db->affected_rows();
    }

    function update_working_history($data, $id){
      $this->db->update('working_history', $data, array('id' => $id));
      return $this->db->affected_rows();
    }

    function delete_working_history($id){
      $this->db->where('id', $id);
      $this->db->delete('working_history');
      return $this->db->affected_rows();
    }

    function delete_working_history_by_employee($employee_id){
      $this->db->where('employee_id', $employee_id);
      $this->db->delete('working_history');
      return $this->db->affected_rows();
    }
  }
?>
