<?php
  class User_access_model extends CI_Model {
    public $id;
    public $role_id;
    public $user_module;
    public $employee_module;
    public $search_module;
    public $self_module;
    public $training_module;
    public $config_module;

    function get_user_access($search=null, $order=null, $limit=null){
      $this->db->select("a.*, r.role");
      if($search){
        $this->db->where("r.role", $search);
      }
      $this->db->join("user_role r", "r.id = a.role_id", "LEFT");
      $this->db->from('user_access a');
      if($order){
        $this->db->order_by("a.{$order['field']}", $order['order']); 
      }
      if($limit){
        $this->db->limit($limit['size'], $limit['start']);
      }
      $query = $this->db->get();
      return $query->result();
    }

    function count_user_access($search=null){
      $this->db->select("a.*, r.role");
      $this->db->from('user_access a');
      $this->db->join("user_role r", "r.id = a.role_id", "LEFT");
      if($search){
        $this->db->where("r.role", $search);
      }
      return $this->db->count_all_results();
    }

    function get_user_access_by_id($id){
      $this->db->select("a.*, r.role");
      $this->db->join("user_role r", "r.id = a.role_id", "LEFT");
      $this->db->where("a.id", $id);
      $this->db->from('user_access a');
      $query = $this->db->get();
      return $query->num_rows() > 0 ? $query->row() : null;
    }

    function get_user_access_by_role_id($role_id){
        $this->db->select("a.*, r.role");
        $this->db->join("user_role r", "r.id = a.role_id", "LEFT");
        $this->db->where("a.role_id", $role_id);
        $this->db->from('user_access a');
        $query = $this->db->get();
        return $query->num_rows() > 0 ? $query->row() : null;
    }

    function create_user_access($data){
        $this->id               = $data['id'];
        $this->role_id          = $data['role_id'];
        $this->user_module      = $data['user_module'];
        $this->employee_module  = $data['employee_module'];
        $this->search_module    = $data['search_module'];
        $this->self_module      = $data['self_module'];
        $this->training_module  = $data['training_module'];
        $this->config_module    = $data['config_module'];
        $this->created_at       = date("Y-m-d H:i:s");
        $this->db->insert('user_access', $this);
        return $this->db->affected_rows() > 0;
    }
  
    function update_user_access($data, $id){
        $this->db->update('user_access', $data, array("id" => $id));
        return $this->db->affected_rows() > 0;
    }
  }
?>