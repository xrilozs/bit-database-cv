<?php
  class Training_type_model extends CI_Model {
    public $id;
    public $name;

    function get_training_type($search=null, $order=null, $limit=null){
        if($search){
          $this->db->where("name", $search);
        }
        $this->db->from('training_type');
        if($order){
          $this->db->order_by($order['field'], $order['order']); 
        }
        if($limit){
          $this->db->limit($limit['size'], $limit['start']);
        }
        $query = $this->db->get();
        return $query->result();
    }
  
    function count_training_type($search=null){
        $this->db->from('training_type');
        if($search){
          $this->db->where("name", $search);
        }
        return $this->db->count_all_results();
    }
  
    function get_training_type_by_id($id, $is_assoc=false){
        $this->db->where("id", $id);
        $this->db->from('training_type');
        $query = $this->db->get();
        if($is_assoc){
          return $query->num_rows() ? $query->row_array() : null;
        }else{
          return $query->num_rows() ? $query->row() : null;
        }
    }

    function create_training_type($data){
        $this->id         = $data['id'];
        $this->name       = $data['name'];
        $this->created_at = date("Y-m-d H:i:s");
        $this->db->insert('training_type', $this);
        return $this->db->affected_rows() > 0;
    }
  
    function update_training_type($data, $id){
        $this->db->update('training_type', $data, array("id" => $id));
        return $this->db->affected_rows();
    }

    function delete_training_type($id){
        $this->db->where('id', $id);
        $this->db->delete('training_type');
        return $this->db->affected_rows();
    }
  }
?>