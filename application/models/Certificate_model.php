<?php
  class Certificate_model extends CI_Model{
    public $id;
    public $employee_id;
    public $file_url;
    public $file_size;
    public $file_name;

    function get_certificate($employee_id=null, $order=null, $limit=null){
      if($employee_id){
        $this->db->where("employee_id", $employee_id);
      }
      if($order){
        $this->db->order_by($order['field'], $order['order']); 
      }
      if($limit){
        $this->db->limit($limit['size'], $limit['start']);
      }
      $this->db->from("certificate");
      $query = $this->db->get();
      return $query->result();
    }

    function count_certificat($employee_id=null){
      if($employee_id){
        $this->db->where("employee_id", $employee_id);
      }
      $this->db->from('certificate');
      return $this->db->count_all_results();
    }

    function get_certificate_by_id($id, $is_assoc=false){
      $this->db->where("id", $id);
      $query = $this->db->get('certificate');
      if($is_assoc){
        return $query->num_rows() ? $query->row_array() : null;
      }else{
        return $query->num_rows() ? $query->row() : null;
      }
    }

    function create_certificate($data){
      $this->id           = $data['id'];
      $this->employee_id  = $data['employee_id'];
      $this->file_url     = $data['file_url'];
      $this->file_name    = $data['file_name'];
      $this->file_size    = $data['file_size'];
      $this->created_at   = date('Y-m-d H:i:s');

      $this->db->insert('certificate', $this);
      return $this->db->affected_rows();
    }

    function update_certificate($data, $id){
      $this->db->update('certificate', $data, array('id' => $id));
      return $this->db->affected_rows();
    }

    function delete_certificate($id){
      $this->db->where('id', $id);
      $this->db->delete('certificate');
      return $this->db->affected_rows();
    }

    function delete_certificate_by_employee($employee_id){
      $this->db->where('employee_id', $employee_id);
      $this->db->delete('certificate');
      return $this->db->affected_rows();
    }
  }
?>
