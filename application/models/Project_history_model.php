<?php
  class Project_history_model extends CI_Model{
    public $id;
    public $employee_id;
    public $title;
    public $location;
    public $position;
    public $start_project;
    public $end_project;
    public $jobdesk;

    function get_project_history($search=null, $employee_id=null, $order=null, $limit=null){
      if($search){
        $where_search = "CONCAT_WS(',', title, location, position, start_project, end_project, jobdesk) LIKE '%".$search."%'";
        $this->db->where($where_search);
      }
      if($employee_id){
        $this->db->where("employee_id", $employee_id);
      }
      if($order){
        $this->db->order_by($order['field'], $order['order']); 
      }
      if($limit){
        $this->db->limit($limit['size'], $limit['start']);
      }
      $this->db->from("project_history");
      $query = $this->db->get();
      return $query->result();
    }

    function count_project_history($search=null, $employee_id=null){
      if($search){
        $where_search = "CONCAT_WS(',', title, location, position, start_project, end_project, jobdesk) LIKE '%".$search."%'";
        $this->db->where($where_search);
      }
      if($employee_id){
        $this->db->where("employee_id", $employee_id);
      }
      $this->db->from('project_history');
      return $this->db->count_all_results();
    }

    function get_project_history_by_id($id, $is_assoc=false){
      $this->db->where("id", $id);
      $this->db->from("project_history");
      $query = $this->db->get();
      if($is_assoc){
        return $query->num_rows() > 0 ? $query->row_array() : null;
      }else{
        return $query->num_rows() > 0 ? $query->row() : null;
      }
    }

    function create_project_history($data){
      $this->id             = $data['id'];
      $this->employee_id          = $data['employee_id'];
      $this->title          = $data['title'];
      $this->location       = $data['location'];
      $this->position       = $data['position'];
      $this->start_project  = $data['start_project'];
      $this->end_project    = $data['end_project'];
      $this->jobdesk        = $data['jobdesk'];
      $this->created_at     = date('Y-m-d H:i:s');

      $this->db->insert('project_history', $this);
      return $this->db->affected_rows();
    }

    function update_project_history($data, $id){
      $this->db->update('project_history', $data, array('id' => $id));
      return $this->db->affected_rows();
    }

    function delete_project_history($id){
      $this->db->where('id', $id);
      $this->db->delete('project_history');
      return $this->db->affected_rows();
    }

    function delete_project_history_by_employee($employee_id){
      $this->db->where('employee_id', $employee_id);
      $this->db->delete('project_history');
      return $this->db->affected_rows();
    }
  }
?>
