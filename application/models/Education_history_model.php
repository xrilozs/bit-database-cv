<?php
  class Education_history_model extends CI_Model{
    public $id;
    public $employee_id;
    public $start_year;
    public $end_year;
    public $level;
    public $school;
    public $major;

    function get_education_history($search=null, $employee_id=null, $order=null, $limit=null){
      if($search){
        $where_search = "CONCAT_WS(',', start_year, end_year, level, school, major) LIKE '%".$search."%'";
        $this->db->where($where_search);
      }
      if($employee_id){
        $this->db->where("employee_id", $employee_id);
      }
      if($order){
        $this->db->order_by($order['field'], $order['order']); 
      }
      if($limit){
        $this->db->limit($limit['size'], $limit['start']);
      }
      $this->db->from("education_history");
      $query = $this->db->get();
      return $query->result();
    }

    function count_education_history($search=null, $employee_id=null){
      if($search){
        $where_search = "CONCAT_WS(',', start_year, end_year, level, school, major) LIKE '%".$search."%'";
        $this->db->where($where_search);
      }
      if($employee_id){
        $this->db->where("employee_id", $employee_id);
      }
      $this->db->from('education_history');
      return $this->db->count_all_results();
    }

    function get_education_history_by_id($id, $is_assoc=false){
      $this->db->where("id", $id);
      $this->db->from("education_history");
      $query = $this->db->get();
      if($is_assoc){
        return $query->num_rows() > 0 ? $query->row_array() : null;
      }else{
        return $query->num_rows() > 0 ? $query->row() : null;
      }
    }

    function create_education_history($data){
      $this->id         = $data['id'];
      $this->employee_id      = $data['employee_id'];
      $this->start_year = $data['start_year'];
      $this->end_year   = $data['end_year'];
      $this->level      = $data['level'];
      $this->school     = $data['school'];
      $this->major      = $data['major'];
      $this->created_at = date('Y-m-d H:i:s');

      $this->db->insert('education_history', $this);
      return $this->db->affected_rows();
    }

    function update_education_history($data, $id){
      $this->db->update('education_history', $data, array('id' => $id));
      return $this->db->affected_rows();
    }

    function delete_education_history($id){
      $this->db->where('id', $id);
      $this->db->delete('education_history');
      return $this->db->affected_rows();
    }

    function delete_education_history_by_employee($employee_id){
      $this->db->where('employee_id', $employee_id);
      $this->db->delete('education_history');
      return $this->db->affected_rows();
    }
  }
?>
