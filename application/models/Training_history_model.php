<?php
  class Training_history_model extends CI_Model{
    public $id;
    public $employee_id;
    public $title;
    public $type;
    public $is_type_other;
    public $institution;
    public $training_date;
    public $expired_date;

    function get_training_history_almost_expired(){
      $this->db->select("t.*, e.fullname");
      $this->db->where("expired_date >= DATE_SUB(NOW(), INTERVAL 2 MONTH)");
      $this->db->join("employee e", "e.id = t.employee_id");
      $query = $this->db->get("training_history t");
      return $query->result();
    }

    function get_training_history($search=null, $employee_id=null, $expired_date=null, $order=null, $limit=null){
      if($search){
        $where_search = "CONCAT_WS(',', title, type, institution, training_date, expired_date) LIKE '%".$search."%'";
        $this->db->where($where_search);
      }
      if($employee_id){
        $this->db->where("employee_id", $employee_id);
      }
      if($expired_date){
        $this->db->where("expired_date", $expired_date);
      }
      if($order){
        $this->db->order_by($order['field'], $order['order']); 
      }
      if($limit){
        $this->db->limit($limit['size'], $limit['start']);
      }
      $this->db->from("training_history");
      $query = $this->db->get();
      return $query->result();
    }

    function count_training_history($search=null, $employee_id=null, $expired_date=null){
      if($search){
        $where_search = "CONCAT_WS(',', title, type, institution, training_date, expired_date) LIKE '%".$search."%'";
        $this->db->where($where_search);
      }
      if($employee_id){
        $this->db->where("employee_id", $employee_id);
      }
      if($expired_date){
        $this->db->where("expired_date", $expired_date);
      }
      $this->db->from('training_history');
      return $this->db->count_all_results();
    }

    function get_training_history_by_id($id, $is_assoc=false){
      $this->db->where("id", $id);
      $this->db->from("training_history");
      $query = $this->db->get();
      if($is_assoc){
        return $query->num_rows() > 0 ? $query->row_array() : null;
      }else{
        return $query->num_rows() > 0 ? $query->row() : null;
      }
    }

    function get_training_history_option(){
      $this->db->select("type");
      $this->db->group_by("type");
      $this->db->from("training_history");
      $query = $this->db->get();
      return $query->result();
    }

    function create_training_history($data){
      $this->id             = $data['id'];
      $this->employee_id          = $data['employee_id'];
      $this->title          = $data['title'];
      $this->type           = $data['type'];
      $this->is_type_other  = $data['is_type_other'];
      $this->institution    = $data['institution'];
      $this->training_date  = $data['training_date'];
      $this->expired_date   = $data['expired_date'];
      $this->created_at     = date('Y-m-d H:i:s');

      $this->db->insert('training_history', $this);
      return $this->db->affected_rows();
    }

    function update_training_history($data, $id){
      $this->db->update('training_history', $data, array('id' => $id));
      return $this->db->affected_rows();
    }

    function delete_training_history($id){
      $this->db->where('id', $id);
      $this->db->delete('training_history');
      return $this->db->affected_rows();
    }

    function delete_training_history_by_employee($employee_id){
      $this->db->where('employee_id', $employee_id);
      $this->db->delete('training_history');
      return $this->db->affected_rows();
    }
  }
?>
