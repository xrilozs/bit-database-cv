<?php
  class User_role_model extends CI_Model {
    public $id;
    public $role;

    function get_user_role($search=null, $order=null, $limit=null){
        if($search){
          $this->db->where("role", $search);
        }
        $this->db->from('user_role');
        if($order){
          $this->db->order_by($order['field'], $order['order']); 
        }
        if($limit){
          $this->db->limit($limit['size'], $limit['start']);
        }
        $query = $this->db->get();
        return $query->result();
    }
  
    function count_user_role($search=null){
        $this->db->from('user_role');
        if($search){
          $this->db->where("role", $search);
        }
        return $this->db->count_all_results();
    }
  
    function get_user_role_by_id($id, $is_assoc=false){
        $this->db->where("id", $id);
        $this->db->from('user_role');
        $query = $this->db->get();
        if($is_assoc){
          return $query->num_rows() ? $query->row_array() : null;
        }else{
          return $query->num_rows() ? $query->row() : null;
        }
    }

    function get_user_role_by_role($role, $is_assoc=false){
        $this->db->where("role", $role);
        $this->db->from('user_role');
        $query = $this->db->get();
        if($is_assoc){
          return $query->num_rows() ? $query->row_array() : null;
        }else{
          return $query->num_rows() ? $query->row() : null;
        }
    }

    function create_user_role($data){
        $this->id         = $data['id'];
        $this->role       = $data['role'];
        $this->created_at = date("Y-m-d H:i:s");
        $this->db->insert('user_role', $this);
        return $this->db->affected_rows() > 0;
    }
  
    function update_user_role($data, $id){
        $this->db->update('user_role', $data, array("id" => $id));
        return $this->db->affected_rows();
    }

    function delete_user_role($id){
        $this->db->where('id', $id);
        $this->db->delete('user_role');
        return $this->db->affected_rows();
    }
  }
?>