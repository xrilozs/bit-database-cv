<?php
  class User_model extends CI_Model {
    public $id;
    public $fullname;
    public $username;
    public $password;
    public $role_id;
    public $is_active;

    function get_user($search=null, $role_id=null, $order=null, $limit=null){
      $this->db->select("u.*, r.role");
      if($search){
        $where_search = "CONCAT_WS(',', u.username, u.fullname, r.role) LIKE '%".$search."%'";
        $this->db->where($where_search);
      }
      if($role_id){
        $this->db->where("role_id", $role_id);
      }
      $this->db->from('user u');
      $this->db->join('user_role r', "u.role_id = r.id", "LEFT");
      if($order){
        $this->db->order_by("u.{$order['field']}", $order['order']); 
      }
      if($limit){
        $this->db->limit($limit['size'], $limit['start']);
      }
      $query = $this->db->get();
      return $query->result();
    }

    function count_user($search=null, $role_id=null){
      $this->db->from('user');
      if($search){
        $where_search = "CONCAT_WS(',', u.username, u.fullname, r.role) LIKE '%".$search."%'";
        $this->db->where($where_search);
      }
      if($role_id){
        $this->db->where("role_id", $role_id);
      }
      return $this->db->count_all_results();
    }

    function get_user_by_username($username, $is_active=null, $is_assoc=false){
      $this->db->where("username", $username);
      if(!is_null($is_active)){
        $this->db->where("is_active", $is_active);
      }
      $query = $this->db->get('user');
      if($is_assoc){
        return $query->num_rows() ? $query->row_array() : null;
      }else{
        return $query->num_rows() ? $query->row() : null;
      }
    }

    function get_user_by_id($id, $is_assoc=false){
      $this->db->where("id", $id);
      $this->db->from('user');
      $query = $this->db->get();
      if($is_assoc){
        return $query->num_rows() ? $query->row_array() : null;
      }else{
        return $query->num_rows() ? $query->row() : null;
      }
    }

    function create_user($data){
      $this->id         = $data['id'];
      $this->username   = $data['username'];
      $this->password   = $data['password'];
      $this->fullname   = $data['fullname'];
      $this->role_id    = $data['role_id'];
      $this->is_active  = 1;
      $this->created_at = date("Y-m-d H:i:s");
      $this->db->insert('user', $this);
      return $this->db->affected_rows() > 0;
    }

    function update_user($data, $id){
      $this->db->update('user', $data, array("id" => $id));
      return $this->db->affected_rows();
    }

    function inactive_user($id){
      $data = array(
        'is_active' => 0,
      );
      $this->db->update('user', $data, array('id' => $id));
      return $this->db->affected_rows();
    }

    function active_user($id){
      $data = array(
        'is_active' => 1
      );
      $this->db->update('user', $data, array('id' => $id));
      return $this->db->affected_rows();
    }
    
    function change_password_user($password, $id){
      $data = array(
        'password' => $password
      );
      $this->db->update('user', $data, array('id' => $id));
      return $this->db->affected_rows();
    }
  }
?>
