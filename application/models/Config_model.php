<?php
  class Config_model extends CI_Model{
    public $id;
    public $manager_email;
    public $cv_logo;
    public $apps_logo;
    public $apps_icon;

    function get_config($is_assoc=false){
      $this->db->from('config');
      $query = $this->db->get();
      if($is_assoc){
        return $query->num_rows() ? $query->row_array() : null;
      }else{
        return $query->num_rows() ? $query->row() : null;
      }
    }

    function get_config_by_id($id, $is_assoc=false){
      $this->db->where("id", $id);
      $this->db->from('config');
      $query = $this->db->get();
      if($is_assoc){
        return $query->num_rows() ? $query->row_array() : null;
      }else{
        return $query->num_rows() ? $query->row() : null;
      }
    }

    function create_config($data){
      $this->id             = $data['id'];
      $this->manager_email  = $data['manager_email'];
      $this->cv_logo        = $data['cv_logo'];
      $this->apps_logo      = $data['apps_logo'];
      $this->apps_icon      = $data['apps_icon'];
      $this->db->insert('config', $this);
      return $this->db->affected_rows() > 0;
    }

    function update_config($data, $id){
      $this->db->update('config', $data, array("id"=>$id));
      return $this->db->affected_rows();
    } 
  }
?>
