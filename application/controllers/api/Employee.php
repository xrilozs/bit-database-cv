<?php
defined('BASEPATH') OR exit('No direct script access allowed');
if (isset($_SERVER['HTTP_ORIGIN'])) {
    header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
    header('Access-Control-Allow-Credentials: true');
    header('Access-Control-Max-Age: 86400');    // cache for 1 day
}

// Access-Control headers are received during OPTIONS requests
if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {

    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
        header("Access-Control-Allow-Methods: GET, POST, PUT, DELETE, OPTIONS");         

    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
        header("Access-Control-Allow-Headers:        {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");

    exit(0);
}

class Employee extends CI_Controller {

    public function __construct(){
        parent::__construct();
        $this->load->helper('url_helper');
    }

    #path: /api/employee/total [GET]
    function get_employee_total(){
        #init req & resp
        $resp_obj       = new Response_api();

        #check token
        $header = $this->input->request_headers();
        $resp   = verify_user_token($header, ['employee_module', 'search_module']);
        if($resp['status'] == 'failed'){
            logging('error', '/api/employee/total [GET] - '.$resp['message']);
            set_output($resp);
            return;
        }

        #get employee total
        $total = $this->employee_model->count_employee();

        logging('debug', '/api/employee/total [GET] - Get employee total is success');
        $resp_obj->set_response(200, "success", "Get employee total is success", $total);
        set_output($resp_obj->get_response());
        return;
    }

    #path: /api/employee [GET]
    function get_employee(){
        #init req & resp
        $resp_obj       = new Response_api();
        $page_number    = $this->input->get('page_number');
        $page_size      = $this->input->get('page_size');
        $search         = $this->input->get('search');
        $type           = $this->input->get('type');
        $draw           = $this->input->get('draw');

        #check token
        $header = $this->input->request_headers();
        $resp   = verify_user_token($header, ['employee_module', 'search_module']);
        if($resp['status'] == 'failed'){
            logging('error', '/api/employee [GET] - '.$resp['message']);
            set_output($resp);
            return;
        }
        
        #check request params
        $params = array($page_number, $page_size);
        if(!check_parameter($params)){
            logging('error', "/api/employee [GET] - Missing parameter. please check API documentation", array('page_number'=>$page_number, 'page_size'=>$page_size));
            $resp_obj->set_response(400, "failed", "Missing parameter. please check API documentation");
            set_output($resp_obj->get_response());
            return;
        }

        #get employee
        $start      = $page_number * $page_size;
        $order      = array('field'=>'created_at', 'order'=>'DESC');
        $limit      = array('start'=>$start, 'size'=>$page_size);
        $employee   = $this->employee_model->get_employee($search, $type, $order, $limit);
        $query = $this->db->last_query();
        $total      = $this->employee_model->count_employee($search, $type);

        if(empty($draw)){
            logging('debug', '/api/employee [GET] - Get employee is success');
            $resp_obj->set_response(200, "success", "Get employee is success", $employee);
            set_output($resp_obj->get_response());
            return;
        }else{
            logging('debug', '/api/employee [GET] - Get employee is success'.$query);
            $resp_obj->set_response_datatable(200, $employee, $draw, $total, $total);
            set_output($resp_obj->get_response_datatable());
            return;
        } 
    }

    #path: /api/employee/id/$id [GET]
    function get_employee_by_id($id){
        $resp_obj   = new Response_api();
        $is_detail  = $this->input->get('is_detail');

        #check token
        $header = $this->input->request_headers();
        $resp   = verify_user_token($header, ['employee_module']);
        if($resp['status'] == 'failed'){
            logging('error', '/api/employee/id/'.$id.' [GET] - '.$resp['message']);
            set_output($resp);
            return;
        }

        #get employee detail
        $employee = $this->employee_model->get_employee_by_id($id);
        if(is_null($employee)){
            logging('error', '/api/employee/id/'.$id.' [GET] - employee not found');
            $resp_obj->set_response(404, "failed", "employee not found");
            set_output($resp_obj->get_response());
            return;
        }

        #get certificate detail
        if(isset($is_detail) && intval($is_detail)){
            $employee->certificate        = $this->certificate_model->get_certificate($employee->id);
            $employee->education_history  = $this->education_history_model->get_education_history(null, $employee->id);
            $employee->project_history    = $this->project_history_model->get_project_history(null, $employee->id);
            $employee->training_history   = $this->training_history_model->get_training_history(null, $employee->id);
            $employee->working_history    = $this->working_history_model->get_working_history(null, $employee->id);
        }

        #response
        logging('debug', '/api/employee/id/'.$id.' [GET] - Get employee by id success', $employee);
        $resp_obj->set_response(200, "success", "Get employee by id success", $employee);
        set_output($resp_obj->get_response());
        return;
    }

    #path: /api/employee/user [GET]
    function get_employee_by_user(){
        $resp_obj   = new Response_api();
        $is_detail  = $this->input->get('is_detail');

        #check token
        $header = $this->input->request_headers();
        $resp   = verify_user_token($header, ['self_module']);
        if($resp['status'] == 'failed'){
            logging('error', '/api/employee/user [GET] - '.$resp['message']);
            set_output($resp);
            return;
        }
        $user = $resp['data']['user'];

        #get employee detail
        $employee = $this->employee_model->get_employee_by_user_id($user->id);
        if(is_null($employee)){
            logging('error', '/api/employee/user [GET] - employee not found');
            $resp_obj->set_response(404, "failed", "employee not found");
            set_output($resp_obj->get_response());
            return;
        }

        #get certificate detail
        if(isset($is_detail) && intval($is_detail)){
            $employee->certificate        = $this->certificate_model->get_certificate($employee->id);
            $employee->education_history  = $this->education_history_model->get_education_history(null, $employee->id);
            $employee->project_history    = $this->project_history_model->get_project_history(null, $employee->id);
            $employee->training_history   = $this->training_history_model->get_training_history(null, $employee->id);
            $employee->working_history    = $this->working_history_model->get_working_history(null, $employee->id);
        }

        #response
        logging('debug', '/api/employee/user [GET] - Get employee by user success', $employee);
        $resp_obj->set_response(200, "success", "Get employee by user success", $employee);
        set_output($resp_obj->get_response());
        return;
    }

    #path: /api/employee/print/$id [GET]
    function generate_cv($id){
        $resp_obj   = new Response_api();

        #check token
        $header = $this->input->request_headers();
        $resp   = verify_user_token($header, ['employee_module', 'self_module']);
        if($resp['status'] == 'failed'){
            logging('error', '/api/employee/print/'.$id.' [GET] - '.$resp['message']);
            set_output($resp);
            return;
        }

        #get employee detail
        $employee = $this->employee_model->get_employee_by_id($id);
        if(is_null($employee)){
            logging('error', '/api/employee/print/'.$id.' [GET] - employee not found');
            $resp_obj->set_response(404, "failed", "employee not found");
            set_output($resp_obj->get_response());
            return;
        }

        #get certificate detail
        $data = array(
            "employee"      => $employee,
            "certificate"   => $this->certificate_model->get_certificate($employee->id),
            "education"     => $this->education_history_model->get_education_history(null, $employee->id),
            "project"       => $this->project_history_model->get_project_history(null, $employee->id),
            "training"      => $this->training_history_model->get_training_history(null, $employee->id),
            "working"       => $this->working_history_model->get_working_history(null, $employee->id)
        );

        $pdfResp = generate_cv($data, $employee->fullname);

        #response
        logging('debug', '/api/employee/print/'.$id.' [GET] - Generate CV success', $pdfResp);
        $resp_obj->set_response(200, "success", "Generate CV success", $pdfResp);
        set_output($resp_obj->get_response());
        return;
    }

    #path: /api/employee [POST]
    function create_employee(){
        #init req & res
        $resp_obj       = new Response_api();
        $request        = json_decode($this->input->raw_input_stream, true);
        $self_service   = $this->input->get("self-service");
        
        #check token
        $header = $this->input->request_headers();
        $resp   = verify_user_token($header, ['employee_module', 'self_module']);
        if($resp['status'] == 'failed'){
            logging('error', '/api/employee/user [GET] - '.$resp['message']);
            set_output($resp);
            return;
        }
        $user = $resp['data']['user'];
        
        #check request params
        $keys = array('img_url', 'fullname', 'dob', 'city', 'gender', 'address', 'phone', 'email', 'nationality', 'education');
        if(!check_parameter_by_keys($request, $keys)){
            logging('error', '/api/employee/user [GET] - Missing parameter. please check API documentation', $request);
            $resp_obj->set_response(400, "failed", "Missing parameter. please check API documentation");
            set_output($resp_obj->get_response());
            return;
        }

        #create employee
        $request['id'] = get_uniq_id();
        if(isset($self_service) && intval($self_service)){
            $request['user_id'] = $user->id;
        }
        $flag = $this->employee_model->create_employee($request);
        
        #response
        if(!$flag){
            logging('error', '/api/employee/user [GET] - Internal server error', $request);
            $resp_obj->set_response(500, "failed", "Internal server error");
            set_output($resp_obj->get_response());
            return;
        }
        logging('debug', '/api/employee/user [GET] - Create employee success', $request);
        $resp_obj->set_response(200, "success", "Create employee success", $request);
        set_output($resp_obj->get_response());
        return;
    }

    #path: /api/employee [PUT]
    function update_employee(){
        $resp_obj   = new Response_api();
        $request    = json_decode($this->input->raw_input_stream, true);

        #check token
        $header = $this->input->request_headers();
        $resp   = verify_user_token($header, ['employee_module', 'self_module']);
        if($resp['status'] == 'failed'){
            logging('error', '/api/employee [PUT] - '.$resp['message']);
            set_output($resp);
            return;
        }
        
        #check request params
        $keys = array('id', 'img_url', 'fullname', 'dob', 'city', 'gender', 'address', 'phone', 'email', 'nationality', 'education');
        if(!check_parameter_by_keys($request, $keys)){
            logging('error', '/api/employee [PUT] - Missing parameter. please check API documentation', $request);
            $resp_obj->set_response(400, "failed", "Missing parameter. please check API documentation");
            set_output($resp_obj->get_response());
            return;
        }

        #check employee
        $employee = $this->employee_model->get_employee_by_id($request['id']);
        if(is_null($employee)){
            logging('error', '/api/employee [PUT] - employee not found', $request);
            $resp_obj->set_response(404, "failed", "employee not found");
            set_output($resp_obj->get_response());
            return;
        }

        $flag = $this->employee_model->update_employee($request, $request['id']);
        
        #response
        if(empty($flag)){
            logging('error', '/api/employee [PUT] - Internal server error', $request);
            $resp_obj->set_response(500, "failed", "Internal server error");
            set_output($resp_obj->get_response());
            return;
        }
        logging('debug', '/api/employee [PUT] - Update employee success', $request);
        $resp_obj->set_response(200, "success", "Update employee success", $request);
        set_output($resp_obj->get_response());
        return;
    }

    #path: /api/employee/$id [DELETE]
    function delete_employee($id){
        $resp_obj = new Response_api();

        #check token
        $header = $this->input->request_headers();
        $resp   = verify_user_token($header, ['employee_module']);
        if($resp['status'] == 'failed'){
            logging('error', '/api/employee/'.$id.' [DELETE] - '.$resp['message']);
            set_output($resp);
            return;
        }

        #check employee
        $employee = $this->employee_model->get_employee_by_id($id);
        if(is_null($employee)){
            logging('error', '/api/employee/'.$id.' [DELETE] - employee not found');
            $resp_obj->set_response(404, "failed", "employee not found");
            set_output($resp_obj->get_response());
            return;
        }

        #update employee
        $this->certificate_model->delete_certificate_by_employee($id);
        $this->education_history_model->delete_education_history_by_employee($id);
        $this->project_history_model->delete_project_history_by_employee($id);
        $this->training_history_model->delete_training_history_by_employee($id);
        $this->working_history_model->delete_working_history_by_employee($id);
        $flag = $this->employee_model->delete_employee($id);
        
        #response
        if(empty($flag)){
            logging('error', '/api/employee/'.$id.' [DELETE] - Internal server error');
            $resp_obj->set_response(500, "failed", "Internal server error");
            set_output($resp_obj->get_response());
            return;
        }
        logging('debug', '/api/employee/'.$id.' [DELETE] - Delete employee success');
        $resp_obj->set_response(200, "success", "Delete employee success");
        set_output($resp_obj->get_response());
        return;
    }

    #path: /api/employee/upload [POST]
    function upload(){
        #init variable
        $resp_obj = new Response_api();

        #check token
        $header = $this->input->request_headers();
        $resp = verify_user_token($header, ['employee_module', 'self_module']);
        if($resp['status'] == 'failed'){
            logging('error', '/api/employee/upload [POST] - '.$resp['message']);
            set_output($resp);
            return;
        }

        #check requested param
        $destination = 'assets/employee/';
        if (empty($_FILES['image']['name'])) {
            logging('error', '/api/employee/upload [POST] - Missing parameter. please check API documentation');
            $resp_obj->set_response(400, "failed", "Missing parameter. please check API documentation");
            set_output($resp_obj->get_response());
            return;   
        }

        #upload image
        $file = $_FILES['image'];
        $resp = _upload_file($file, $destination);

        #response
        if($resp['status'] == 'failed'){
            logging('error', '/api/employee/upload [POST] - '.$resp['message']);
            $resp_obj->set_response(400, "failed", $resp['message']);
            set_output($resp_obj->get_response());
            return; 
        }
        $data                   = $resp['data'];
        $data['full_file_url']  = base_url($data['file_url']);
        
        logging('debug', '/api/employee/upload [POST] - Upload image success', $data);
        $resp_obj->set_response(200, "success", "Upload image success", $data);
        set_output($resp_obj->get_response());
        return; 
    }
}
