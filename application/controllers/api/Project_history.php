<?php
defined('BASEPATH') OR exit('No direct script access allowed');
if (isset($_SERVER['HTTP_ORIGIN'])) {
    header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
    header('Access-Control-Allow-Credentials: true');
    header('Access-Control-Max-Age: 86400');    // cache for 1 day
}

// Access-Control headers are received during OPTIONS requests
if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {

    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
        header("Access-Control-Allow-Methods: GET, POST, PUT, DELETE, OPTIONS");         

    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
        header("Access-Control-Allow-Headers: {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");

    exit(0);
}

class Project_history extends CI_Controller {
    #path: /api/project-history/employee/$1 [GET]
    function get_project_history_by_employee($employee_id){
        #init req & res
        $resp_obj   = new Response_api();
        
        #check token
        $header = $this->input->request_headers();
        $resp   = verify_user_token($header, ['employee_module', 'self_module']);
        if($resp['status'] == 'failed'){
            logging('error', '/api/project-history/employee/$id [GET] - '.$resp['message']);
            set_output($resp);
            return;
        }

        $project_history = $this->project_history_model->get_project_history(null, $employee_id);

        logging('debug', '/api/project-history/employee/$id [GET] - Get project history by employee success');
        $resp_obj->set_response(200, "success", "Get project history by employee success", $project_history);
        set_output($resp_obj->get_response());
        return;
    }

    #path: /api/project-history/id/$id [GET]
    function get_project_history_by_id($id){
        $resp_obj = new Response_api();

        #check token
        $header = $this->input->request_headers();
        $resp   = verify_user_token($header, ['employee_module', 'self_module']);
        if($resp['status'] == 'failed'){
            logging('error', '/api/project-history/id/'.$id.' [GET] - '.$resp['message']);
            set_output($resp);
            return;
        }

        #check project history 
        $project_history = $this->project_history_model->get_project_history_by_id($id);
        if(is_null($project_history)){
            logging('error', '/api/project-history/id/'.$id.' [GET] - project history not found');
            $resp_obj->set_response(404, "failed", "project history not found");
            set_output($resp_obj->get_response());
            return;
        }

        logging('debug', '/api/project-history/id/'.$id.' [GET] - Get project history by id success');
        $resp_obj->set_response(200, "success", "Get project history by id success", $project_history);
        set_output($resp_obj->get_response());
        return;
    }

    #path: /api/project-history [POST]
    function add_project_history(){
        #init req & res
        $resp_obj   = new Response_api();
        $request    = json_decode($this->input->raw_input_stream, true);
        
        #check token
        $header = $this->input->request_headers();
        $resp = verify_user_token($header, ['employee_module', 'self_module']);
        if($resp['status'] == 'failed'){
            logging('error', '/api/project-history [POST] - '.$resp['message']);
            set_output($resp);
            return;
        }
        
        #check request params
        $keys = array('employee_id', 'title', 'location', 'position', 'start_project', 'end_project', 'jobdesk');
        if(!check_parameter_by_keys($request, $keys)){
            logging('error', '/api/project-history [POST] - Missing parameter. please check API documentation', $request);
            $resp_obj->set_response(400, "failed", "Missing parameter. please check API documentation");
            set_output($resp_obj->get_response());
            return;
        }

        #create education history
        $request['id']  = get_uniq_id();
        $flag           = $this->project_history_model->create_project_history($request);
        
        #response
        if(!$flag){
            logging('error', '/api/project-history [POST] - Internal server error', $request);
            $resp_obj->set_response(500, "failed", "Internal server error");
            set_output($resp_obj->get_response());
            return;
        }
        logging('debug', '/api/project-history [POST] - Add project history success', $request);
        $resp_obj->set_response(200, "success", "Add project history success", $request);
        set_output($resp_obj->get_response());
        return;
    }

    #path: /api/project-history [PUT]
    function edit_project_history(){
        #init req & res
        $resp_obj   = new Response_api();
        $request    = json_decode($this->input->raw_input_stream, true);
        
        #check token
        $header = $this->input->request_headers();
        $resp = verify_user_token($header, ['employee_module', 'self_module']);
        if($resp['status'] == 'failed'){
            logging('error', '/api/project-history [PUT] - '.$resp['message']);
            set_output($resp);
            return;
        }
        
        #check request params
        $keys = array('id', 'employee_id', 'title', 'location', 'position', 'start_project', 'end_project', 'jobdesk');
        if(!check_parameter_by_keys($request, $keys)){
            logging('error', '/api/project-history [PUT] - Missing parameter. please check API documentation', $request);
            $resp_obj->set_response(400, "failed", "Missing parameter. please check API documentation");
            set_output($resp_obj->get_response());
            return;
        }

        #check project history 
        $project_history = $this->project_history_model->get_project_history_by_id($request['id']);
        if(is_null($project_history)){
            logging('error', '/api/project-history [PUT] - project history not found');
            $resp_obj->set_response(404, "failed", "project history not found");
            set_output($resp_obj->get_response());
            return;
        }

        #check employee
        $employee = $this->employee_model->get_employee_by_id($request['employee_id']);
        if(is_null($employee)){
            logging('error', '/api/project-history [PUT] - employee not found');
            $resp_obj->set_response(404, "failed", "employee not found");
            set_output($resp_obj->get_response());
            return;
        }

        #create education history
        $flag = $this->project_history_model->update_project_history($request, $request['id']);
        
        #response
        if(!$flag){
            logging('error', '/api/project-history [PUT] - Internal server error', $request);
            $resp_obj->set_response(500, "failed", "Internal server error");
            set_output($resp_obj->get_response());
            return;
        }
        logging('debug', '/api/project-history [PUT] - Edit project history success', $request);
        $resp_obj->set_response(200, "success", "Edit project history success", $request);
        set_output($resp_obj->get_response());
        return;
    }

    #path: /api/project-history/delete/$id [DELETE]
    function remove_project_history($id){
        $resp_obj = new Response_api();

        #check token
        $header = $this->input->request_headers();
        $resp = verify_user_token($header, ['employee_module', 'self_module']);
        if($resp['status'] == 'failed'){
            logging('error', '/api/project-history/delete/'.$id.' [DELETE] - '.$resp['message']);
            set_output($resp);
            return;
        }

        #check project history 
        $project_history = $this->project_history_model->get_project_history_by_id($id);
        if(is_null($project_history)){
            logging('error', '/api/project-history/delete/'.$id.' [DELETE] - project history not found');
            $resp_obj->set_response(404, "failed", "project history not found");
            set_output($resp_obj->get_response());
            return;
        }

        #delete project history 
        $flag = $this->project_history_model->delete_project_history($id);
        if(empty($flag)){
            logging('error', '/api/project-history/delete/'.$id.' [DELETE] - Internal server error');
            $resp_obj->set_response(500, "failed", "Internal server error");
            set_output($resp_obj->get_response());
            return;
        }
        logging('debug', '/api/project-history/delete/'.$id.' [DELETE] - Delete project history success');
        $resp_obj->set_response(200, "success", "Delete project history success");
        set_output($resp_obj->get_response());
        return;
    }
}
?>