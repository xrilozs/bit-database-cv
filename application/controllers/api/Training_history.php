<?php
defined('BASEPATH') OR exit('No direct script access allowed');
if (isset($_SERVER['HTTP_ORIGIN'])) {
    header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
    header('Access-Control-Allow-Credentials: true');
    header('Access-Control-Max-Age: 86400');    // cache for 1 day
}

// Access-Control headers are received during OPTIONS requests
if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {

    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
        header("Access-Control-Allow-Methods: GET, POST, PUT, DELETE, OPTIONS");         

    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
        header("Access-Control-Allow-Headers: {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");

    exit(0);
}

class Training_history extends CI_Controller {
    #path: /api/training-history/employee/$1 [GET]
    function get_training_history_by_employee($employee_id){
        #init req & res
        $resp_obj   = new Response_api();
        
        #check token
        $header = $this->input->request_headers();
        $resp   = verify_user_token($header, ['employee_module', 'self_module']);
        if($resp['status'] == 'failed'){
            logging('error', '/api/training-history/employee/$id [GET] - '.$resp['message']);
            set_output($resp);
            return;
        }

        $training_history = $this->training_history_model->get_training_history(null, $employee_id);

        logging('debug', '/api/training-history/employee/$id [GET] - Get training history by employee success');
        $resp_obj->set_response(200, "success", "Get training history by employee success", $training_history);
        set_output($resp_obj->get_response());
        return;
    }

    #path: /api/training-history/id/$id [GET]
    function get_training_history_by_id($id){
        $resp_obj = new Response_api();

        #check token
        $header = $this->input->request_headers();
        $resp   = verify_user_token($header, ['employee_module', 'self_module']);
        if($resp['status'] == 'failed'){
            logging('error', '/api/training-history/id/'.$id.' [GET] - '.$resp['message']);
            set_output($resp);
            return;
        }

        #check training history 
        $training_history = $this->training_history_model->get_training_history_by_id($id);
        if(is_null($training_history)){
            logging('error', '/api/training-history/id/'.$id.' [GET] - training history not found');
            $resp_obj->set_response(404, "failed", "training history not found");
            set_output($resp_obj->get_response());
            return;
        }

        logging('debug', '/api/training-history/id/'.$id.' [GET] - Get training history by id success');
        $resp_obj->set_response(200, "success", "Get training history by id success", $training_history);
        set_output($resp_obj->get_response());
        return;
    }

    #path: /api/training-history/option [GET]
    function get_training_history_option(){
        #init req & res
        $resp_obj   = new Response_api();
        
        #check token
        $header = $this->input->request_headers();
        $resp   = verify_user_token($header, ['search_module']);
        if($resp['status'] == 'failed'){
            logging('error', '/api/training-history/option [GET] - '.$resp['message']);
            set_output($resp);
            return;
        }

        $training_history_option = $this->training_history_model->get_training_history_option();

        logging('debug', '/api/training-history/option [GET] - Get training history option success');
        $resp_obj->set_response(200, "success", "Get training history option success", $training_history_option);
        set_output($resp_obj->get_response());
        return;
    }

    #path: /api/training-history [POST]
    function add_training_history(){
        #init req & res
        $resp_obj   = new Response_api();
        $request    = json_decode($this->input->raw_input_stream, true);
        
        #check token
        $header = $this->input->request_headers();
        $resp = verify_user_token($header, ['employee_module', 'self_module']);
        if($resp['status'] == 'failed'){
            logging('error', '/api/training-history [POST] - '.$resp['message']);
            set_output($resp);
            return;
        }
        
        #check request params
        $keys = array('employee_id', 'title', 'type', 'is_type_other', 'institution', 'training_date', 'expired_date');
        if(!check_parameter_by_keys($request, $keys)){
            logging('error', '/api/training-history [POST] - Missing parameter. please check API documentation', $request);
            $resp_obj->set_response(400, "failed", "Missing parameter. please check API documentation");
            set_output($resp_obj->get_response());
            return;
        }


        #create education history
        $request['id']  = get_uniq_id();
        $flag           = $this->training_history_model->create_training_history($request);
        
        #response
        if(!$flag){
            logging('error', '/api/training-history [POST] - Internal server error', $request);
            $resp_obj->set_response(500, "failed", "Internal server error");
            set_output($resp_obj->get_response());
            return;
        }
        logging('debug', '/api/training-history [POST] - Add training history success', $request);
        $resp_obj->set_response(200, "success", "Add training history success", $request);
        set_output($resp_obj->get_response());
        return;
    }

    #path: /api/training-history [PUT]
    function edit_training_history(){
        #init req & res
        $resp_obj   = new Response_api();
        $request    = json_decode($this->input->raw_input_stream, true);
        
        #check token
        $header = $this->input->request_headers();
        $resp = verify_user_token($header, ['employee_module', 'self_module']);
        if($resp['status'] == 'failed'){
            logging('error', '/api/training-history [PUT] - '.$resp['message']);
            set_output($resp);
            return;
        }
        
        #check request params
        $keys = array('employee_id', 'title', 'type', 'is_type_other', 'institution', 'training_date', 'expired_date');
        if(!check_parameter_by_keys($request, $keys)){
            logging('error', '/api/training-history [PUT] - Missing parameter. please check API documentation', $request);
            $resp_obj->set_response(400, "failed", "Missing parameter. please check API documentation");
            set_output($resp_obj->get_response());
            return;
        }

        #check training history 
        $training_history = $this->training_history_model->get_training_history_by_id($request['id']);
        if(is_null($training_history)){
            logging('error', '/api/training-history [PUT] - training history not found');
            $resp_obj->set_response(404, "failed", "training history not found");
            set_output($resp_obj->get_response());
            return;
        }

        #check employee
        $employee = $this->employee_model->get_employee_by_id($request['employee_id']);
        if(is_null($employee)){
            logging('error', '/api/training-history [PUT] - employee not found');
            $resp_obj->set_response(404, "failed", "employee not found");
            set_output($resp_obj->get_response());
            return;
        }

        #create education history
        $flag = $this->training_history_model->update_training_history($request, $request['id']);
        
        #response
        if(!$flag){
            logging('error', '/api/training-history [PUT] - Internal server error', $request);
            $resp_obj->set_response(500, "failed", "Internal server error");
            set_output($resp_obj->get_response());
            return;
        }
        logging('debug', '/api/training-history [PUT] - Edit training history success', $request);
        $resp_obj->set_response(200, "success", "Edit training history success", $request);
        set_output($resp_obj->get_response());
        return;
    }

    #path: /api/training-history/delete/$id [DELETE]
    function remove_training_history($id){
        $resp_obj = new Response_api();

        #check token
        $header = $this->input->request_headers();
        $resp = verify_user_token($header, ['employee_module', 'self_module']);
        if($resp['status'] == 'failed'){
            logging('error', '/api/training-history/delete/'.$id.' [DELETE] - '.$resp['message']);
            set_output($resp);
            return;
        }

        #check training history 
        $training_history = $this->training_history_model->get_training_history_by_id($id);
        if(is_null($training_history)){
            logging('error', '/api/training-history/delete/'.$id.' [DELETE] - training history not found');
            $resp_obj->set_response(404, "failed", "training history not found");
            set_output($resp_obj->get_response());
            return;
        }

        #delete training history 
        $flag = $this->training_history_model->delete_training_history($id);
        if(empty($flag)){
            logging('error', '/api/training-history/delete/'.$id.' [DELETE] - Internal server error');
            $resp_obj->set_response(500, "failed", "Internal server error");
            set_output($resp_obj->get_response());
            return;
        }
        logging('debug', '/api/training-history/delete/'.$id.' [DELETE] - Delete training history success');
        $resp_obj->set_response(200, "success", "Delete training history success");
        set_output($resp_obj->get_response());
        return;
    }
}
?>