<?php
defined('BASEPATH') OR exit('No direct script access allowed');
if (isset($_SERVER['HTTP_ORIGIN'])) {
    header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
    header('Access-Control-Allow-Credentials: true');
    header('Access-Control-Max-Age: 86400');
}

if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {

    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
        header("Access-Control-Allow-Methods: GET, POST, PUT, DELETE, OPTIONS");         

    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
        header("Access-Control-Allow-Headers:        {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");

    exit(0);
}

// require_once(APPPATH . './vendor/autoload.php');
require FCPATH . 'vendor/autoload.php';
use Firebase\JWT\JWT;

class User extends CI_Controller {
    public function __construct($config = 'rest'){
      parent::__construct($config);
    }

    #path: /api/user [GET]
    function get_user(){
        $resp = new Response_api();

        #init variable
        $page_number  = $this->input->get('page_number');
        $page_size    = $this->input->get('page_size');
        $search       = $this->input->get('search');
        $role_id      = $this->input->get('role_id');
        $draw         = $this->input->get('draw');
        $params       = array($page_number, $page_size);

        #check token
        $header      = $this->input->request_headers();
        $verify_resp = verify_user_token($header, ["user_module"]);
        if($verify_resp['status'] == 'failed'){
            logging('error', '/api/user [GET] - '.$verify_resp['message']);
            set_output($verify_resp);
            return;
        }
        $user = $verify_resp['data']['user'];
        
        #check request params
        if(!check_parameter($params)){
            logging('error', "/api/user [GET] - Missing parameter. please check API documentation", array('page_number'=>$page_number, 'page_size'=>$page_size));
            $resp->set_response(400, "failed", "Missing parameter. please check API documentation");
            set_output($resp->get_response());
            return;
        }

        #get user
        $start  = $page_number * $page_size;
        $order  = array('field'=>'fullname', 'order'=>'DESC');
        $limit  = array('start'=>$start, 'size'=>$page_size);
        $users  = $this->user_model->get_user($search, $role_id, $order, $limit);
        $total  = $this->user_model->count_user($search);
        
        #response
        if(empty($draw)){
          logging('debug', '/api/user [GET] - Get user is success', $user);
          $resp->set_response(200, "success", "Get user is success", $user);
          set_output($resp->get_response());
          return;
        }else{
          $output['draw'] = $draw;
          logging('debug', '/api/user [GET] - Get user is success');
          $resp->set_response_datatable(200, $users, $draw, $total, $total);
          set_output($resp->get_response_datatable());
          return;
        }
    }

    #path: /api/user/id/$id [GET]
    function get_user_by_id($id){
        $resp = new Response_api();

        #check token
        $header         = $this->input->request_headers();
        $verify_resp    = verify_user_token($header, ["user_module"]);
        if($verify_resp['status'] == 'failed'){
            logging('error', '/api/user/id/'.$id.' [GET] - '.$verify_resp['message']);
            set_output($verify_resp);
            return;
        }

        #get user by id
        $user = $this->user_model->get_user_by_id($id);
        if(is_null($user)){
            logging('error', '/api/user/id/'.$id.' [GET] - user not found');
            $resp->set_response(404, "failed", "user not found");
            set_output($resp->get_response());
            return;
        }
        unset($user->password);

        #response
        logging('debug', '/api/user/id/'.$id.' [GET] - Get user by id success', $user);
        $resp->set_response(200, "success", "Get user by id success", $user);
        set_output($resp->get_response());
        return;
    }

    #path: /api/user [POST]
    function create_user(){
        $resp    = new Response_api();
        $request = json_decode($this->input->raw_input_stream, true);
        
        #check token
        $header       = $this->input->request_headers();
        $verify_resp  = verify_user_token($header, ["user_module"]);
        if($verify_resp['status'] == 'failed'){
            logging('error', '/api/user [POST] - '.$verify_resp['message']);
            set_output($verify_resp);
            return;
        }
        $user = $verify_resp['data']['user'];
        
        #check request params
        $keys = array('username', 'fullname', 'password', 'role_id');
        if(!check_parameter_by_keys($request, $keys)){
            logging('error', '/api/user [POST] - Missing parameter. please check API documentation', $request);
            $resp->set_response(400, "failed", "Missing parameter. please check API documentation");
            set_output($resp->get_response());
            return;
        }

        #check duplicate
        $user = $this->user_model->get_user_by_username($request['username']);
        if($user){
            logging('error', '/api/user [POST] - Username already registered', $request);
            $resp->set_response(400, "failed", "Username already registered");
            set_output($resp->get_response());
            return;
        }

        #check role
        $user_role = $this->user_role_model->get_user_role_by_id($request['role_id']);
        if(is_null($user_role)){
            logging('error', '/api/user [POST] - User role not found', $request);
            $resp->set_response(400, "failed", "User role not found");
            set_output($resp->get_response());
            return;
        }

        #create user
        $request['id']          = get_uniq_id();
        $request['password']    = password_hash($request['password'], PASSWORD_DEFAULT);
        $flag                   = $this->user_model->create_user($request);
        
        #response
        if(!$flag){
            logging('error', '/api/user [POST] - Internal server error', $request);
            $resp->set_response(500, "failed", "Internal server error");
            set_output($resp->get_response());
            return;
        }
        unset($request['password']);

        logging('debug', '/api/user [POST] - Create user success', $request);
        $resp->set_response(200, "success", "Create user success", $request);
        set_output($resp->get_response());
        return;
    }

    #path: /api/user [PUT]
    function update_user(){
        $resp    = new Response_api();
        $request = json_decode($this->input->raw_input_stream, true);

        #check token
        $header         = $this->input->request_headers();
        $verify_resp    = verify_user_token($header, ["user_module"]);
        if($verify_resp['status'] == 'failed'){
            logging('error', '/api/user [PUT] - '.$verify_resp['message']);
            set_output($verify_resp);
            return;
        }
        
        #check request params
        $keys = array('id', 'username', 'fullname', 'role_id');
        if(!check_parameter_by_keys($request, $keys)){
            logging('error', '/api/user [PUT] - Missing parameter. please check API documentation', $request);
            $resp->set_response(400, "failed", "Missing parameter. please check API documentation");
            set_output($resp->get_response());
            return;
        }

        #check user
        $user = $this->user_model->get_user_by_id($request['id']);
        if(is_null($user)){
            logging('error', '/api/user [PUT] - user not found', $request);
            $resp->set_response(404, "failed", "user not found");
            set_output($resp->get_response());
            return;
        }

        #check changes of username
        if($user->username != $request['username']){
            #check duplicate username
            $user_exist = $this->user_model->get_user_by_username($request['username']);
            if($user_exist){
                logging('error', '/api/user [PUT] - New username already registered', $request);
                $resp->set_response(400, "failed", "New username already registered");
                set_output($resp->get_response());
                return;
            }
        }

        #check role
        $user_role = $this->user_role_model->get_user_role_by_id($request['role_id']);
        if(is_null($user_role)){
            logging('error', '/api/user [PUT] - User role not found', $request);
            $resp->set_response(400, "failed", "User role not found");
            set_output($resp->get_response());
            return;
        }

        #update user
        $flag = $this->user_model->update_user($request, $request['id']);
        if(empty($flag)){
            logging('error', '/api/user [PUT] - Internal server error', $request);
            $resp->set_response(500, "failed", "Internal server error");
            set_output($resp->get_response());
            return;
        }
        unset($request['password']);

        logging('debug', '/api/user [PUT] - Update user success', $request);
        $resp->set_response(200, "success", "Update user success", $request);
        set_output($resp->get_response());
        return;
    }

    #path: /api/user/inactive/$id [PUT]
    function inactive_user($id){
        $resp = new Response_api();

        #check token
        $header         = $this->input->request_headers();
        $verify_resp    = verify_user_token($header, ["user_module"]);
        if($verify_resp['status'] == 'failed'){
            logging('error', '/api/user/inactive/'.$id.' [PUT] - '.$verify_resp['message']);
            set_output($verify_resp);
            return;
        }

        #check user
        $user = $this->user_model->get_user_by_id($id);
        if(is_null($user)){
            logging('error', '/api/user/inactive/'.$id.' [PUT] - user not found');
            $resp->set_response(404, "failed", "user not found");
            set_output($resp->get_response());
            return;
        }

        #inactive user
        $flag = $this->user_model->inactive_user($id);
        
        #response
        if(!$flag){
            logging('error', '/api/user/inactive/'.$id.' [PUT] - Internal server error');
            $resp->set_response(500, "failed", "Internal server error");
            set_output($resp->get_response());
            return;
        }
        logging('debug', '/api/user/inactive/'.$id.' [PUT] - inactive user success');
        $resp->set_response(200, "success", "inactive user success");
        set_output($resp->get_response());
        return;
    }

    #path: /api/user/active/$id [PUT]
    function active_user($id){
        $resp = new Response_api();

        #check token
        $header         = $this->input->request_headers();
        $verify_resp    = verify_user_token($header, ["user_module"]);
        if($verify_resp['status'] == 'failed'){
            logging('error', '/api/user/active/'.$id.' [PUT] - '.$verify_resp['message']);
            set_output($verify_resp);
            return;
        }

        #check user
        $user = $this->user_model->get_user_by_id($id);
        if(is_null($user)){
            logging('error', '/api/user/active/'.$id.' [PUT] - user not found');
            $resp->set_response(404, "failed", "user not found");
            set_output($resp->get_response());
            return;
        }

        #active user
        $flag = $this->user_model->active_user($id);
        
        #response
        if(empty($flag)){
            logging('error', '/api/user/active/'.$id.' [PUT] - Internal server error');
            $resp->set_response(500, "failed", "Internal server error");
            set_output($resp->get_response());
            return;
        }
        logging('debug', '/api/user/active/'.$id.' [PUT] - active user success');
        $resp->set_response(200, "success", "active user success");
        set_output($resp->get_response());
        return;
    }
    
    #path: /api/user/password [PUT]
    function change_password_user(){
        $resp       = new Response_api();
        $request    = json_decode($this->input->raw_input_stream, true);

        #check token
        $header      = $this->input->request_headers();
        $verify_resp = verify_user_token($header);
        if($verify_resp['status'] == 'failed'){
            logging('error', '/api/user/password [PUT] - '.$verify_resp['message']);
            set_output($verify_resp);
            return;
        }
        $user = $verify_resp['data']['user'];

        #check request params
        $keys = array('password');
        if(!check_parameter_by_keys($request, $keys)){
            logging('error', '/api/user/password [PUT] - Missing parameter. please check API documentation', $request);
            $resp->set_response(400, "failed", "Missing parameter. please check API documentation");
            set_output($resp->get_response());
            return;
        }

        #update user
        $hash                   = password_hash($request['password'], PASSWORD_DEFAULT);
        $request['password']    = $hash;
        $request['id']          = $user->id;
        $flag                   = $this->user_model->change_password_user($hash, $user->id);
        
        #response
        if(empty($flag)){
            logging('error', '/api/user/password [PUT] - Internal server error', $request);
            $resp->set_response(500, "failed", "Internal server error");
            set_output($resp->get_response());
            return;
        }

        logging('debug', '/api/user/password [PUT] - Change password user success', $request);
        $resp->set_response(200, "success", "Change password user success");
        set_output($resp->get_response());
        return;
    }

    #path: /api/user/login [POST]
    function login(){
        #init req & resp
        $resp = new Response_api();
        $request = json_decode($this->input->raw_input_stream, true);
        
        #check request payload
        $keys = array('username', 'password');
        if(!check_parameter_by_keys($request, $keys)){
            logging('error', '/api/user/login [POST] - Missing parameter. please check API documentation', $request);
            $resp->set_response(400, "failed", "Missing parameter. please check API documentation");
            set_output($resp->get_response());
            return;
        }

        #init variable
        $username   = $request['username'];
        $password   = $request['password'];

        #get user
        $user = $this->user_model->get_user_by_username($username);
        if(is_null($user)){
            logging('error', '/api/user/login [POST] - user not found', $request);
            $resp->set_response(404, "failed", "user not found");
            set_output($resp->get_response());
            return;
        }

        #check user
        $verify = password_verify($password, $user->password);
        if(!$verify){
            logging('error', '/api/user/login [POST] - Invalid password', $request);
            $resp->set_response(400, "failed", "Invalid password");
            set_output($resp->get_response());
            return;
        }
        if(!$user->is_active){
            logging('error', '/api/user/login [POST] - Account is Inactive', $request);
            $resp->set_response(400, "failed", "Account is Inactive");
            set_output($resp->get_response());
            return;
        }

        #Create user token      
        $token          = create_token($username);
        $user_access    = $this->user_access_model->get_user_access_by_role_id($user->role_id);
        $token['user']  = $user;
        $token['access']= $user_access;
        
        logging('debug', '/api/user/login [POST] - user login success', $token);
        $resp->set_response(200, "success", "user login success", $token);
        set_output($resp->get_response());
        return;
    }

    #path: user/refresh [GET]
    function refresh(){
      $resp = new Response_api();

      #check header
      $header = $this->input->request_headers();
      if(isset($header['Authorization'])){
        list(, $token) = explode(' ', $header['Authorization']);
      }else{
        logging('debug', '/api/user/refresh [GET] - Please use token to access this resource.');
        $resp->set_response(401, "failed", "Please use token to access this resource.");
        set_output($resp->get_response());
        return;
      }

      try {
        $jwt = JWT::decode($token, REFRESH_TOKEN_SALT, ['HS256']);
      } catch (Exception $e) {
        logging('debug', '/api/user/refresh [GET] - Invalid requested token');
        $resp->set_response(401, "failed", "Invalid requested token");
        set_output($resp->get_response());
        return;
      }
      $username = $jwt->username;
      $user     = $this->user_model->get_user_by_username($username);

      #check user exist
      if(is_null($user)){
        logging('debug', '/api/user/refresh [GET] - user not found');
        $resp->set_response(401, "failed", "user not found");
        set_output($resp->get_response());
        return;
      }
      if(!$user->is_active){
        logging('error', '/api/user/login [POST] - User is Inactive', $request);
        $resp->set_response(400, "failed", "User is Inactive");
        set_output($resp->get_response());
        return;
      }
      
      #generate new token      
      $token          = create_token($username);
      $user_access    = $this->user_access_model->get_user_access_by_role_id($user->role_id);
      $token['user']  = $user;
      $token['access']= $user_access;
      
      logging('debug', '/api/user/refresh [GET] - Refresh user success', $token);
      $resp->set_response(200, "success", "Refresh user success", $token);
      set_output($resp->get_response());
      return;
    }

    #path: /api/user/profile [GET]
    function get_profile(){
        $resp = new Response_api();
  
        #check token
        $header      = $this->input->request_headers();
        $verify_resp = verify_user_token($header);
        if($verify_resp['status'] == 'failed'){
            logging('error', '/api/user/profile [GET] - '.$verify_resp['message']);
            set_output($verify_resp);
            return;
        }
        $user = $verify_resp['data']['user'];
        unset($user->password);
  
        #response
        logging('debug', '/api/user/profile [GET] - Get profile success', $user);
        $resp->set_response(200, "success", "Get profile success", $user);
        set_output($resp->get_response());
        return;
    }
}