<?php
defined('BASEPATH') OR exit('No direct script access allowed');
if (isset($_SERVER['HTTP_ORIGIN'])) {
    header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
    header('Access-Control-Allow-Credentials: true');
    header('Access-Control-Max-Age: 86400');    // cache for 1 day
}

// Access-Control headers are received during OPTIONS requests
if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {

    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
        header("Access-Control-Allow-Methods: GET, POST, PUT, DELETE, OPTIONS");         

    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
        header("Access-Control-Allow-Headers: {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");

    exit(0);
}

class Education_history extends CI_Controller {
    #path: /api/education-history/employee/$1 [GET]
    function get_education_history_by_employee($employee_id){
        #init req & res
        $resp_obj   = new Response_api();
        
        #check token
        $header = $this->input->request_headers();
        $resp   = verify_user_token($header, ["user_module", "self_module"]);
        if($resp['status'] == 'failed'){
            logging('error', '/api/education-history/employee/$id [GET] - '.$resp['message']);
            set_output($resp);
            return;
        }

        $education_history = $this->education_history_model->get_education_history(null, $employee_id);

        logging('debug', '/api/education-history/employee/$id [GET] - Get education history by employee success');
        $resp_obj->set_response(200, "success", "Get education history by employee success", $education_history);
        set_output($resp_obj->get_response());
        return;
    }

    #path: /api/education-history/id/$id [GET]
    function get_education_history_by_id($id){
        $resp_obj = new Response_api();

        #check token
        $header = $this->input->request_headers();
        $resp   = verify_user_token($header, ["user_module", "self_module"]);
        if($resp['status'] == 'failed'){
            logging('error', '/api/education-history/id/'.$id.' [GET] - '.$resp['message']);
            set_output($resp);
            return;
        }

        #check education history 
        $education_history = $this->education_history_model->get_education_history_by_id($id);
        if(is_null($education_history)){
            logging('error', '/api/education-history/id/'.$id.' [GET] - education history not found');
            $resp_obj->set_response(404, "failed", "education history not found");
            set_output($resp_obj->get_response());
            return;
        }

        logging('debug', '/api/education-history/id/'.$id.' [GET] - Get education history by id success');
        $resp_obj->set_response(200, "success", "Get education history by id success", $education_history);
        set_output($resp_obj->get_response());
        return;
    }

    #path: /api/education-history [POST]
    function add_education_history(){
        #init req & res
        $resp_obj   = new Response_api();
        $request    = json_decode($this->input->raw_input_stream, true);
        
        #check token
        $header = $this->input->request_headers();
        $resp = verify_user_token($header, ["user_module", "self_module"]);
        if($resp['status'] == 'failed'){
            logging('error', '/api/education-history [POST] - '.$resp['message']);
            set_output($resp);
            return;
        }
        
        #check request params
        $keys = array('employee_id', 'start_year', 'end_year', 'level', 'school', 'major');
        $param_res = check_parameter_by_keysV2($request, $keys);
        if(!$param_res['success']){
            logging('error', '/api/education-history [POST] - Missing parameter. please check API documentation', $request);
            $resp_obj->set_response(400, "failed", $param_res['message']);
            set_output($resp_obj->get_response());
            return;
        }


        #create education history
        $request['id']  = get_uniq_id();
        $flag           = $this->education_history_model->create_education_history($request);
        
        #response
        if(!$flag){
            logging('error', '/api/education-history [POST] - Internal server error', $request);
            $resp_obj->set_response(500, "failed", "Internal server error");
            set_output($resp_obj->get_response());
            return;
        }
        logging('debug', '/api/education-history [POST] - Add education history success', $request);
        $resp_obj->set_response(200, "success", "Add education history success", $request);
        set_output($resp_obj->get_response());
        return;
    }

    #path: /api/education-history [PUT]
    function edit_education_history(){
        #init req & res
        $resp_obj   = new Response_api();
        $request    = json_decode($this->input->raw_input_stream, true);
        
        #check token
        $header = $this->input->request_headers();
        $resp = verify_user_token($header, ["user_module", "self_module"]);
        if($resp['status'] == 'failed'){
            logging('error', '/api/education-history [PUT] - '.$resp['message']);
            set_output($resp);
            return;
        }
        
        #check request params
        $keys = array('id', 'employee_id', 'start_year', 'end_year', 'level', 'school', 'major');
        if(!check_parameter_by_keys($request, $keys)){
            logging('error', '/api/education-history [PUT] - Missing parameter. please check API documentation', $request);
            $resp_obj->set_response(400, "failed", "Missing parameter. please check API documentation");
            set_output($resp_obj->get_response());
            return;
        }

        #check education history 
        $education_history = $this->education_history_model->get_education_history_by_id($request['id']);
        if(is_null($education_history)){
            logging('error', '/api/education-history [PUT] - education history not found');
            $resp_obj->set_response(404, "failed", "education history not found");
            set_output($resp_obj->get_response());
            return;
        }

        #check employee
        $employee = $this->employee_model->get_employee_by_id($request['employee_id']);
        if(is_null($employee)){
            logging('error', '/api/education-history [PUT] - employee not found');
            $resp_obj->set_response(404, "failed", "employee not found");
            set_output($resp_obj->get_response());
            return;
        }

        #create education history
        $flag = $this->education_history_model->update_education_history($request, $request['id']);
        
        #response
        if(!$flag){
            logging('error', '/api/education-history [PUT] - Internal server error', $request);
            $resp_obj->set_response(500, "failed", "Internal server error");
            set_output($resp_obj->get_response());
            return;
        }
        logging('debug', '/api/education-history [PUT] - Edit education history success', $request);
        $resp_obj->set_response(200, "success", "Edit education history success", $request);
        set_output($resp_obj->get_response());
        return;
    }

    #path: /api/education-history/delete/$id [DELETE]
    function remove_education_history($id){
        $resp_obj = new Response_api();

        #check token
        $header = $this->input->request_headers();
        $resp = verify_user_token($header, ["user_module", "self_module"]);
        if($resp['status'] == 'failed'){
            logging('error', '/api/education-history/delete/'.$id.' [DELETE] - '.$resp['message']);
            set_output($resp);
            return;
        }

        #check education history 
        $education_history = $this->education_history_model->get_education_history_by_id($id);
        if(is_null($education_history)){
            logging('error', '/api/education-history/delete/'.$id.' [DELETE] - education history not found');
            $resp_obj->set_response(404, "failed", "education history not found");
            set_output($resp_obj->get_response());
            return;
        }

        #delete education history 
        $flag = $this->education_history_model->delete_education_history($id);
        if(empty($flag)){
            logging('error', '/api/education-history/delete/'.$id.' [DELETE] - Internal server error');
            $resp_obj->set_response(500, "failed", "Internal server error");
            set_output($resp_obj->get_response());
            return;
        }
        logging('debug', '/api/education-history/delete/'.$id.' [DELETE] - Delete education history success');
        $resp_obj->set_response(200, "success", "Delete education history success");
        set_output($resp_obj->get_response());
        return;
    }
}
?>