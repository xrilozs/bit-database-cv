<?php
defined('BASEPATH') OR exit('No direct script access allowed');
if (isset($_SERVER['HTTP_ORIGIN'])) {
    header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
    header('Access-Control-Allow-Credentials: true');
    header('Access-Control-Max-Age: 86400');    // cache for 1 day
}

// Access-Control headers are received during OPTIONS requests
if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {

    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
        header("Access-Control-Allow-Methods: GET, POST, PUT, DELETE, OPTIONS");         

    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
        header("Access-Control-Allow-Headers: {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");

    exit(0);
}

class Working_history extends CI_Controller {
    #path: /api/working-history/employee/$1 [GET]
    function get_working_history_by_employee($employee_id){
        #init req & res
        $resp_obj   = new Response_api();
        
        #check token
        $header = $this->input->request_headers();
        $resp   = verify_user_token($header, ['employee_module', 'self_module']);
        if($resp['status'] == 'failed'){
            logging('error', '/api/working-history/employee/$id [GET] - '.$resp['message']);
            set_output($resp);
            return;
        }

        $working_history = $this->working_history_model->get_working_history(null, $employee_id);

        logging('debug', '/api/working-history/employee/$id [GET] - Get working history by employee success');
        $resp_obj->set_response(200, "success", "Get working history by employee success", $working_history);
        set_output($resp_obj->get_response());
        return;
    }

    #path: /api/working-history/id/$id [GET]
    function get_working_history_by_id($id){
        $resp_obj = new Response_api();

        #check token
        $header = $this->input->request_headers();
        $resp   = verify_user_token($header, ['employee_module', 'self_module']);
        if($resp['status'] == 'failed'){
            logging('error', '/api/working-history/id/'.$id.' [GET] - '.$resp['message']);
            set_output($resp);
            return;
        }

        #check working history 
        $working_history = $this->working_history_model->get_working_history_by_id($id);
        if(is_null($working_history)){
            logging('error', '/api/working-history/id/'.$id.' [GET] - working history not found');
            $resp_obj->set_response(404, "failed", "working history not found");
            set_output($resp_obj->get_response());
            return;
        }

        logging('debug', '/api/working-history/id/'.$id.' [GET] - Get working history by id success');
        $resp_obj->set_response(200, "success", "Get working history by id success", $working_history);
        set_output($resp_obj->get_response());
        return;
    }

    #path: /api/working-history [POST]
    function add_working_history(){
        #init req & res
        $resp_obj   = new Response_api();
        $request    = json_decode($this->input->raw_input_stream, true);
        
        #check token
        $header = $this->input->request_headers();
        $resp = verify_user_token($header, ['employee_module', 'self_module']);
        if($resp['status'] == 'failed'){
            logging('error', '/api/working-history [POST] - '.$resp['message']);
            set_output($resp);
            return;
        }
        
        #check request params
        $keys = array('employee_id', 'company', 'position', 'start_year', 'is_still_working');
        if(!check_parameter_by_keys($request, $keys)){
            logging('error', '/api/working-history [POST] - Missing parameter. please check API documentation', $request);
            $resp_obj->set_response(400, "failed", "Missing parameter. please check API documentation");
            set_output($resp_obj->get_response());
            return;
        }


        #create education history
        $request['id']  = get_uniq_id();
        $flag           = $this->working_history_model->create_working_history($request);
        
        #response
        if(!$flag){
            logging('error', '/api/working-history [POST] - Internal server error', $request);
            $resp_obj->set_response(500, "failed", "Internal server error");
            set_output($resp_obj->get_response());
            return;
        }
        logging('debug', '/api/working-history [POST] - Add working history success', $request);
        $resp_obj->set_response(200, "success", "Add working history success", $request);
        set_output($resp_obj->get_response());
        return;
    }

    #path: /api/working-history [PUT]
    function edit_working_history(){
        #init req & res
        $resp_obj   = new Response_api();
        $request    = json_decode($this->input->raw_input_stream, true);
        
        #check token
        $header = $this->input->request_headers();
        $resp = verify_user_token($header, ['employee_module', 'self_module']);
        if($resp['status'] == 'failed'){
            logging('error', '/api/working-history [PUT] - '.$resp['message']);
            set_output($resp);
            return;
        }
        
        #check request params
        $keys = array('employee_id', 'company', 'position', 'start_year', 'is_still_working');
        if(!check_parameter_by_keys($request, $keys)){
            logging('error', '/api/working-history [PUT] - Missing parameter. please check API documentation', $request);
            $resp_obj->set_response(400, "failed", "Missing parameter. please check API documentation");
            set_output($resp_obj->get_response());
            return;
        }

        #check working history 
        $working_history = $this->working_history_model->get_working_history_by_id($request['id']);
        if(is_null($working_history)){
            logging('error', '/api/working-history [PUT] - working history not found');
            $resp_obj->set_response(404, "failed", "working history not found");
            set_output($resp_obj->get_response());
            return;
        }

        #check employee
        $employee = $this->employee_model->get_employee_by_id($request['employee_id']);
        if(is_null($employee)){
            logging('error', '/api/working-history [PUT] - employee not found');
            $resp_obj->set_response(404, "failed", "employee not found");
            set_output($resp_obj->get_response());
            return;
        }

        #create education history
        $flag = $this->working_history_model->update_working_history($request, $request['id']);
        
        #response
        if(!$flag){
            logging('error', '/api/working-history [PUT] - Internal server error', $request);
            $resp_obj->set_response(500, "failed", "Internal server error");
            set_output($resp_obj->get_response());
            return;
        }
        logging('debug', '/api/working-history [PUT] - Edit working history success', $request);
        $resp_obj->set_response(200, "success", "Edit working history success", $request);
        set_output($resp_obj->get_response());
        return;
    }

    #path: /api/working-history/delete/$id [DELETE]
    function remove_working_history($id){
        $resp_obj = new Response_api();

        #check token
        $header = $this->input->request_headers();
        $resp = verify_user_token($header, ['employee_module', 'self_module']);
        if($resp['status'] == 'failed'){
            logging('error', '/api/working-history/delete/'.$id.' [DELETE] - '.$resp['message']);
            set_output($resp);
            return;
        }

        #check working history 
        $working_history = $this->working_history_model->get_working_history_by_id($id);
        if(is_null($working_history)){
            logging('error', '/api/working-history/delete/'.$id.' [DELETE] - working history not found');
            $resp_obj->set_response(404, "failed", "working history not found");
            set_output($resp_obj->get_response());
            return;
        }

        #delete working history 
        $flag = $this->working_history_model->delete_working_history($id);
        if(empty($flag)){
            logging('error', '/api/working-history/delete/'.$id.' [DELETE] - Internal server error');
            $resp_obj->set_response(500, "failed", "Internal server error");
            set_output($resp_obj->get_response());
            return;
        }
        logging('debug', '/api/working-history/delete/'.$id.' [DELETE] - Delete working history success');
        $resp_obj->set_response(200, "success", "Delete working history success");
        set_output($resp_obj->get_response());
        return;
    }
}
?>