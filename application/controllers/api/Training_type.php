<?php
defined('BASEPATH') OR exit('No direct script access allowed');
if (isset($_SERVER['HTTP_ORIGIN'])) {
    header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
    header('Access-Control-Allow-Credentials: true');
    header('Access-Control-Max-Age: 86400');
}

if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {

    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
        header("Access-Control-Allow-Methods: GET, POST, PUT, DELETE, OPTIONS");         

    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
        header("Access-Control-Allow-Headers:        {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");

    exit(0);
}

class Training_type extends CI_Controller {
    public function __construct($config = 'rest'){
      parent::__construct($config);
    }

    #path: /api/training-type [GET]
    function get_training_type(){
        $resp = new Response_api();

        #init variable
        $page_number  = $this->input->get('page_number');
        $page_size    = $this->input->get('page_size');
        $search       = $this->input->get('search');
        $draw         = $this->input->get('draw');
        // $params       = array($page_number, $page_size);

        #check token
        $header      = $this->input->request_headers();
        $verify_resp = verify_user_token($header);
        if($verify_resp['status'] == 'failed'){
            logging('error', '/api/training-type [GET] - '.$verify_resp['message']);
            set_output($verify_resp);
            return;
        }
        $user = $verify_resp['data']['user'];
        
        #check request params
        // if(!check_parameter($params)){
        //     logging('error', "/api/training-type [GET] - Missing parameter. please check API documentation", array('page_number'=>$page_number, 'page_size'=>$page_size));
        //     $resp->set_response(400, "failed", "Missing parameter. please check API documentation");
        //     set_output($resp->get_response());
        //     return;
        // }

        #get training type
        $limit = null;
        if($page_number && $page_size){
            $start = $page_number * $page_size;
            $limit = array('start'=>$start, 'size'=>$page_size);
        }

        $order          = array('field'=>'name', 'order'=>'DESC');
        $training_types = $this->training_type_model->get_training_type($search, $order, $limit);
        $total          = $this->training_type_model->count_training_type($search);

        #response
        if(empty($draw)){
          logging('debug', '/api/training-type [GET] - Get training type is success');
          $resp->set_response(200, "success", "Get training type is success", $training_types);
          set_output($resp->get_response());
          return;
        }else{
          $output['draw'] = $draw;
          logging('debug', '/api/training-type [GET] - Get training type is success');
          $resp->set_response_datatable(200, $training_types, $draw, $total, $total);
          set_output($resp->get_response_datatable());
          return;
        }
    }

    #path: /api/training-type/id/$id [GET]
    function get_training_type_by_id($id){
        $resp = new Response_api();

        #check token
        $header         = $this->input->request_headers();
        $verify_resp    = verify_user_token($header, ["training_module"]);
        if($verify_resp['status'] == 'failed'){
            logging('error', '/api/training-type/id/'.$id.' [GET] - '.$verify_resp['message']);
            set_output($verify_resp);
            return;
        }

        #get training type by id
        $training_type = $this->training_type_model->get_training_type_by_id($id);
        if(is_null($training_type)){
            logging('error', '/api/training-type/id/'.$id.' [GET] - training type not found');
            $resp->set_response(404, "failed", "training type not found");
            set_output($resp->get_response());
            return;
        }
        unset($training_type->password);

        #response
        logging('debug', '/api/training-type/id/'.$id.' [GET] - Get training type by id success', $training_type);
        $resp->set_response(200, "success", "Get training type by id success", $training_type);
        set_output($resp->get_response());
        return;
    }

    #path: /api/training-type [POST]
    function create_training_type(){
        $resp    = new Response_api();
        $request = json_decode($this->input->raw_input_stream, true);
        
        #check token
        $header       = $this->input->request_headers();
        $verify_resp  = verify_user_token($header, ["training_module"]);
        if($verify_resp['status'] == 'failed'){
            logging('error', '/api/training-type [POST] - '.$verify_resp['message']);
            set_output($verify_resp);
            return;
        }
        $user = $verify_resp['data']['user'];
        
        #check request params
        $keys = array('name');
        if(!check_parameter_by_keys($request, $keys)){
            logging('error', '/api/training-type [POST] - Missing parameter. please check API documentation', $request);
            $resp->set_response(400, "failed", "Missing parameter. please check API documentation");
            set_output($resp->get_response());
            return;
        }

        #create training type
        $request['id']  = get_uniq_id();
        $flag           = $this->training_type_model->create_training_type($request);
        
        #response
        if(!$flag){
            logging('error', '/api/training-type [POST] - Internal server error', $request);
            $resp->set_response(500, "failed", "Internal server error");
            set_output($resp->get_response());
            return;
        }

        logging('debug', '/api/training-type [POST] - Create training type success', $request);
        $resp->set_response(200, "success", "Create training type success", $request);
        set_output($resp->get_response());
        return;
    }

    #path: /api/training-type [PUT]
    function update_training_type(){
        $resp    = new Response_api();
        $request = json_decode($this->input->raw_input_stream, true);

        #check token
        $header         = $this->input->request_headers();
        $verify_resp    = verify_user_token($header, ["training_module"]);
        if($verify_resp['status'] == 'failed'){
            logging('error', '/api/training-type [PUT] - '.$verify_resp['message']);
            set_output($verify_resp);
            return;
        }
        
        #check request params
        $keys = array('id', 'name');
        if(!check_parameter_by_keys($request, $keys)){
            logging('error', '/api/training-type [PUT] - Missing parameter. please check API documentation', $request);
            $resp->set_response(400, "failed", "Missing parameter. please check API documentation");
            set_output($resp->get_response());
            return;
        }

        #check training type
        $training_type = $this->training_type_model->get_training_type_by_id($request['id']);
        if(is_null($training_type)){
            logging('error', '/api/training-type [PUT] - training type not found', $request);
            $resp->set_response(404, "failed", "user not found");
            set_output($resp->get_response());
            return;
        }

        #update training type
        $flag = $this->training_type_model->update_training_type($request, $request['id']);
        if(empty($flag)){
            logging('error', '/api/training-type [PUT] - Internal server error', $request);
            $resp->set_response(500, "failed", "Internal server error");
            set_output($resp->get_response());
            return;
        }

        logging('debug', '/api/training-type [PUT] - Update training type success', $request);
        $resp->set_response(200, "success", "Update training type success", $request);
        set_output($resp->get_response());
        return;
    }

    #path: /api/training-type/delete/$id [DELETE]
    function delete_training_type($id){
        $resp_obj = new Response_api();

        #check token
        $header = $this->input->request_headers();
        $resp   = verify_user_token($header, ["training_module"]);
        if($resp['status'] == 'failed'){
            logging('error', '/api/training-type/delete/'.$id.' [DELETE] - '.$resp['message']);
            set_output($resp);
            return;
        }

        #check training type
        $training_type = $this->training_type_model->get_training_type_by_id($id);
        if(is_null($training_type)){
            logging('error', '/api/training-type/delete/'.$id.' [DELETE] - training type not found');
            $resp_obj->set_response(404, "failed", "training type not found");
            set_output($resp_obj->get_response());
            return;
        }

        #update training type
        $flag = $this->training_type_model->delete_training_type($id);
        
        #response
        if(empty($flag)){
            logging('error', '/api/training-type/delete/'.$id.' [DELETE] - Internal server error');
            $resp_obj->set_response(500, "failed", "Internal server error");
            set_output($resp_obj->get_response());
            return;
        }
        logging('debug', '/api/training-type/delete/'.$id.' [DELETE] - Delete training type success');
        $resp_obj->set_response(200, "success", "Delete training type success");
        set_output($resp_obj->get_response());
        return;
    }
}

?>