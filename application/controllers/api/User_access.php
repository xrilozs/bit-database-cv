<?php
defined('BASEPATH') OR exit('No direct script access allowed');
if (isset($_SERVER['HTTP_ORIGIN'])) {
    header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
    header('Access-Control-Allow-Credentials: true');
    header('Access-Control-Max-Age: 86400');
}

if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {

    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
        header("Access-Control-Allow-Methods: GET, POST, PUT, DELETE, OPTIONS");         

    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
        header("Access-Control-Allow-Headers:        {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");

    exit(0);
}

class User_access extends CI_Controller {
    public function __construct($config = 'rest'){
      parent::__construct($config);
    }

    #path: /api/user-access [GET]
    function get_user_access(){
        $resp = new Response_api();

        #init variable
        $page_number  = $this->input->get('page_number');
        $page_size    = $this->input->get('page_size');
        $search       = $this->input->get('search');
        $draw         = $this->input->get('draw');
        $params       = array($page_number, $page_size);

        #check token
        $header      = $this->input->request_headers();
        $verify_resp = verify_user_token($header, ["user_module"]);
        if($verify_resp['status'] == 'failed'){
            logging('error', '/api/user-access [GET] - '.$verify_resp['message']);
            set_output($verify_resp);
            return;
        }
        $user = $verify_resp['data']['user'];
        
        #check request params
        if(!check_parameter($params)){
            logging('error', "/api/user-access [GET] - Missing parameter. please check API documentation", array('page_number'=>$page_number, 'page_size'=>$page_size));
            $resp->set_response(400, "failed", "Missing parameter. please check API documentation");
            set_output($resp->get_response());
            return;
        }

        #get user access
        $start          = $page_number * $page_size;
        $limit          = array('start'=>$start, 'size'=>$page_size);
        $order          = array('field'=>'id', 'order'=>'DESC');
        $user_accesss   = $this->user_access_model->get_user_access($search, $order, $limit);
        $total          = $this->user_access_model->count_user_access($search);

        #response
        if(empty($draw)){
          logging('debug', '/api/user-access [GET] - Get user access is success', $user_access);
          $resp->set_response(200, "success", "Get user access is success", $user_access);
          set_output($resp->get_response());
          return;
        }else{
          $output['draw'] = $draw;
          logging('debug', '/api/user-access [GET] - Get user access is success');
          $resp->set_response_datatable(200, $user_accesss, $draw, $total, $total);
          set_output($resp->get_response_datatable());
          return;
        }
    }

    #path: /api/user-access/id/$id [GET]
    function get_user_access_by_id($id){
        $resp = new Response_api();

        #check token
        $header      = $this->input->request_headers();
        $verify_resp = verify_user_token($header, ["user_module"]);
        if($verify_resp['status'] == 'failed'){
            logging('error', '/api/user-access/id/$id [GET] - '.$verify_resp['message']);
            set_output($verify_resp);
            return;
        }
        $user = $verify_resp['data']['user'];

        #get user access
        $user_access = $this->user_access_model->get_user_access_by_id($id);
        if(is_null($user_access)){
            logging('error', '/api/user-access/id/$id [GET] - user access is not found', $request);
            $resp->set_response(404, "failed", " user access is not found");
            set_output($resp->get_response());
            return;
        }
        
        #response
        logging('debug', '/api/user-access/id/$id [GET] - Get user access by id is success', $user_access);
        $resp->set_response(200, "success", "Get user access by id is success", $user_access);
        set_output($resp->get_response());
        return;
    }

    #path: /api/user-access/role/$role [GET]
    function get_user_access_by_role($role_id){
        $resp = new Response_api();

        #check token
        $header      = $this->input->request_headers();
        $verify_resp = verify_user_token($header, ["user_module"]);
        if($verify_resp['status'] == 'failed'){
            logging('error', '/api/user-access/role/$access [GET] - '.$verify_resp['message']);
            set_output($verify_resp);
            return;
        }
        $user = $verify_resp['data']['user'];

        #get user access
        $user_accesss = $this->user_access_model->get_user_access_by_role_id($role_id);
        if(is_null($user_access)){
            logging('error', '/api/user-access/access/$access [GET] - user access is not found', $request);
            $resp->set_response(404, "failed", " user access is not found");
            set_output($resp->get_response());
            return;
        }
        
        #response
        logging('debug', '/api/user-access/access/$access [GET] - Get user access by role is success', $user_access);
        $resp->set_response(200, "success", "Get user access by role is success", $user_access);
        set_output($resp->get_response());
        return;
    }

    #path: /api/user-access [POST]
    function create_user_access(){
        $resp    = new Response_api();
        $request = json_decode($this->input->raw_input_stream, true);
        
        #check token
        $header       = $this->input->request_headers();
        $verify_resp  = verify_user_token($header, ["user_module"]);
        if($verify_resp['status'] == 'failed'){
            logging('error', '/api/user-access [POST] - '.$verify_resp['message']);
            set_output($verify_resp);
            return;
        }
        $user = $verify_resp['data']['user'];
        
        #check request params
        $keys = array('role_id', 'user_module', 'employee_module', 'search_module', 'self_module', 'config_module');
        if(!check_parameter_by_keys($request, $keys)){
            logging('error', '/api/user-access [POST] - Missing parameter. please check API documentation', $request);
            $resp->set_response(400, "failed", "Missing parameter. please check API documentation");
            set_output($resp->get_response());
            return;
        }

        #check duplicate
        $user_role = $this->user_access_model->get_user_access_by_role_id($request['role_id']);
        if($user_role){
            logging('error', '/api/user-access [POST] - role access is duplicate', $request);
            $resp->set_response(400, "failed", "role access is duplicate");
            set_output($resp->get_response());
            return;
        }

        #create user access
        $request['id']  = get_uniq_id();
        $flag           = $this->user_access_model->create_user_access($request);
        
        #response
        if(!$flag){
            logging('error', '/api/user-access [POST] - Internal server error', $request);
            $resp->set_response(500, "failed", "Internal server error");
            set_output($resp->get_response());
            return;
        }

        logging('debug', '/api/user-access [POST] - Create user access success', $request);
        $resp->set_response(200, "success", "Create user access success", $request);
        set_output($resp->get_response());
        return;
    }

    #path: /api/user-access [PUT]
    function update_user_access(){
        $resp    = new Response_api();
        $request = json_decode($this->input->raw_input_stream, true);

        #check token
        $header         = $this->input->request_headers();
        $verify_resp    = verify_user_token($header, ["user_module"]);
        if($verify_resp['status'] == 'failed'){
            logging('error', '/api/user-access [PUT] - '.$verify_resp['message']);
            set_output($verify_resp);
            return;
        }
        
        #check request params
        $keys = array('id', 'role_id', 'user_module', 'employee_module', 'search_module', 'self_module', 'config_module');
        if(!check_parameter_by_keys($request, $keys)){
            logging('error', '/api/user-access [PUT] - Missing parameter. please check API documentation', $request);
            $resp->set_response(400, "failed", "Missing parameter. please check API documentation");
            set_output($resp->get_response());
            return;
        }

        #check user access
        $user_access = $this->user_access_model->get_user_access_by_id($request['id']);
        if(is_null($user_access)){
            logging('error', '/api/user-access [PUT] - user access not found', $request);
            $resp->set_response(404, "failed", "user access not found");
            set_output($resp->get_response());
            return;
        }

        #check user role
        $user_role = $this->user_role_model->get_user_role_by_id($request['role_id']);
        if(is_null($user_role)){
            logging('error', '/api/user-access [POST] - user role not found');
            $resp->set_response(400, "failed", "user role not found");
            set_output($resp->get_response());
            return;
        }

        #check duplicate
        if($user_access->role_id != $request['role_id']){
            $user_access_exist = $this->user_access_model->get_user_access_by_role_id($request['role_id']);
            if($user_access_exist){
                logging('error', '/api/user-access [PUT] - New role access is duplicate', $request);
                $resp->set_response(400, "failed", "New role access is duplicate");
                set_output($resp->get_response());
                return;
            }
        }

        #update user access
        $flag = $this->user_access_model->update_user_access($request, $request['id']);
        if(empty($flag)){
            logging('error', '/api/user-access [PUT] - Internal server error', $request);
            $resp->set_response(500, "failed", "Internal server error");
            set_output($resp->get_response());
            return;
        }

        logging('debug', '/api/user-access [PUT] - Update user access success', $request);
        $resp->set_response(200, "success", "Update user access success", $request);
        set_output($resp->get_response());
        return;
    }
}

?>