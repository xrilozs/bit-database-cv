<?php
defined('BASEPATH') OR exit('No direct script access allowed');
if (isset($_SERVER['HTTP_ORIGIN'])) {
    header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
    header('Access-Control-Allow-Credentials: true');
    header('Access-Control-Max-Age: 86400');
}

if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {

    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
        header("Access-Control-Allow-Methods: GET, POST, PUT, DELETE, OPTIONS");         

    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
        header("Access-Control-Allow-Headers:        {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");

    exit(0);
}

class User_role extends CI_Controller {
    public function __construct($config = 'rest'){
      parent::__construct($config);
    }

    #path: /api/user-role [GET]
    function get_user_role(){
        $resp = new Response_api();

        #init variable
        $page_number  = $this->input->get('page_number');
        $page_size    = $this->input->get('page_size');
        $search       = $this->input->get('search');
        $draw         = $this->input->get('draw');
        // $params       = array($page_number, $page_size);

        #check token
        $header      = $this->input->request_headers();
        $verify_resp = verify_user_token($header, ["user_module"]);
        if($verify_resp['status'] == 'failed'){
            logging('error', '/api/user-role [GET] - '.$verify_resp['message']);
            set_output($verify_resp);
            return;
        }
        $user = $verify_resp['data']['user'];
        
        #check request params
        // if(!check_parameter($params)){
        //     logging('error', "/api/user-role [GET] - Missing parameter. please check API documentation", array('page_number'=>$page_number, 'page_size'=>$page_size));
        //     $resp->set_response(400, "failed", "Missing parameter. please check API documentation");
        //     set_output($resp->get_response());
        //     return;
        // }

        #get user role
        $limit = null;
        if($page_number && $page_size){
            $start = $page_number * $page_size;
            $limit = array('start'=>$start, 'size'=>$page_size);
        }

        $order          = array('field'=>'role', 'order'=>'DESC');
        $user_roles     = $this->user_role_model->get_user_role($search, $order, $limit);
        $total          = $this->user_role_model->count_user_role($search);

        #response
        if(empty($draw)){
          logging('debug', '/api/user-role [GET] - Get user role is success');
          $resp->set_response(200, "success", "Get user role is success", $user_roles);
          set_output($resp->get_response());
          return;
        }else{
          $output['draw'] = $draw;
          logging('debug', '/api/user-role [GET] - Get user role is success');
          $resp->set_response_datatable(200, $user_roles, $draw, $total, $total);
          set_output($resp->get_response_datatable());
          return;
        }
    }

    #path: /api/user-role/id/$id [GET]
    function get_user_role_by_id($id){
        $resp = new Response_api();

        #check token
        $header         = $this->input->request_headers();
        $verify_resp    = verify_user_token($header, ["user_module"]);
        if($verify_resp['status'] == 'failed'){
            logging('error', '/api/user-role/id/'.$id.' [GET] - '.$verify_resp['message']);
            set_output($verify_resp);
            return;
        }

        #get user role by id
        $user_role = $this->user_role_model->get_user_role_by_id($id);
        if(is_null($user_role)){
            logging('error', '/api/user-role/id/'.$id.' [GET] - user role not found');
            $resp->set_response(404, "failed", "user role not found");
            set_output($resp->get_response());
            return;
        }
        unset($user_role->password);

        #response
        logging('debug', '/api/user-role/id/'.$id.' [GET] - Get user role by id success', $user_role);
        $resp->set_response(200, "success", "Get user role by id success", $user_role);
        set_output($resp->get_response());
        return;
    }

    #path: /api/user-role [POST]
    function create_user_role(){
        $resp    = new Response_api();
        $request = json_decode($this->input->raw_input_stream, true);
        
        #check token
        $header       = $this->input->request_headers();
        $verify_resp  = verify_user_token($header, ["user_module"]);
        if($verify_resp['status'] == 'failed'){
            logging('error', '/api/user-role [POST] - '.$verify_resp['message']);
            set_output($verify_resp);
            return;
        }
        $user = $verify_resp['data']['user'];
        
        #check request params
        $keys = array('role');
        if(!check_parameter_by_keys($request, $keys)){
            logging('error', '/api/user-role [POST] - Missing parameter. please check API documentation', $request);
            $resp->set_response(400, "failed", "Missing parameter. please check API documentation");
            set_output($resp->get_response());
            return;
        }

        #check duplicate
        $user_role = $this->user_role_model->get_user_role_by_role($request['role']);
        if($user_role){
            logging('error', '/api/user-role [POST] - role is duplicate', $request);
            $resp->set_response(400, "failed", "role is duplicate");
            set_output($resp->get_response());
            return;
        }

        #create user role
        $request['id']  = get_uniq_id();
        $flag           = $this->user_role_model->create_user_role($request);
        
        #response
        if(!$flag){
            logging('error', '/api/user-role [POST] - Internal server error', $request);
            $resp->set_response(500, "failed", "Internal server error");
            set_output($resp->get_response());
            return;
        }

        logging('debug', '/api/user-role [POST] - Create user role success', $request);
        $resp->set_response(200, "success", "Create user role success", $request);
        set_output($resp->get_response());
        return;
    }

    #path: /api/user-role [PUT]
    function update_user_role(){
        $resp    = new Response_api();
        $request = json_decode($this->input->raw_input_stream, true);

        #check token
        $header         = $this->input->request_headers();
        $verify_resp    = verify_user_token($header, ["user_module"]);
        if($verify_resp['status'] == 'failed'){
            logging('error', '/api/user-role [PUT] - '.$verify_resp['message']);
            set_output($verify_resp);
            return;
        }
        
        #check request params
        $keys = array('id', 'role');
        if(!check_parameter_by_keys($request, $keys)){
            logging('error', '/api/user-role [PUT] - Missing parameter. please check API documentation', $request);
            $resp->set_response(400, "failed", "Missing parameter. please check API documentation");
            set_output($resp->get_response());
            return;
        }

        #check user role
        $user_role = $this->user_role_model->get_user_role_by_id($request['id']);
        if(is_null($user_role)){
            logging('error', '/api/user-role [PUT] - user role not found', $request);
            $resp->set_response(404, "failed", "user not found");
            set_output($resp->get_response());
            return;
        }

        #check duplicate
        if($user_role->role != $request['role']){
            $user_role_exist = $this->user_role_model->get_user_role_by_role($request['role']);
            if($user_role_exist){
                logging('error', '/api/user-role [PUT] - New role is duplicate', $request);
                $resp->set_response(400, "failed", "New role is duplicate");
                set_output($resp->get_response());
                return;
            }
        }

        #update user role
        $flag = $this->user_role_model->update_user_role($request, $request['id']);
        if(empty($flag)){
            logging('error', '/api/user-role [PUT] - Internal server error', $request);
            $resp->set_response(500, "failed", "Internal server error");
            set_output($resp->get_response());
            return;
        }

        logging('debug', '/api/user-role [PUT] - Update user role success', $request);
        $resp->set_response(200, "success", "Update user role success", $request);
        set_output($resp->get_response());
        return;
    }

    #path: /api/user-role/delete/$id [DELETE]
    function delete_user_role($id){
        $resp_obj = new Response_api();

        #check token
        $header = $this->input->request_headers();
        $resp   = verify_user_token($header, ["user_module"]);
        if($resp['status'] == 'failed'){
            logging('error', '/api/user-role/delete/'.$id.' [DELETE] - '.$resp['message']);
            set_output($resp);
            return;
        }

        #check user role
        $user_role = $this->user_role_model->get_user_role_by_id($id);
        if(is_null($user_role)){
            logging('error', '/api/user-role/delete/'.$id.' [DELETE] - user role not found');
            $resp_obj->set_response(404, "failed", "user role not found");
            set_output($resp_obj->get_response());
            return;
        }

        #update user role
        $flag = $this->user_role_model->delete_user_role($id);
        
        #response
        if(empty($flag)){
            logging('error', '/api/user-role/delete/'.$id.' [DELETE] - Internal server error');
            $resp_obj->set_response(500, "failed", "Internal server error");
            set_output($resp_obj->get_response());
            return;
        }
        logging('debug', '/api/user-role/delete/'.$id.' [DELETE] - Delete user role success');
        $resp_obj->set_response(200, "success", "Delete user role success");
        set_output($resp_obj->get_response());
        return;
    }
}

?>