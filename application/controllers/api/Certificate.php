<?php
defined('BASEPATH') OR exit('No direct script access allowed');
if (isset($_SERVER['HTTP_ORIGIN'])) {
    header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
    header('Access-Control-Allow-Credentials: true');
    header('Access-Control-Max-Age: 86400');    // cache for 1 day
}

// Access-Control headers are received during OPTIONS requests
if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {

    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
        header("Access-Control-Allow-Methods: GET, POST, PUT, DELETE, OPTIONS");         

    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
        header("Access-Control-Allow-Headers: {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");

    exit(0);
}

class Certificate extends CI_Controller {
    #path: /api/certificate/employee/$1 [GET]
    function get_certificate_by_employee($employee_id){
        #init req & res
        $resp_obj   = new Response_api();
        
        #check token
        $header = $this->input->request_headers();
        $resp = verify_user_token($header, ["user_module", "self_module"]);
        if($resp['status'] == 'failed'){
            logging('error', '/api/certificate/employee/$employee_id [GET] - '.$resp['message']);
            set_output($resp);
            return;
        }

        $certificates = $this->certificate_model->get_certificate($employee_id);

        logging('debug', '/api/certificate/$id [GET] - Get certificate by employee success');
        $resp_obj->set_response(200, "success", "Get certificate by employee success", $certificates);
        set_output($resp_obj->get_response());
        return;
    }

    #path: /api/certificate [POST]
    function add_certificate(){
        #init req & res
        $resp_obj   = new Response_api();
        $request    = json_decode($this->input->raw_input_stream, true);
        
        #check token
        $header = $this->input->request_headers();
        $resp = verify_user_token($header, ["user_module", "self_module"]);
        if($resp['status'] == 'failed'){
            logging('error', '/api/certificate [POST] - '.$resp['message']);
            set_output($resp);
            return;
        }
        
        #check request params
        $keys = array('employee_id', 'file_url');
        if(!check_parameter_by_keys($request, $keys)){
            logging('error', '/api/certificate [POST] - Missing parameter. please check API documentation', $request);
            $resp_obj->set_response(400, "failed", "Missing parameter. please check API documentation");
            set_output($resp_obj->get_response());
            return;
        }


        #create photo
        $request['id']  = get_uniq_id();
        $flag           = $this->certificate_model->create_certificate($request);
        
        #response
        if(!$flag){
            logging('error', '/api/certificate [POST] - Internal server error', $request);
            $resp_obj->set_response(500, "failed", "Internal server error");
            set_output($resp_obj->get_response());
            return;
        }
        logging('debug', '/api/certificate [POST] - Add certificate success', $request);
        $resp_obj->set_response(200, "success", "Add certificate success", $request);
        set_output($resp_obj->get_response());
        return;
    }

    #path: /api/certificate/delete/$id [DELETE]
    function remove_certificate($id){
        $resp_obj = new Response_api();

        #check token
        $header = $this->input->request_headers();
        $resp = verify_user_token($header, ["user_module", "self_module"]);
        if($resp['status'] == 'failed'){
            logging('error', '/api/certificate/delete/'.$id.' [DELETE] - '.$resp['message']);
            set_output($resp);
            return;
        }

        #check employee 
        $certificate = $this->certificate_model->get_certificate_by_id($id);
        if(is_null($certificate)){
            logging('error', '/api/certificate/delete/'.$id.' [DELETE] - certificate  not found');
            $resp_obj->set_response(404, "failed", "certificate  not found");
            set_output($resp_obj->get_response());
            return;
        }

        #delete employee 
        $flag = $this->certificate_model->delete_certificate($id);
        if(empty($flag)){
            logging('error', '/api/certificate/delete/'.$id.' [DELETE] - Internal server error');
            $resp_obj->set_response(500, "failed", "Internal server error");
            set_output($resp_obj->get_response());
            return;
        }
        logging('debug', '/api/certificate/delete/'.$id.' [DELETE] - Delete certificate success');
        $resp_obj->set_response(200, "success", "Delete certificate success");
        set_output($resp_obj->get_response());
        return;
    }

    #path: /api/certificate/upload [POST]
    function upload_certificate(){
        #init variable
        $resp_obj       = new Response_api();
        $employee_id    = $this->input->get('employee_id');

        #check token
        $header = $this->input->request_headers();
        $resp = verify_user_token($header, ["user_module", "self_module"]);
        if($resp['status'] == 'failed'){
            logging('error', '/api/certificate/upload [POST] - '.$resp['message']);
            set_output($resp);
            return;
        }

        #get employee detail
        $employee = $this->employee_model->get_employee_by_id($employee_id);
        if(is_null($employee)){
            logging('error', '/api/certificate/upload [GET] - employee not found');
            $resp_obj->set_response(404, "failed", "employee not found");
            set_output($resp_obj->get_response());
            return;
        }

        #check requested param
        $destination = 'assets/certificate/';
        if (empty($_FILES['file']['name'])) {
            logging('error', '/api/certificate/upload [POST] - Missing parameter. please check API documentation');
            $resp_obj->set_response(400, "failed", "Missing parameter. please check API documentation");
            set_output($resp_obj->get_response());
            return;   
        }

        #upload 
        $file = $_FILES['file'];
        $resp = _upload_file($file, $destination);

        #response
        if($resp['status'] == 'failed'){
            logging('error', '/api/certificate/upload [POST] - '.$resp['message']);
            $resp_obj->set_response(400, "failed", $resp['message']);
            set_output($resp_obj->get_response());
            return; 
        }

        #create photo
        $request    = array(
            'id'            => get_uniq_id(),
            'employee_id'   => $employee_id,
            'file_url'      => $resp['data']['file_url'],
            'file_size'     => convertByte($file['size']),
            'file_name'     => $file['name']
        );
        $flag       = $this->certificate_model->create_certificate($request);
        
        #response
        if(!$flag){
            logging('error', '/api/certificate/upload [POST] - Internal server error', $request);
            $resp_obj->set_response(500, "failed", "Internal server error");
            set_output($resp_obj->get_response());
            return;
        }
        
        logging('debug', '/api/certificate/upload [POST] - Upload certificate success', $request);
        $resp_obj->set_response(200, "success", "Upload certificate success", $request);
        set_output($resp_obj->get_response());
        return;
    }
}
?>