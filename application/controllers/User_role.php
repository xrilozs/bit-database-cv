<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User_role extends CI_Controller {

	public function page(){
		$data = array(
			"js_file" 	=> array(
				base_url('assets/js/user_role.js?v=').time()
			),
			"title"		=> "User Role"
		);

		$this->load->view('layout/main_header', $data);
		$this->load->view('layout/main_sidebar', $data);
		$this->load->view('pages/user_role/page');
		$this->load->view('layout/main_footer', $data);
	}
}
