<?php
    defined('BASEPATH') OR exit('No direct script access allowed');

    class Employee extends CI_Controller {
        public function page(){
            $data = array(
                "js_file" => array(
                    base_url('assets/js/employee.js?v=').time()
                ),
                "title"		=> "Karyawan"
            );
    
            $this->load->view('layout/main_header', $data);
            $this->load->view('layout/main_sidebar', $data);
            $this->load->view('pages/employee/page');
            $this->load->view('layout/main_footer', $data);
        }

        public function detail(){
            $data = array(
                "js_file" => array(
                    base_url('assets/js/certificate.js?v=').time(),
                    base_url('assets/js/employee_project.js?v=').time(),
                    base_url('assets/js/employee_training.js?v=').time(),
                    base_url('assets/js/employee_working.js?v=').time(),
                    base_url('assets/js/employee_education.js?v=').time(),
                    base_url('assets/js/employee_detail.js?v=').time()
                ),
                "title"		=> "Detail Karyawan"
            );
    
            $this->load->view('layout/main_header', $data);
            $this->load->view('layout/main_sidebar', $data);
            $this->load->view('pages/employee/detail');
            $this->load->view('layout/main_footer', $data);
            $this->load->view('components/education_modal');
            $this->load->view('components/working_modal');
            $this->load->view('components/training_modal');
            $this->load->view('components/project_modal');
            $this->load->view('components/certificate_modal');
        }

        public function advance_search(){
            $data = array(
                "js_file" => array(
                    base_url('assets/js/advance_search.js?v=').time()
                ),
                "title"		=> "Pencarian Karyawan"
            );
    
            $this->load->view('layout/main_header', $data);
            $this->load->view('layout/main_sidebar', $data);
            $this->load->view('pages/employee/advance_search');
            $this->load->view('layout/main_footer', $data);
        }

        public function self_service(){
            $data = array(
                "js_file" => array(
                    base_url('assets/js/certificate.js?v=').time(),
                    base_url('assets/js/employee_project.js?v=').time(),
                    base_url('assets/js/employee_training.js?v=').time(),
                    base_url('assets/js/employee_working.js?v=').time(),
                    base_url('assets/js/employee_education.js?v=').time(),
                    base_url('assets/js/self_service.js?v=').time()
                ),
                "title"		=> "Self Service"
            );
    
            $this->load->view('layout/main_header', $data);
            $this->load->view('layout/main_sidebar', $data);
            $this->load->view('pages/employee/self_service');
            $this->load->view('layout/main_footer', $data);
            $this->load->view('components/education_modal');
            $this->load->view('components/working_modal');
            $this->load->view('components/training_modal');
            $this->load->view('components/project_modal');
            $this->load->view('components/certificate_modal');
        }
    }
?>