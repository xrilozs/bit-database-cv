<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Training_type extends CI_Controller {

	public function page(){
		$data = array(
			"js_file" 	=> array(
				base_url('assets/js/training_type.js?v=').time()
			),
			"title"		=> "Jenis Pelatihan"
		);

		$this->load->view('layout/main_header', $data);
		$this->load->view('layout/main_sidebar', $data);
		$this->load->view('pages/training_type/page');
		$this->load->view('layout/main_footer', $data);
	}
}
