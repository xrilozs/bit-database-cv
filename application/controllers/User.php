<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {

	public function page(){
		$data = array(
			"js_file" 	=> array(
				base_url('assets/js/user.js?v=').time()
			),
			"title"		=> "User"
		);

		$this->load->view('layout/main_header', $data);
		$this->load->view('layout/main_sidebar', $data);
		$this->load->view('pages/user/page');
		$this->load->view('layout/main_footer', $data);
	}
}
