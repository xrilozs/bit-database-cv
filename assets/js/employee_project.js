let PROJECT_ID
let JOBDESK = [""]

$(document).ready(function(){

  $('#add-jobdesk-create').click(function(){
    JOBDESK.push("")
    renderJobdesk("create")
  })

  $('#add-jobdesk-update').click(function(){
    JOBDESK.push("")
    renderJobdesk("update")
  })

  $("body").delegate(".jobdesk-item", "keyup", function(e) {
    let id      = $(this).data("id")
    JOBDESK[id] = $(this).val()
  })
  
  $("body").delegate(".jobdesk-delete-button", "click", function(e) {
    let id = $(this).data("id")
    let type = $(this).data("type")
    JOBDESK.splice(id, 1)
    renderJobdesk(type)
  })

    $('#project-create-toggle').click(function(){
        clearFormProject('create')
    })

    $("body").delegate(".project-update-toggle", "click", function(e) {
        PROJECT_ID = $(this).data('id')
        clearFormProject('update')
    
        $('#project-update-overlay').show()
        $.ajax({
            async: true,
            url: `${PROJECT_HISTORY_API_URL}/id/${PROJECT_ID}`,
            type: 'GET',
            beforeSend: function (xhr) {
              xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
            },
            error: function(res) {
              const response = JSON.parse(res.responseText)
              let isRetry = retryRequest(response)
              if(isRetry) $.ajax(this)
              else $('#project-update-modal').modal('toggle')
            },
            success: function(res) {
              const response = res.data
              $('#project-update-overlay').hide()
              renderFormProject(response)
            }
        });
    })
    
    $("body").delegate(".project-delete-toggle", "click", function(e) {
        PROJECT_ID = $(this).data('id')
    })

    $('#project-create-form').submit(function (e){
        e.preventDefault()
        let jobdeskStr = ""
        if(JOBDESK.length <= 0){
          showError("Jobdesk harus diisi!")
          return
        }else{
          JOBDESK.forEach((item, i) => {
            if(i != 0){
              jobdeskStr += "<br>"
            }
            jobdeskStr += item
          })
        }

        startLoadingButton('#project-create-button')
    
        let $form = $(this)
        let data = {
          employee_id: EMPLOYEE_ID,
          title: $form.find( "input[name='title']" ).val(),
          location: $form.find( "input[name='location']" ).val(),
          position: $form.find( "input[name='position']" ).val(),
          start_project: formatDateReverse($form.find( "input[name='start_project']" ).val()),
          end_project: formatDateReverse($form.find( "input[name='end_project']" ).val()),
          jobdesk: jobdeskStr
        }

    
        $.ajax({
            async: true,
            url: PROJECT_HISTORY_API_URL,
            type: 'POST',
            beforeSend: function (xhr) {
              xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
            },
            data: JSON.stringify(data),
            error: function(res) {
              const response = JSON.parse(res.responseText)
              let isRetry = retryRequest(response)
              if(isRetry) $.ajax(this)
              else endLoadingButton('#project-create-button', 'Simpan')
            },
            success: function(res) {
              endLoadingButton('#project-create-button', 'Simpan')
              showSuccess(res.message)
              $('#project-create-modal').modal('toggle')
              getEmployeeDetail()
            }
        });
    })

    $('#project-update-form').submit(function (e){
        e.preventDefault()

        let jobdeskStr = ""
        if(JOBDESK.length <= 0){
          showError("Jobdesk harus diisi!")
          return
        }else{
          JOBDESK.forEach((item, i) => {
            if(i != 0){
              jobdeskStr += "<br>"
            }
            jobdeskStr += item
          })
        }

        startLoadingButton('#project-update-button')
    
        let $form = $(this)
        let data = {
          id: PROJECT_ID,
          employee_id: EMPLOYEE_ID,
          title: $form.find( "input[name='title']" ).val(),
          location: $form.find( "input[name='location']" ).val(),
          position: $form.find( "input[name='position']" ).val(),
          start_project: formatDateReverse($form.find( "input[name='start_project']" ).val()),
          end_project: formatDateReverse($form.find( "input[name='end_project']" ).val()),
          jobdesk: jobdeskStr
        }
    
        $.ajax({
            async: true,
            url: PROJECT_HISTORY_API_URL,
            type: 'PUT',
            beforeSend: function (xhr) {
              xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
            },
            data: JSON.stringify(data),
            error: function(res) {
              const response = JSON.parse(res.responseText)
              let isRetry = retryRequest(response)
              if(isRetry) $.ajax(this)
              else endLoadingButton('#project-update-button', 'Simpan')
            },
            success: function(res) {
              endLoadingButton('#project-update-button', 'Simpan')
              showSuccess(res.message)
              $('#project-update-modal').modal('toggle')
              getEmployeeDetail()
            }
        });
    })

    $('#project-delete-button').click(function (){
        startLoadingButton('#project-delete-button')
        
        $.ajax({
            async: true,
            url: `${PROJECT_HISTORY_API_URL}/delete/${PROJECT_ID}`,
            type: 'DELETE',
            beforeSend: function (xhr) {
              xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
            },
            error: function(res) {
              const response = JSON.parse(res.responseText)
              let isRetry = retryRequest(response)
              if(isRetry) $.ajax(this)
              else endLoadingButton('#project-delete-button', 'Iya')
            },
            success: function(res) {
              endLoadingButton('#project-delete-button', 'Iya')
              showSuccess(res.message)
              $('#project-delete-modal').modal('toggle')
              getEmployeeDetail()
            }
        });
    })
})

function renderProject(data){
    if(data.length > 0){
        let content_html = ``
        data.forEach(item => {
          let jobdeskArr  = item.jobdesk.split("<br>")
          let jobdeskHtml = ``
          jobdeskArr.forEach(item => {
            let itemHtml =  `<tr><td>${item}</td></tr>`
            jobdeskHtml += itemHtml
          })

          let item_html = `<table class="table table-bordered table-hover" id="project-datatable">
            <thead>
            <tr>
                <th>Detail</th>
                <th>Deskripsi</th>
                <th rowspan="7">Aksi</th>
            </tr>
            </thead>
            <tbody>
                <tr>
                  <td>Nama Projek</td>
                  <td>${item.title}</td>
                  <td rowspan="${7+jobdeskArr.length}" class="text-center">
                    <a href="#" class="btn btn-success text-white project-update-toggle" data-id="${item.id}" data-toggle="modal" data-target="#project-update-modal" title="ubah">
                        <i class="fa fa-edit"></i>
                    </a>
                    <a href="#" class="btn btn-danger text-white project-delete-toggle" data-id="${item.id}" data-toggle="modal" data-target="#project-delete-modal" title="hapus">
                        <i class="fa fa-trash"></i>
                    </a>
                  </td>
                </tr>
                <tr>
                  <td>Lokasi Projek</td>
                  <td>${item.location}</td>
                </tr>
                <tr>
                  <td>Posisi</td>
                  <td>${item.position}</td>
                </tr>
                <tr>
                  <td>Mulai Projek</td>
                  <td>${item.start_project}</td>
                </tr>
                <tr>
                  <td>Selesai Projek</td>
                  <td>${item.end_project}</td>
                </tr>
                <tr>
                  <td rowspan="${jobdeskArr.length+1}">Jobdesk</td>
                </tr>
                ${jobdeskHtml}
            </tbody>
          </table>`

          content_html += item_html
        });

        $('#project-history-table').html(content_html)
    }
}

function renderFormProject(data){
    let $form = $(`#project-update-form`)    
    $form.find( "input[name='title']" ).val(data.title)
    $form.find( "input[name='location']" ).val(data.location)
    $form.find( "input[name='position']" ).val(data.position)

    JOBDESK = data.jobdesk.split("<br>")
    renderJobdesk("update")
    // if(type == 'update'){
      $('#project-start-project-update-field').datepicker({
        format: 'dd-mm-yyyy',
        orientation: 'bottom',
        autoclose: true
      })
      $('#project-end-project-update-field').datepicker({
        format: 'dd-mm-yyyy',
        orientation: 'bottom',
        autoclose: true
      })
      $('#project-start-project-update-field').datepicker("setDate", new Date(data.start_project));
      $('#project-end-project-update-field').datepicker("setDate", new Date(data.end_project));
    // }
}
  
function clearFormProject(type){
  $(`#project-${type}-form`)[0].reset();

  if(type == 'create'){
    JOBDESK = [""]
    renderJobdesk("create")

    $('.datepicker').datepicker({
      format: 'dd-mm-yyyy',
      orientation: 'bottom',
      autoclose: true
    })
    $('.datepicker').datepicker("setDate", new Date());
  }
}

function renderJobdesk(type){
  let jobdeskHtml = ``
  JOBDESK.forEach((item, i) => {
    let itemHtml = `<input type="text" name="jobdesk-${type}-${i}" class="form-control mb-3 jobdesk-item" id="project-jobdesk-${type}-field-${i}"  placeholder="Jobdesk.." data-id="${i}" value="${item}" required>`
    if(i != 0){
      itemHtml = `<div class="input-group mb-3">
        <input type="text" name="jobdesk-${type}-${i}" class="form-control jobdesk-item" id="project-jobdesk-${type}-field-${i}"  placeholder="Jobdesk.." data-id="${i}" value="${item}" required>
        <div class="input-group-append">
          <button type="button" class="btn btn-danger jobdesk-delete-button" data-id="${i}" data-type="${type}">
            <i class="fas fa-trash"></i>
          </button>
        </div>
      </div>`
    }
    jobdeskHtml += itemHtml
  })

  if(jobdeskHtml == ""){
    jobdeskHtml = `<input type="text" name="jobdesk-${type}-0" class="form-control jobdesk-item" id="project-jobdesk-${type}-field-0"  placeholder="Jobdesk.." data-id="0" value="" required>`
  }

  $(`#jobdesk-${type}`).html(jobdeskHtml)
}