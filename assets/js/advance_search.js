let TYPE = null

$(document).ready(function(){
    getTrainingOption()

    let employee_table = $('#employee-datatable').DataTable( {
        processing: true,
        serverSide: true,
        searching: true,
        ajax: {
          async: true,
          url: EMPLOYEE_API_URL,
          type: "GET",
          dataType: "json",
          crossDomain: true,
          beforeSend: function (xhr) {
            xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
          },
          data: function ( d ) {
            let newObj          = {}
            let start           = d.start
            let size            = d.length
            newObj.page_number  = d.start > 0 ? (start/size) : 0;
            newObj.page_size    = size
            newObj.search       = d.search.value
            newObj.type         = TYPE ? TYPE.toString() : TYPE;
            newObj.draw         = d.draw
            d                   = newObj
            console.log("D itu:", d)
            return d
          },
          error: function(res) {
            const response = JSON.parse(res.responseText)
            let isRetry = retryRequest(response)
            if(isRetry) $.ajax(this)
          },
        },
        columns: [
            {
              data: "img_url",
              render: function (data, type, row, meta) {
                return `<img src="${WEB_URL+"/"+data}" class="img-responsive" style="width:200px;">`
              }
            },
            { 
              data: "fullname",
              orderable: false
            },
            { 
              data: "total_work_year",
              render: function (data, type, row, meta) {
                return data ? `${data} Tahun` : "-"
              },
              orderable: false
            },
            { 
              data: "dob",
              render: function (data, type, row, meta) {
                return formatDateReverse(data)
              },
              orderable: false
            },
            { 
              data: "city",
              orderable: false
            },
            { 
              data: "gender",
              render: function (data, type, row, meta) {
                return data == 'L' ? 'Laki-laki' : 'Perempuan'
              },
              orderable: false
            },
            { 
              data: "address",
              render: function (data, type, row, meta) {
                return sortText(data, 20)
              },
              orderable: false
            },
            { 
              data: "phone",
              orderable: false
            },
            { 
              data: "email",
              orderable: false
            },
            { 
              data: "nationality",
              orderable: false
            },
            { 
              data: "education",
              orderable: false
            },
            {
              data: "created_at",
              orderable: false
            },
            {
              data: "id",
              render: function (data, type, row, meta) {
                return `<button type="button" class="btn btn-primary print-cv-button" data-id="${data}">
                            <i class="nav-icon fas fa-file-word"></i>&nbsp;&nbsp;Cetak CV
                        </button>`
              },
              orderable: false
            }
        ]
    });

    $('#training-option').change(function(){
        TYPE = $(this).val()
        console.log(TYPE)
        if(TYPE.length >= 1){
          $('#search-button').prop("disabled", false)
        }else{
          $('#search-button').prop("disabled", true)
        }
    })

    $('#search-form').submit(function(e){
        e.preventDefault()
        startLoadingButton('#search-button')
        employee_table.ajax.reload()
        endLoadingButton("#search-button", `<i class="fas fa-search"></i> Cari`)
        $('#employee-table').show()
    })
})

function getTrainingOption(){
    $.ajax({
        async: true,
        url: `${TRAINING_HISTORY_API_URL}/option`,
        type: 'GET',
        beforeSend: function (xhr) {
          xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
        },
        error: function(res) {
          const response = JSON.parse(res.responseText)
          let isRetry = retryRequest(response)
          if(isRetry) $.ajax(this)
          else showError("Upload gambar", "Gagal mendapatkan opsi!")
        },
        success: function(res) {
          const response = res.data
          renderTrainingOption(response)
        }
    });
}

function renderTrainingOption(data){
    let training_html = "<option readonly>--Pilih Tipe Pelatihan--</option>"
    data.forEach(item => {
      let option_html   = `<option value="${item.type}">${item.type}</option>`
      training_html     += option_html
    });
    $(`#training-option`).html(training_html)
}