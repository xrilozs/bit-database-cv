let WORKING_ID
let IS_STILL_WORKING = 0

$(document).ready(function(){
    $('#still-working-create').click(function(){
      // Check if the checkbox is checked
      if ($(this).is(':checked')) {
        // Show the content if checked
        $('#end-year-create-section').hide();
        $('#working-end-year-create-field').removeAttr('required');
        IS_STILL_WORKING = 1
      } else {
        // Hide the content if unchecked
        $('#end-year-create-section').show();
        $('#working-end-year-create-field').attr('required', true);
        IS_STILL_WORKING = 0
      }
    });

    $('#still-working-update').click(function(){
      // Check if the checkbox is checked
      if ($(this).is(':checked')) {
        // Show the content if checked
        $('#end-year-update-section').hide();
        $('#working-end-year-update-field').removeAttr('required');
        IS_STILL_WORKING = 1
      } else {
        // Hide the content if unchecked
        $('#end-year-update-section').show();
        $('#working-end-year-update-field').attr('required', true);
        IS_STILL_WORKING = 0
      }
    });

    $('#working-create-toggle').click(function(){
        clearFormWorking('create')
    })

    $("body").delegate(".working-update-toggle", "click", function(e) {
        WORKING_ID = $(this).data('id')
        clearFormWorking('update')
    
        $('#working-update-overlay').show()
        $.ajax({
            async: true,
            url: `${WORKING_HISTORY_API_URL}/id/${WORKING_ID}`,
            type: 'GET',
            beforeSend: function (xhr) {
              xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
            },
            error: function(res) {
              const response = JSON.parse(res.responseText)
              let isRetry = retryRequest(response)
              if(isRetry) $.ajax(this)
              else $('#working-update-modal').modal('toggle')
            },
            success: function(res) {
              const response = res.data
              $('#working-update-overlay').hide()
              renderFormWorking(response)
            }
        });
    })
    
    $("body").delegate(".working-delete-toggle", "click", function(e) {
        WORKING_ID = $(this).data('id')
    })

    $('#working-create-form').submit(function (e){
        e.preventDefault()
        startLoadingButton('#working-create-button')
    
        let $form = $(this)
        let data = {
          employee_id: EMPLOYEE_ID,
          company: $form.find( "input[name='company']" ).val(),
          position: $form.find( "input[name='position']" ).val(),
          start_year: $form.find( "input[name='start_year']" ).val(),
          is_still_working: IS_STILL_WORKING
        }

        if(!IS_STILL_WORKING){
          data.end_year = $form.find( "input[name='end_year']" ).val()
        }
    
        $.ajax({
            async: true,
            url: WORKING_HISTORY_API_URL,
            type: 'POST',
            beforeSend: function (xhr) {
              xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
            },
            data: JSON.stringify(data),
            error: function(res) {
              const response = JSON.parse(res.responseText)
              let isRetry = retryRequest(response)
              if(isRetry) $.ajax(this)
              else endLoadingButton('#working-create-button', 'Simpan')
            },
            success: function(res) {
              endLoadingButton('#working-create-button', 'Simpan')
              showSuccess(res.message)
              $('#working-create-modal').modal('toggle')
              getEmployeeDetail()
            }
        });
    })

    $('#working-update-form').submit(function (e){
        e.preventDefault()
        startLoadingButton('#working-update-button')
    
        let $form = $(this)
        let data = {
          id: WORKING_ID,
          employee_id: EMPLOYEE_ID,
          company: $form.find( "input[name='company']" ).val(),
          position: $form.find( "input[name='position']" ).val(),
          start_year: $form.find( "input[name='start_year']" ).val(),
          is_still_working: IS_STILL_WORKING
        }

        if(!IS_STILL_WORKING){
          data.end_year = $form.find( "input[name='end_year']" ).val()
        }
    
        $.ajax({
            async: true,
            url: WORKING_HISTORY_API_URL,
            type: 'PUT',
            beforeSend: function (xhr) {
              xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
            },
            data: JSON.stringify(data),
            error: function(res) {
              const response = JSON.parse(res.responseText)
              let isRetry = retryRequest(response)
              if(isRetry) $.ajax(this)
              else endLoadingButton('#working-update-button', 'Simpan')
            },
            success: function(res) {
              endLoadingButton('#working-update-button', 'Simpan')
              showSuccess(res.message)
              $('#working-update-modal').modal('toggle')
              getEmployeeDetail()
            }
        });
    })

    $('#working-delete-button').click(function (){
        startLoadingButton('#working-delete-button')
        
        $.ajax({
            async: true,
            url: `${WORKING_HISTORY_API_URL}/delete/${WORKING_ID}`,
            type: 'DELETE',
            beforeSend: function (xhr) {
              xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
            },
            error: function(res) {
              const response = JSON.parse(res.responseText)
              let isRetry = retryRequest(response)
              if(isRetry) $.ajax(this)
              else endLoadingButton('#working-delete-button', 'Iya')
            },
            success: function(res) {
              endLoadingButton('#working-delete-button', 'Iya')
              showSuccess(res.message)
              $('#working-delete-modal').modal('toggle')
              getEmployeeDetail()
            }
        });
    })
})

function renderWorking(data){
    if(data.length > 0){
        let content_html = ``
        data.forEach(item => {
            let item_html = `<tr>
                <td>${item.company}</td>
                <td>${item.position}</td>
                <td>${item.start_year}</td>
                <td>${parseInt(item.is_still_working) ? 'Masih Bekerja' : item.end_year }</td>
                <td>
                    <a href="#" class="btn btn-success text-white working-update-toggle" data-id="${item.id}" data-toggle="modal" data-target="#working-update-modal" title="ubah">
                        <i class="fa fa-edit"></i>
                    </a>
                    <a href="#" class="btn btn-danger text-white working-delete-toggle" data-id="${item.id}" data-toggle="modal" data-target="#working-delete-modal" title="hapus">
                        <i class="fa fa-trash"></i>
                    </a>
                </td>
            </tr>`
            content_html += item_html
        });

        let table_html = `<table class="table table-bordered table-hover" id="working-datatable">
            <thead>
            <tr>
                <th>Perusahaan</th>
                <th>Posisi</th>
                <th>Tahun Mulai Bekerja</th>
                <th>Tahun Selesai Bekerja</th>
                <th>Aksi</th>
            </tr>
            </thead>
            <tbody>
                ${content_html}
            </tbody>
        </table>`

        $('#working-history-table').html(table_html)
    }
}

function renderFormWorking(data){
    let $form = $(`#working-update-form`)
    $form.find( "input[name='company']" ).val(data.company);
    $form.find( "input[name='position']" ).val(data.position); 
    $form.find( "input[name='start_year']" ).val(data.start_year);
    $form.find( "input[name='end_year']" ).val(data.end_year);
    if(parseInt(data.is_still_working)){
      $('#still-working-update').prop('checked', true);
      $('#end-year-update-section').hide()
    }else{
      $('#still-working-update').prop('checked', false);
      $('#end-year-update-section').show()
    }
}
  
function clearFormWorking(type){
  $(`#working-${type}-form`)[0].reset();
  $('.datepicker').datepicker({
    format: 'dd-mm-yyyy',
    orientation: 'bottom',
    autoclose: true
  })
  $('.datepicker').datepicker("setDate", new Date());
  
}