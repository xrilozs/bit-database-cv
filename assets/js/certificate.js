let CERTIFICATE_ID

$(document).ready(function(){
  let drEvent = $('#certificate-file').dropify()
  $('#certificate-file').on('change', function() {
    // Get the selected file
    var file = this.files[0];

    // Prepare form data
    var formData = new FormData();
    formData.append('file', file);

    // Make the AJAX request
    $.ajax({
        url: `${CERTIFICATE_API_URL}/upload?employee_id=${EMPLOYEE_ID}`,
        type: 'POST',
        beforeSend: function (xhr) {
          xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
        },
        data: formData,
        contentType: false,
        processData: false,
        error: function(error) {
          const response = JSON.parse(res.responseText)
          let isRetry = retryRequest(response)
          if(isRetry) $.ajax(this)
          else{
            showError("Gagal melakukan upload sertifikat!")
            drEvent.data('dropify').clearElement();
            console.error('Error uploading file:', error);
          }
        },
        success: function(response) {
          console.log('File uploaded successfully:', response);
          drEvent.data('dropify').clearElement();
          showSuccess("Sukses melakukan upload sertifikat!")
          getEmployeeDetail()
      },
    });
  });

  $("body").delegate(".certificate-delete-toggle", "click", function(e) {
    CERTIFICATE_ID = $(this).data('id')
  })

  $('#certificate-delete-button').click(function (){
    startLoadingButton('#certificate-delete-button')
    
    $.ajax({
        async: true,
        url: `${CERTIFICATE_API_URL}/delete/${CERTIFICATE_ID}`,
        type: 'DELETE',
        beforeSend: function (xhr) {
          xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
        },
        error: function(res) {
          const response = JSON.parse(res.responseText)
          let isRetry = retryRequest(response)
          if(isRetry) $.ajax(this)
          else endLoadingButton('#certificate-delete-button', 'Iya')
        },
        success: function(res) {
          endLoadingButton('#certificate-delete-button', 'Iya')
          showSuccess(res.message)
          $('#certificate-delete-modal').modal('toggle')
          getEmployeeDetail()
        }
    });
  })
})

function renderCertificate(data){
  let documentHtml = ``
  for(const item of data){
    const itemHtml = `<div class="col-12">
      <div class="card">
        <div class="card-body">
          <div class="row">
            <div class="col-2 d-flex justify-content-center">
              <i class="far fa-file-pdf fa-5x"></i>
            </div>
            <div class="col-7">
              <h4>${item.file_name}</h4>
              <span>Ukuran: ${item.file_size}</span>
            </div>
            <div class="col-3">
              <a href="${WEB_URL+"/"+item.file_url}" class="btn btn-success float-end mt-2" target="_blank">
                <i class="fas fa-cloud-download-alt"></i> Download
              </a>
              <a href="#" class="btn btn-danger text-white certificate-delete-toggle" data-id="${item.id}" data-toggle="modal" data-target="#certificate-delete-modal" title="hapus">
                <i class="fa fa-trash"></i> Hapus
              </a>
            </div>
          </div>
        </div>
      </div>
    </div>`
    documentHtml += itemHtml
  }

  $('#certiciate-list').html(documentHtml)
}