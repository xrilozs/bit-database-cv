let USER_ACCESS_ID

$(document).ready(function(){
  getUserRole()

  //data table
  let user_access_table = $('#user-access-datatable').DataTable( {
      processing: true,
      serverSide: true,
      searching: true,
      ajax: {
        async: true,
        url: `${USER_ACCESS_API_URL}`,
        type: "GET",
        dataType: "json",
        crossDomain: true,
        beforeSend: function (xhr) {
          xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
        },
        data: function ( d ) {
          let newObj          = {}
          let start           = d.start
          let size            = d.length
          newObj.page_number  = d.start > 0 ? (start/size) : 0;
          newObj.page_size    = size
          newObj.search       = d.search.value
          newObj.draw         = d.draw
          d                   = newObj
          console.log("D itu:", d)
          return d
        },
        error: function(res) {
          const response = JSON.parse(res.responseText)
          let isRetry = retryRequest(response)
          if(isRetry) $.ajax(this)
        }
      },
      columns: [
          { 
            data: "role",
            render: function (data, type, row, meta) {
              return `<span class="badge badge-pill badge-dark">${data}</span>`
            },
            orderable: false
          },
          { 
            data: "user_module",
            render: function (data, type, row, meta) {
              return hasAccessIcon(data)
            },
            orderable: false
          },
          { 
            data: "employee_module",
            render: function (data, type, row, meta) {
              return hasAccessIcon(data)
            },
            orderable: false
          },
          { 
            data: "training_module",
            render: function (data, type, row, meta) {
              return hasAccessIcon(data)
            },
            orderable: false
          },
          { 
            data: "search_module",
            render: function (data, type, row, meta) {
              return hasAccessIcon(data)
            },
            orderable: false
          },
          { 
            data: "self_module",
            render: function (data, type, row, meta) {
              return hasAccessIcon(data)
            },
            orderable: false
          },
          { 
            data: "config_module",
            render: function (data, type, row, meta) {
              return hasAccessIcon(data)
            },
            orderable: false
          },
          {
            data: "created_at",
            orderable: false
          },
          {
            data: "id",
            className: "dt-body-right",
            render: function (data, type, row, meta) {
              let button = `<button class="btn btn-sm btn-success user-access-update-toggle" data-id="${data}" data-toggle="modal" data-target="#user-access-update-modal" title="update">
                <i class="fas fa-edit"></i>
              </button>`
              
              return button
            },
            orderable: false
          }
      ]
  });
  
  //toggle
  $('#user-access-create-toggle').click(function(e) {
    clearForm('create')  
  })
  
  $("body").delegate(".user-access-update-toggle", "click", function(e) {
    USER_ACCESS_ID = $(this).data('id')
    $('#user-access-update-overlay').show()

    $.ajax({
        async: true,
        url: `${USER_ACCESS_API_URL}/id/${USER_ACCESS_ID}`,
        type: 'GET',
        beforeSend: function (xhr) {
          xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
        },
        error: function(res) {
          const response  = JSON.parse(res.responseText)
          let isRetry     = retryRequest(response)
          if(isRetry) $.ajax(this)
          else $('#user-access-update-modal').modal('toggle')
        },
        success: function(res) {
          const response = res.data
          renderForm(response, 'update')
          $('#user-access-update-overlay').hide()  
        }
    });
  })
  
  $("body").delegate(".user-access-delete-toggle", "click", function(e) {
    USER_ACCESS_ID = $(this).data('id')
  })

  //form
  $('#user-access-create-form').submit(function (e){
    e.preventDefault();
    startLoadingButton('#user-access-create-button')
    
    let $form = $(this),
        request = {
          role_id: $("#user-access-role-create-field").find(":selected").val(),
          user_module: getCheckboxValue($form, "user_module"),
          employee_module: getCheckboxValue($form, "employee_module"),
          training_module: getCheckboxValue($form, "training_module"),
          search_module: getCheckboxValue($form, "search_module"),
          self_module: getCheckboxValue($form, "self_module"),
          config_module: getCheckboxValue($form, "config_module"),
        }

    console.log("Request", request)
        
    $.ajax({
        async: true,
        url: USER_ACCESS_API_URL,
        type: 'POST',
        beforeSend: function (xhr) {
          xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
        },
        data: JSON.stringify(request),
        error: function(res) {
          const response  = JSON.parse(res.responseText)
          let isRetry     = retryRequest(response)
          if(isRetry) $.ajax(this)
          else endLoadingButton('#user-access-create-button', 'Simpan')
        },
        success: function(res) {
          showSuccess("Membuat user access baru berhasil!")
          endLoadingButton('#user-access-create-button', 'Simpan')
          $('#user-access-create-modal').modal('hide')
          user_access_table.ajax.reload()
        }
    });
  })
  
  $('#user-access-update-form').submit(function (e){
    e.preventDefault()
    startLoadingButton('#user-access-update-button')

    let $form = $(this),
        request = {
          id: USER_ACCESS_ID,
          role_id: $("#user-access-role-update-field").find(":selected").val(),
          user_module: getCheckboxValue($form, "user_module"),
          employee_module: getCheckboxValue($form, "employee_module"),
          training_module: getCheckboxValue($form, "training_module"),
          search_module: getCheckboxValue($form, "search_module"),
          self_module: getCheckboxValue($form, "self_module"),
          config_module: getCheckboxValue($form, "config_module"),
        }

    $.ajax({
        async: true,
        url: USER_ACCESS_API_URL,
        type: 'PUT',
        beforeSend: function (xhr) {
          xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
        },
        data: JSON.stringify(request),
        error: function(res) {
          const response = JSON.parse(res.responseText)
          let isRetry    = retryRequest(response)
          if(isRetry) $.ajax(this)
          else endLoadingButton('#user-access-update-button', 'Simpan')
        },
        success: function(res) {
          showSuccess("Mengubah user access berhasil!")
          endLoadingButton('#user-access-update-button', 'Simpan')
          $('#user-access-update-modal').modal('toggle')
          user_access_table.ajax.reload()
        }
    });
  })
  
  $('#user-access-delete-button').click(function (){
    startLoadingButton('#user-access-delete-button')
    
    $.ajax({
        async: true,
        url: `${USER_ACCESS_API_URL}/delete/${USER_ACCESS_ID}`,
        type: 'DELETE',
        beforeSend: function (xhr) {
          xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
        },
        error: function(res) {
          const response = JSON.parse(res.responseText)
          let isRetry    = retryRequest(response)
          if(isRetry) $.ajax(this)
          else endLoadingButton('#user-access-delete-button', 'Simpan')
        },
        success: function(res) {
          showSuccess(`Menghapus user access berhasil!`)
          endLoadingButton('#user-access-delete-button', 'Simpan')
          $('#user-access-delete-modal').modal('toggle')
          user_access_table.ajax.reload()
        }
    });
  })
})

function renderForm(data, type){
  let $form = $(`#user-access-${type}-form`)
  $(`#user-access-role-${type}-field`).val(data.role_id).change()
  $form.find("input[name='user_module']").prop("checked", parseInt(data.user_module) ? true : false);
  $form.find("input[name='employee_module']").prop("checked", parseInt(data.employee_module) ? true : false);
  $form.find("input[name='training_module']").prop("checked", parseInt(data.training_module) ? true : false);
  $form.find("input[name='search_module']").prop("checked", parseInt(data.search_module) ? true : false);
  $form.find("input[name='self_module']").prop("checked", parseInt(data.self_module) ? true : false);
  $form.find("input[name='config_module']").prop("checked", parseInt(data.config_module) ? true : false);
}

function clearForm(type){
  console.log("CLEAR FORM ", type)
  let $form = $(`#user-access-${type}-form`)
  $form.find("input[name='user_module']").prop("checked", false);
  $form.find("input[name='employee_module']").prop("checked", false);
  $form.find("input[name='training_module']").prop("checked", false);
  $form.find("input[name='search_module']").prop("checked", false);
  $form.find("input[name='self_module']").prop("checked", false);
  $form.find("input[name='config_module']").prop("checked", false);
}

function hasAccessIcon(data){
  return parseInt(data) ? `<p align="center"><i class="fas fa-check-square" style="color:#0580bc;"></i></p>` : `<p align="center"><i class="fas fa-times-circle" style="color:red;"></i></p>`
}

function getCheckboxValue($form, name){
  return $form.find( `input[name='${name}']:checked` ).val() == 'on' ? 1 : 0;
}

function getUserRole(){
  $.ajax({
    async: true,
    url: USER_ROLE_API_URL,
    type: 'GET',
    beforeSend: function (xhr) {
      xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
    },
    error: function(res) {
      const response = JSON.parse(res.responseText)
      let isRetry    = retryRequest(response)
      if(isRetry) $.ajax(this)
    },
    success: function(res) {
      const response = res.data
      renderRoleOption(response)
    }
  });
}

function renderRoleOption(data){
  let role_html = ""
  data.forEach(item => {
    let option_html = `<option value="${item.id}">${item.role}</option>`
    role_html       += option_html
  });
  $(`.role-option`).html(role_html)
}