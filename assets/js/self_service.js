let EMPLOYEE_ID, IMG_URL

$(document).ready(function(){
    getEmployeeDetail()

    $('#employee-image').change(function(e) {
      let file = e.target.files[0];
      upload_image(file)
    });
    
    $('#print-cv-button').click(function(){
      startLoadingButton('#print-cv-button')
      $.ajax({
        async: true,
        url: `${EMPLOYEE_API_URL}/print/${EMPLOYEE_ID}`,
        type: 'GET',
        beforeSend: function (xhr) {
          xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
        },
        error: function(res) {
          const response = JSON.parse(res.responseText)
          let isRetry = retryRequest(response)
          if(isRetry) $.ajax(this)
          else endLoadingButton('#print-cv-button', '<i class="nav-icon fas fa-file-word"></i>&nbsp;&nbsp;Cetak CV')
        },
        success: function(res) {
          endLoadingButton('#print-cv-button', '<i class="nav-icon fas fa-file-word"></i>&nbsp;&nbsp;Cetak CV')
          showSuccess(res.message)
          window.location.href = WEB_URL + res.data
        }
      });
    })

    //submit form
  $('#employee-form').submit(function (e){
    e.preventDefault();
    startLoadingButton('#employee-button')
    
    let $form = $(this)
    let data = {
      img_url: IMG_URL,
      fullname: $form.find( "input[name='fullname']" ).val(),
      dob: $form.find( "input[name='dob']" ).val(),
      city: $form.find( "input[name='city']" ).val(),
      gender:  $form.find('input[name="gender"]:checked').val(),
      address: $form.find( "textarea[name='address']" ).val(),
      phone: $form.find( "input[name='phone']" ).val(),
      email: $form.find( "input[name='email']" ).val(),
      nationality: $form.find( "input[name='nationality']" ).val(),
      education: $form.find( "input[name='education']" ).val()
    }

    if(EMPLOYEE_ID){
      data.employee_id = EMPLOYEE_ID
    }

    $.ajax({
        async: true,
        url: `${EMPLOYEE_API_URL}?self-service=1`,
        type: 'POST',
        beforeSend: function (xhr) {
          xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
        },
        data: JSON.stringify(data),
        error: function(res) {
          const response = JSON.parse(res.responseText)
          let isRetry = retryRequest(response)
          if(isRetry) $.ajax(this)
          else endLoadingButton('#employee-button', 'Simpan')
        },
        success: function(res) {
          endLoadingButton('#employee-button', 'Simpan')
          showSuccess(res.message)
          getEmployeeDetail()
        }
    });
  })
})

function upload_image(file){
  $('#employee-button').attr("disabled", true)

  let formData = new FormData();
  formData.append('image', file);
  
  $.ajax({
      async: true,
      url: `${EMPLOYEE_API_URL}/upload`,
      type: 'POST',
      beforeSend: function (xhr) {
        xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
      },
      data: formData,
      processData: false, 
      contentType: false, 
      error: function(res) {
        const response = JSON.parse(res.responseText)
        let isRetry = retryRequest(response)
        if(isRetry) $.ajax(this)
        else {
          showError("Upload gambar", "Gagal melakukan upload gambar!")
          $('#employee-button').attr("disabled", false)
        }
      },
      success: function(res) {
        $('#employee-button').attr("disabled", false)

        const response = res.data
        IMG_URL        = response.file_url
        showSuccess(res.message)
      }
  });
}

function getEmployeeDetail() {
    $.ajax({
        async: true,
        url: `${EMPLOYEE_API_URL}/user?is_detail=1`,
        type: 'GET',
        beforeSend: function (xhr) {
          xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
        },
        error: function(res) {
          const response = JSON.parse(res.responseText)
          let isRetry = retryRequest(response)
          if(isRetry) $.ajax(this)
          else {
            $('#employee-detail-overlay').hide()
            $('#employee-image').dropify()
            $('.disabled-button').attr("disabled", true)
            $('.datepicker').datepicker("setDate", new Date());
            $('#certificate-section').hide()
          }
        },
        success: function(res) {
          const response = res.data
          $('#certificate-section').show()
          $('#employee-detail-overlay').hide()
          $('.disabled-button').attr("disabled", false)
          renderForm(response)
        }
    });
}

function renderForm(data){
    EMPLOYEE_ID = data.id

    let $form = $('#employee-form')
    $form.find("input[name='fullname']").val(data.fullname)
    $('.datepicker').datepicker("setDate", new Date(data.dob));
    $form.find("input[name='city']").val(data.city)
    // $form.find("input[name='gender']").val(data.gender == 'L' ? 'Laki-laki' : 'Perempuan')
    $form.find( `input[name="gender"][value="${data.gender}"]` ).prop('checked', true);
    $form.find("input[name='fullname']").val(data.fullname)
    $form.find("textarea[name='address']").val(data.address)
    $form.find("input[name='phone']").val(data.phone)
    $form.find("input[name='email']").val(data.email)
    $form.find("input[name='nationality']").val(data.nationality)
    $form.find("input[name='education']").val(data.education)
    if(data.img_url){
      $('#employee-image').dropify({
        defaultFile: `${WEB_URL}/${data.img_url}`
      });
    }

    renderEducation(data.education_history)
    renderWorking(data.working_history)
    renderTraining(data.training_history)
    renderProject(data.project_history)
    renderCertificate(data.certificate)
}
