let EMPLOYEE_ID  = window.location.pathname.split("/").pop()

$(document).ready(function(){
    getEmployeeDetail()
    $('.print-cv-button').attr('data-id', EMPLOYEE_ID);

})

function getEmployeeDetail() {
    $.ajax({
        async: true,
        url: `${EMPLOYEE_API_URL}/id/${EMPLOYEE_ID}?is_detail=1`,
        type: 'GET',
        beforeSend: function (xhr) {
          xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
        },
        error: function(res) {
          const response = JSON.parse(res.responseText)
          let isRetry = retryRequest(response)
          if(isRetry) $.ajax(this)
        },
        success: function(res) {
          const response = res.data
          $('#employee-detail-overlay').hide()
          renderForm(response)
        }
    });
}

function renderForm(data){
    let $form = $('#employee-form')
    $form.find("input[name='fullname']").val(data.fullname)
    $form.find("input[name='dob']").val(formatDateReverse(data.dob))
    $form.find("input[name='city']").val(data.city)
    $form.find("input[name='gender']").val(data.gender == 'L' ? 'Laki-laki' : 'Perempuan')
    $form.find("input[name='fullname']").val(data.fullname)
    $form.find("textarea[name='address']").val(data.address)
    $form.find("input[name='phone']").val(data.phone)
    $form.find("input[name='email']").val(data.email)
    $form.find("input[name='nationality']").val(data.nationality)
    $form.find("input[name='education']").val(data.education)
    if(data.img_url){
        $('#employee-image').prop("src", WEB_URL+"/"+data.img_url)
    }

    renderEducation(data.education_history)
    renderWorking(data.working_history)
    renderTraining(data.training_history)
    renderProject(data.project_history)
    renderCertificate(data.certificate)
}
