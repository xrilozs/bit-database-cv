$(document).ready(function(){
  
  let userAccess = JSON.parse(USER_ACCESS)
  if(!parseInt(userAccess.employee_module)){
    $('#employee-module-box').hide()
  }else{
    getEmployeeTotal()
  }
});

function getEmployeeTotal(){
  $.ajax({
    async: true,
    url: `${EMPLOYEE_API_URL}/total`,
    type: 'GET',
    beforeSend: function (xhr) {
      xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
    },
    error: function(res) {
      const response = JSON.parse(res.responseText)
      // showError(response.message)
      let isRetry = retryRequest(response)
      if(isRetry) $.ajax(this)
    },
    success: function(res) {
      const response = res.data
      $('#dashboard-employee-total').html(response)
    }
  });
}