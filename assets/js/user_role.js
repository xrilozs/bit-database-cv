let USER_ROLE_ID

$(document).ready(function(){

  //data table
  let user_role_table = $('#user-role-datatable').DataTable( {
      processing: true,
      serverSide: true,
      searching: true,
      ajax: {
        async: true,
        url: `${USER_ROLE_API_URL}`,
        type: "GET",
        dataType: "json",
        crossDomain: true,
        beforeSend: function (xhr) {
          xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
        },
        data: function ( d ) {
          let newObj = {}
          let start = d.start
          let size = d.length
          newObj.page_number = d.start > 0 ? (start/size) : 0;
          newObj.page_size = size
          newObj.search = d.search.value
          newObj.draw = d.draw
          d = newObj
          console.log("D itu:", d)
          return d
        },
        error: function(res) {
          const response = JSON.parse(res.responseText)
          let isRetry = retryRequest(response)
          if(isRetry) $.ajax(this)
        }
      },
      columns: [
          { 
            data: "role",
            render: function (data, type, row, meta) {
              return `<span class="badge badge-pill badge-dark">${data}</span>`
            },
            orderable: false
          },
          {
            data: "created_at",
            orderable: false
          },
          {
            data: "id",
            className: "dt-body-right",
            render: function (data, type, row, meta) {
              let button = `<button class="btn btn-sm btn-success user-role-update-toggle" data-id="${data}" data-toggle="modal" data-target="#user-role-update-modal" title="update">
                <i class="fas fa-edit"></i>
              </button>
              <button class="btn btn-sm btn-danger user-role-delete-toggle" data-id="${data}" data-toggle="modal" data-target="#user-role-delete-modal" title="hapus">
                <i class="fas fa-trash"></i>
              </button>`
              
              return button
            },
            orderable: false
          }
      ]
  });
  
  //toggle
  $('#user-role-create-toggle').click(function(e) {
    clearForm('create')  
  })
  
  $("body").delegate(".user-role-update-toggle", "click", function(e) {
    USER_ROLE_ID = $(this).data('id')
    $('#user-role-update-overlay').show()

    $.ajax({
        async: true,
        url: `${USER_ROLE_API_URL}/id/${USER_ROLE_ID}`,
        type: 'GET',
        beforeSend: function (xhr) {
          xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
        },
        error: function(res) {
          const response  = JSON.parse(res.responseText)
          let isRetry     = retryRequest(response)
          if(isRetry) $.ajax(this)
          else $('#user-role-update-modal').modal('toggle')
        },
        success: function(res) {
          const response = res.data
          renderForm(response, 'update')
          $('#user-role-update-overlay').hide()  
        }
    });
  })
  
  $("body").delegate(".user-role-delete-toggle", "click", function(e) {
    USER_ROLE_ID = $(this).data('id')
  })

  //form
  $('#user-role-create-form').submit(function (e){
    e.preventDefault();
    startLoadingButton('#user-role-create-button')
    
    let $form = $(this),
        request = {
          role: $form.find( "input[name='role']" ).val()
        }
        
    $.ajax({
        async: true,
        url: USER_ROLE_API_URL,
        type: 'POST',
        beforeSend: function (xhr) {
          xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
        },
        data: JSON.stringify(request),
        error: function(res) {
          const response  = JSON.parse(res.responseText)
          let isRetry     = retryRequest(response)
          if(isRetry) $.ajax(this)
          else endLoadingButton('#user-role-create-button', 'Simpan')
        },
        success: function(res) {
          showSuccess("Membuat user role baru berhasil!")
          endLoadingButton('#user-role-create-button', 'Simpan')
          $('#user-role-create-modal').modal('hide')
          user_role_table.ajax.reload()
        }
    });
  })
  
  $('#user-role-update-form').submit(function (e){
    e.preventDefault()
    startLoadingButton('#user-role-update-button')

    let $form = $(this),
        request = {
          id: USER_ROLE_ID,
          role: $form.find( "input[name='role']" ).val()
        }

    $.ajax({
        async: true,
        url: USER_ROLE_API_URL,
        type: 'PUT',
        beforeSend: function (xhr) {
          xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
        },
        data: JSON.stringify(request),
        error: function(res) {
          const response = JSON.parse(res.responseText)
          let isRetry    = retryRequest(response)
          if(isRetry) $.ajax(this)
          else endLoadingButton('#user-role-update-button', 'Simpan')
        },
        success: function(res) {
          showSuccess("Mengubah user role berhasil!")
          endLoadingButton('#user-role-update-button', 'Simpan')
          $('#user-role-update-modal').modal('toggle')
          user_role_table.ajax.reload()
        }
    });
  })
  
  $('#user-role-delete-button').click(function (){
    startLoadingButton('#user-role-delete-button')
    
    $.ajax({
        async: true,
        url: `${USER_ROLE_API_URL}/delete/${USER_ROLE_ID}`,
        type: 'DELETE',
        beforeSend: function (xhr) {
          xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
        },
        error: function(res) {
          const response = JSON.parse(res.responseText)
          let isRetry    = retryRequest(response)
          if(isRetry) $.ajax(this)
          else endLoadingButton('#user-role-delete-button', 'Simpan')
        },
        success: function(res) {
          showSuccess(`Menghapus user role berhasil!`)
          endLoadingButton('#user-role-delete-button', 'Simpan')
          $('#user-role-delete-modal').modal('toggle')
          user_role_table.ajax.reload()
        }
    });
  })
})

function renderForm(data, type){
  let $form = $(`#user-role-${type}-form`)
  $form.find( "input[name='role']" ).val(data.role)
}

function clearForm(type){
  console.log("CLEAR FORM ", type)
  let $form = $(`#user-role-${type}-form`)
  $form.find( "input[name='role']" ).val("")
}
