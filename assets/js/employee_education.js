let EDUCATION_ID

$(document).ready(function(){
    $('#education-create-toggle').click(function(){
        clearFormEducation('create')
    })

    $("body").delegate(".education-update-toggle", "click", function(e) {
        EDUCATION_ID = $(this).data('id')
        clearFormEducation('update')
    
        $('#education-update-overlay').show()
        $.ajax({
            async: true,
            url: `${EDUCATION_HISTORY_API_URL}/id/${EDUCATION_ID}`,
            type: 'GET',
            beforeSend: function (xhr) {
              xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
            },
            error: function(res) {
              const response = JSON.parse(res.responseText)
              let isRetry = retryRequest(response)
              if(isRetry) $.ajax(this)
              else $('#education-update-modal').modal('toggle')
            },
            success: function(res) {
              const response = res.data
              $('#education-update-overlay').hide()
              renderFormEducation(response)
            }
        });
    })
    
    $("body").delegate(".education-delete-toggle", "click", function(e) {
        EDUCATION_ID = $(this).data('id')
    })

    $('#education-create-form').submit(function (e){
        e.preventDefault()
        startLoadingButton('#education-create-button')
    
        let $form = $(this)
        let data = {
          employee_id: EMPLOYEE_ID,
          school: $form.find( "input[name='school']" ).val(),
          level: $("#education-level-create-field").find(":selected").val(),
          major: $form.find( "input[name='major']" ).val(),
          start_year: $form.find( "input[name='start_year']" ).val(),
          end_year: $form.find( "input[name='end_year']" ).val(),
        }
    
        $.ajax({
            async: true,
            url: EDUCATION_HISTORY_API_URL,
            type: 'POST',
            beforeSend: function (xhr) {
              xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
            },
            data: JSON.stringify(data),
            error: function(res) {
              const response = JSON.parse(res.responseText)
              let isRetry = retryRequest(response)
              if(isRetry) $.ajax(this)
              else endLoadingButton('#education-create-button', 'Simpan')
            },
            success: function(res) {
              endLoadingButton('#education-create-button', 'Simpan')
              showSuccess(res.message)
              $('#education-create-modal').modal('toggle')
              getEmployeeDetail()
            }
        });
    })

    $('#education-update-form').submit(function (e){
        e.preventDefault()
        startLoadingButton('#education-update-button')
    
        let $form = $(this)
        let data = {
          id: EDUCATION_ID,
          employee_id: EMPLOYEE_ID,
          school: $form.find( "input[name='school']" ).val(),
          level: $("#education-level-update-field").find(":selected").val(),
          major: $form.find( "input[name='major']" ).val(),
          start_year: $form.find( "input[name='start_year']" ).val(),
          end_year: $form.find( "input[name='end_year']" ).val(),
        }
    
        $.ajax({
            async: true,
            url: EDUCATION_HISTORY_API_URL,
            type: 'PUT',
            beforeSend: function (xhr) {
              xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
            },
            data: JSON.stringify(data),
            error: function(res) {
              const response = JSON.parse(res.responseText)
              let isRetry = retryRequest(response)
              if(isRetry) $.ajax(this)
              else endLoadingButton('#education-update-button', 'Simpan')
            },
            success: function(res) {
              endLoadingButton('#education-update-button', 'Simpan')
              showSuccess(res.message)
              $('#education-update-modal').modal('toggle')
              getEmployeeDetail()
            }
        });
    })

    $('#employee-delete-button').click(function (){
        startLoadingButton('#education-delete-button')
        
        $.ajax({
            async: true,
            url: `${EDUCATION_HISTORY_API_URL}/delete/${EDUCATION_ID}`,
            type: 'DELETE',
            beforeSend: function (xhr) {
              xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
            },
            error: function(res) {
              const response = JSON.parse(res.responseText)
              let isRetry = retryRequest(response)
              if(isRetry) $.ajax(this)
              else endLoadingButton('#education-delete-button', 'Iya')
            },
            success: function(res) {
              endLoadingButton('#education-delete-button', 'Iya')
              showSuccess(res.message)
              $('#education-delete-modal').modal('toggle')
              getEmployeeDetail()
            }
        });
    })
})

function renderEducation(data){
    if(data.length > 0){
        let content_html = ``
        data.forEach(item => {
            let item_html = `<tr>
                <td>${item.school}</td>
                <td>${item.level}</td>
                <td>${item.major}</td>
                <td>${item.start_year}</td>
                <td>${item.end_year}</td>
                <td>
                    <a href="#" class="btn btn-success text-white education-update-toggle" data-id="${item.id}" data-toggle="modal" data-target="#education-update-modal" title="ubah">
                        <i class="fa fa-edit"></i>
                    </a>
                    <a href="#" class="btn btn-danger text-white education-delete-toggle" data-id="${item.id}" data-toggle="modal" data-target="#education-delete-modal" title="hapus">
                        <i class="fa fa-trash"></i>
                    </a>
                </td>
            </tr>`
            content_html += item_html
        });

        let table_html = `<table class="table table-bordered table-hover" id="education-datatable">
            <thead>
            <tr>
                <th>Nama Sekolah</th>
                <th>Tingkat</th>
                <th>Jurusan</th>
                <th>Tahun Masuk</th>
                <th>Tahun Lulus</th>
                <th>Aksi</th>
            </tr>
            </thead>
            <tbody>
                ${content_html}
            </tbody>
        </table>`

        $('#education-history-table').html(table_html)
    }
}

function renderFormEducation(data){
    let $form = $(`#education-update-form`)
    $form.find( "input[name='school']" ).val(data.school);
    $(`#education-level-update-field`).val(data.level).change()
    $form.find( "input[name='major']" ).val(data.major);
    $form.find( "input[name='start_year']" ).val(data.start_year);
    $form.find( "input[name='end_year']" ).val(data.end_year);
  }
  
  function clearFormEducation(type){
    $(`#education-${type}-form`)[0].reset();
  }