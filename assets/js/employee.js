let EMPLOYEE_ID,
    IMG_URL

$(document).ready(function(){
  $('#employee-create-image').dropify()

  //render datatable
  let employee_table = $('#employee-datatable').DataTable( {
      processing: true,
      serverSide: true,
      searching: true,
      ajax: {
        async: true,
        url: EMPLOYEE_API_URL,
        type: "GET",
        dataType: "json",
        crossDomain: true,
        beforeSend: function (xhr) {
          xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
        },
        data: function ( d ) {
          let newObj = {}
          let start = d.start
          let size = d.length
          newObj.page_number = d.start > 0 ? (start/size) : 0;
          newObj.page_size = size
          newObj.search = d.search.value
          newObj.draw = d.draw
          d = newObj
          console.log("D itu:", d)
          return d
        },
        error: function(res) {
          const response = JSON.parse(res.responseText)
          let isRetry = retryRequest(response)
          if(isRetry) $.ajax(this)
        }
      },
      columns: [
          {
            data: "img_url",
            render: function (data, type, row, meta) {
              return `<img src="${WEB_URL+"/"+data}" class="img-responsive" style="width:200px;">`
            }
          },
          { 
            data: "fullname",
            orderable: false
          },
          { 
            data: "dob",
            render: function (data, type, row, meta) {
              return formatDateReverse(data)
            },
            orderable: false
          },
          { 
            data: "city",
            orderable: false
          },
          { 
            data: "gender",
            render: function (data, type, row, meta) {
              return data == 'L' ? 'Laki-laki' : 'Perempuan'
            },
            orderable: false
          },
          { 
            data: "address",
            render: function (data, type, row, meta) {
              return sortText(data, 20)
            },
            orderable: false
          },
          { 
            data: "phone",
            orderable: false
          },
          { 
            data: "email",
            orderable: false
          },
          { 
            data: "nationality",
            orderable: false
          },
          { 
            data: "education",
            orderable: false
          },
          {
            data: "created_at",
            orderable: false
          },
          {
            data: "id",
            className: "dt-body-right",
            render: function (data, type, row, meta) {
              let button = `<div class="dropdown dropleft">
                <button class="btn btn-default dropdown-toggle" type="button" data-toggle="dropdown" aria-expanded="false">
                  Action
                </button>
                <div class="dropdown-menu py-0">
                  <a class="dropdown-item bg-dark text-white" href="${WEB_URL}/employee/${data}#certificate-section" title="Sertifikat">
                    <i class="fa fa-file-alt"></i> Sertifikat
                  </a>
                  <a class="dropdown-item bg-dark text-white" href="${WEB_URL}/employee/${data}#education-section" title="Edukasi">
                    <i class="fa fa-graduation-cap"></i> Edukasi
                  </a>
                  <a class="dropdown-item bg-dark text-white" href="${WEB_URL}/employee/${data}#project-section" title="Projek">
                    <i class="fa fa-project-diagram"></i> Projek
                  </a>
                  <a class="dropdown-item bg-dark text-white" href="${WEB_URL}/employee/${data}#training-section" title="Pelatihan">
                    <i class="fa fa-book-reader"></i> Pelatihan
                  </a>
                  <a class="dropdown-item bg-dark text-white" href="${WEB_URL}/employee/${data}#working-section" title="Pekerjaan">
                    <i class="fa fa-briefcase"></i> Pekerjaan
                  </a>
                  <a class="dropdown-item bg-primary text-white" href="${WEB_URL}/employee/${data}" title="Detail">
                    <i class="fa fa-search"></i> Detail
                  </a>
                  <a href="#" class="dropdown-item bg-success text-white employee-update-toggle" data-id="${data}" data-toggle="modal" data-target="#employee-update-modal" title="ubah">
                    <i class="fa fa-edit"></i> Ubah
                  </a>
                  <a href="#" class="dropdown-item bg-danger text-white employee-delete-toggle" data-id="${data}" data-toggle="modal" data-target="#employee-delete-modal" title="hapus">
                    <i class="fa fa-trash"></i> Hapus
                  </a>
                </div>
              </div>`
              return button
            },
            orderable: false
          }
      ]
  });

  $('#employee-create-image').change(function(e) {
    let file = e.target.files[0];
    upload_image(file, "create")
  });

  $('#employee-update-image').change(function(e) {
    let file = e.target.files[0];
    upload_image(file, "update")
  });
  
  //button action click
  $("#employee-create-toggle").click(function(e) {
    clearForm('create')
  })
  
  $("body").delegate(".employee-update-toggle", "click", function(e) {
    EMPLOYEE_ID = $(this).data('id')
    clearForm('update')

    $('#employee-update-overlay').show()
    $.ajax({
        async: true,
        url: `${EMPLOYEE_API_URL}/id/${EMPLOYEE_ID}`,
        type: 'GET',
        beforeSend: function (xhr) {
          xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
        },
        error: function(res) {
          const response = JSON.parse(res.responseText)
          let isRetry = retryRequest(response)
          if(isRetry) $.ajax(this)
          else $('#employee-update-modal').modal('toggle')
        },
        success: function(res) {
          const response = res.data
          $('#employee-update-overlay').hide()
          renderForm(response, 'update')
        }
    });
  })
  
  $("body").delegate(".employee-delete-toggle", "click", function(e) {
    EMPLOYEE_ID = $(this).data('id')
  })

  //submit form
  $('#employee-create-form').submit(function (e){
    e.preventDefault();
    startLoadingButton('#employee-create-button')
    
    let $form = $(this)
    let data = {
      img_url: IMG_URL,
      fullname: $form.find( "input[name='fullname']" ).val(),
      dob: $form.find( "input[name='dob']" ).val(),
      city: $form.find( "input[name='city']" ).val(),
      gender:  $form.find('input[name="gender"]:checked').val(),
      address: $form.find( "textarea[name='address']" ).val(),
      phone: $form.find( "input[name='phone']" ).val(),
      email: $form.find( "input[name='email']" ).val(),
      nationality: $form.find( "input[name='nationality']" ).val(),
      education: $form.find( "input[name='education']" ).val()
    }
    $.ajax({
        async: true,
        url: EMPLOYEE_API_URL,
        type: 'POST',
        beforeSend: function (xhr) {
          xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
        },
        data: JSON.stringify(data),
        error: function(res) {
          const response = JSON.parse(res.responseText)
          let isRetry = retryRequest(response)
          if(isRetry) $.ajax(this)
          else endLoadingButton('#employee-create-button', 'Simpan')
        },
        success: function(res) {
          endLoadingButton('#employee-create-button', 'Simpan')
          showSuccess(res.message)
          $('#employee-create-modal').modal('hide')
          employee_table.ajax.reload()
        }
    });
  })
  
  $('#employee-update-form').submit(function (e){
    e.preventDefault()
    startLoadingButton('#employee-update-button')

    let $form = $(this)
    let data = {
      id: EMPLOYEE_ID,
      fullname: $form.find( "input[name='fullname']" ).val(),
      dob: $form.find( "input[name='dob']" ).val(),
      city: $form.find( "input[name='city']" ).val(),
      gender:  $form.find('input[name="gender"]:checked').val(),
      address: $form.find( "textarea[name='address']" ).val(),
      phone: $form.find( "input[name='phone']" ).val(),
      email: $form.find( "input[name='email']" ).val(),
      nationality: $form.find( "input[name='nationality']" ).val(),
      education: $form.find( "input[name='education']" ).val(),
      img_url: IMG_URL
    }

    $.ajax({
        async: true,
        url: EMPLOYEE_API_URL,
        type: 'PUT',
        beforeSend: function (xhr) {
          xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
        },
        data: JSON.stringify(data),
        error: function(res) {
          const response = JSON.parse(res.responseText)
          let isRetry = retryRequest(response)
          if(isRetry) $.ajax(this)
          else endLoadingButton('#employee-update-button', 'Simpan')
        },
        success: function(res) {
          endLoadingButton('#employee-update-button', 'Simpan')
          showSuccess(res.message)
          $('#employee-update-modal').modal('toggle')
          employee_table.ajax.reload()
        }
    });
  })
  
  $('#employee-delete-button').click(function (){
    startLoadingButton('#employee-delete-button')
    
    $.ajax({
        async: true,
        url: `${EMPLOYEE_API_URL}/delete/${EMPLOYEE_ID}`,
        type: 'DELETE',
        beforeSend: function (xhr) {
          xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
        },
        error: function(res) {
          const response = JSON.parse(res.responseText)
          let isRetry = retryRequest(response)
          if(isRetry) $.ajax(this)
          else endLoadingButton('#employee-delete-button', 'Iya')
        },
        success: function(res) {
          endLoadingButton('#employee-delete-button', 'Iya')
          showSuccess(res.message)
          $('#employee-delete-modal').modal('toggle')
          employee_table.ajax.reload()
        }
    });
  })
})

function upload_image(file, type){
  $(`#employee-${type}-button`).attr("disabled", true)

  let formData = new FormData();
  formData.append('image', file);
  
  $.ajax({
      async: true,
      url: `${EMPLOYEE_API_URL}/upload`,
      type: 'POST',
      beforeSend: function (xhr) {
        xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
      },
      data: formData,
      processData: false, 
      contentType: false, 
      error: function(res) {
        const response = JSON.parse(res.responseText)
        let isRetry = retryRequest(response)
        if(isRetry) $.ajax(this)
        else{
          showError("Upload gambar", "Gagal melakukan upload gambar!")
          $(`#employee-${type}-button`).attr("disabled", false)
        }
      },
      success: function(res) {
        const response = res.data
        IMG_URL = response.file_url
        $(`#employee-${type}-button`).attr("disabled", false)
        showSuccess(res.message)
      }
  });
}

function renderForm(data, type){
  let $form = $(`#employee-${type}-form`)
  $form.find( "input[name='fullname']" ).val(data.fullname);
  $('.datepicker').datepicker("setDate", new Date(data.dob));
  $form.find( "input[name='city']" ).val(data.city);
  $form.find( `input[name="gender"][value="${data.gender}"]` ).prop('checked', true);
  $form.find( "textarea[name='address']" ).val(data.address);
  $form.find( "input[name='phone']" ).val(data.phone);
  $form.find( "input[name='email']" ).val(data.email);
  $form.find( "input[name='nationality']" ).val(data.nationality);
  $form.find( "input[name='education']" ).val(data.education);
  if(type == 'update'){
    let drEvent = $('#employee-update-image').dropify({
      defaultFile: `${WEB_URL}/${data.img_url}`
    });
    drEvent = drEvent.data('dropify');
    drEvent.resetPreview();
    drEvent.clearElement();
    drEvent.settings.defaultFile = `${WEB_URL}/${data.img_url}`;
    drEvent.destroy();
    drEvent.init();
    IMG_URL = data.img_url
  }
}

function clearForm(type){
  $(`#employee-${type}-form`)[0].reset();
  if(type == 'create'){
    let drEvent = $('#employee-create-image').dropify();
    drEvent = drEvent.data('dropify');
    drEvent.resetPreview();
    drEvent.clearElement();
    $('.datepicker').datepicker("setDate", new Date());
  }
}
