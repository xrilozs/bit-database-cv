let CONFIG_ID;
let CV_LOGO_URL;
let APPS_LOGO_URL;
let APPS_ICON_URL;

$(document).ready(function(){
  $('#config-cv-logo').change(function(e) {
    let file = e.target.files[0];
    upload_image(file, true)
  });

  $('#config-apps-logo').change(function(e) {
    let file = e.target.files[0];
    upload_image(file, false, true)
  });

  $('#config-apps-icon').change(function(e) {
    let file = e.target.files[0];
    upload_image(file, false, false, true)
  });
  
  $('#config-form').submit(function (e){
    e.preventDefault()
    startLoadingButton("#config-button")

    let $form   = $(this),
        request = {
          id: CONFIG_ID,
          manager_email: $form.find( "input[name='manager_email']" ).val(),
          cv_logo: CV_LOGO_URL,
          apps_logo: APPS_LOGO_URL,
          apps_icon: APPS_ICON_URL,
        }

    $.ajax({
        async: true,
        url: `${CONFIG_API_URL}`,
        type: 'POST',
        beforeSend: function (xhr) {
          xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
        },
        data: JSON.stringify(request),
        error: function(res) {
          const response = JSON.parse(res.responseText)
          let isRetry = retryRequest(response)
          if(isRetry) $.ajax(this)
          else endLoadingButton('#config-button', 'Simpan')
        },
        success: function(res) {
          endLoadingButton('#config-button', 'Simpan')
          showSuccess(res.message)
          getConfig()
        }
    });
  })
})

function renderConfigForm(data){
  let $form = $(`#config-form`)
  $form.find( "input[name='manager_email']" ).val(data.manager_email)

  CONFIG_ID     = data.id
  CV_LOGO_URL   = data.cv_logo
  APPS_LOGO_URL = data.apps_logo
  APPS_ICON_URL = data.apps_icon
  
  if(CV_LOGO_URL){
    $('#config-cv-logo').attr("data-default-file", `${WEB_URL}/${CV_LOGO_URL}`);
    console.log("ada cv logo")
  }
  $('#config-cv-logo').dropify()

  if(APPS_LOGO_URL){
    $('#config-apps-logo').attr("data-default-file", `${WEB_URL}/${APPS_LOGO_URL}`);
    console.log("ada apps logo")
  }
  $('#config-apps-logo').dropify()

  if(APPS_ICON_URL){
    $('#config-apps-icon').attr("data-default-file", `${WEB_URL}/${APPS_ICON_URL}`);
    console.log("ada apps icon")
  }
  $('#config-apps-icon').dropify()
}

//upload image
function upload_image(file, cv_logo=false, apps_logo=false, apps_icon=false){
  $(`#config-button`).attr("disabled", true)

  let formData = new FormData();
  formData.append('image', file);
  
  $.ajax({
      async: true,
      url: `${CONFIG_API_URL}/upload`,
      type: 'POST',
      beforeSend: function (xhr) {
        xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
      },
      data: formData,
      processData: false, 
      contentType: false, 
      error: function(res) {
        const response = JSON.parse(res.responseText)
        let isRetry = retryRequest(response)
        if(isRetry) $.ajax(this)
        else{
          showError("Upload gambar", "Gagal melakukan upload gambar!")
          $(`#config-button`).attr("disabled", false)
        }
      },
      success: function(res) {
        const response = res.data
        if(cv_logo){
          CV_LOGO_URL = response.file_url
        }
        if(apps_logo){
          APPS_LOGO_URL = response.file_url
        }
        if(apps_icon){
          APPS_ICON_URL = response.file_url
        }

        $(`#config-button`).attr("disabled", false)
        showSuccess(res.message)
      }
  });
}

function renderConfigFormBlank(){
  console.log("render blank")
  $('#config-cv-logo').dropify()
  $('#config-apps-logo').dropify()
  $('#config-apps-icon').dropify()
}