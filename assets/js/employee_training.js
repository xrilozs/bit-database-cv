let TRAINING_ID
let IS_TYPE_OTHER = false

$(document).ready(function(){
    getTrainingType()

    $('.type-option').change(function(){
      let val = $(this).val()
      if(val == 'Sertifikasi Lainnya'){
        IS_TYPE_OTHER = true
        $(".type-text").show()
      }else{
        IS_TYPE_OTHER = false
        $(".type-text").hide()
      }
    })

    $('#training-create-toggle').click(function(){
        clearFormTraining('create')
    })

    $("body").delegate(".training-update-toggle", "click", function(e) {
        TRAINING_ID = $(this).data('id')
        clearFormTraining('update')
    
        $('#training-update-overlay').show()
        $.ajax({
            async: true,
            url: `${TRAINING_HISTORY_API_URL}/id/${TRAINING_ID}`,
            type: 'GET',
            beforeSend: function (xhr) {
              xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
            },
            error: function(res) {
              const response = JSON.parse(res.responseText)
              let isRetry = retryRequest(response)
              if(isRetry) $.ajax(this)
              else $('#training-update-modal').modal('toggle')
            },
            success: function(res) {
              const response = res.data
              $('#training-update-overlay').hide()
              renderFormTraining(response)
            }
        });
    })
    
    $("body").delegate(".training-delete-toggle", "click", function(e) {
        TRAINING_ID = $(this).data('id')
    })

    $('#training-create-form').submit(function (e){
        e.preventDefault()
        startLoadingButton('#training-create-button')
    
        let $form = $(this)
        let data = {
          employee_id: EMPLOYEE_ID,
          title: $form.find( "input[name='title']" ).val(),
          is_type_other: IS_TYPE_OTHER,
          institution: $form.find( "input[name='institution']" ).val(),
          training_date: formatDateReverse($form.find( "input[name='training_date']" ).val()),
          expired_date: formatDateReverse($form.find( "input[name='expired_date']" ).val()),
        }

        if(IS_TYPE_OTHER){
          data.type = $form.find( "input[name='type-other']" ).val()
        }else{
          data.type = $("#training-type-create-field").find(":selected").val()
        }
    
        $.ajax({
            async: true,
            url: TRAINING_HISTORY_API_URL,
            type: 'POST',
            beforeSend: function (xhr) {
              xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
            },
            data: JSON.stringify(data),
            error: function(res) {
              const response = JSON.parse(res.responseText)
              let isRetry = retryRequest(response)
              if(isRetry) $.ajax(this)
              else endLoadingButton('#training-create-button', 'Simpan')
            },
            success: function(res) {
              endLoadingButton('#training-create-button', 'Simpan')
              showSuccess(res.message)
              $('#training-create-modal').modal('toggle')
              getEmployeeDetail()
            }
        });
    })

    $('#training-update-form').submit(function (e){
        e.preventDefault()
        startLoadingButton('#training-update-button')
    
        let $form = $(this)
        let data = {
          id: TRAINING_ID,
          employee_id: EMPLOYEE_ID,
          title: $form.find( "input[name='title']" ).val(),
          is_type_other: IS_TYPE_OTHER,
          institution: $form.find( "input[name='institution']" ).val(),
          training_date: formatDateReverse($form.find( "input[name='training_date']" ).val()),
          expired_date: formatDateReverse($form.find( "input[name='expired_date']" ).val()),
        }

        if(IS_TYPE_OTHER){
          data.type = $form.find( "input[name='type-other']" ).val()
        }else{
          data.type = $("#training-type-update-field").find(":selected").val()
        }
    
        $.ajax({
            async: true,
            url: TRAINING_HISTORY_API_URL,
            type: 'PUT',
            beforeSend: function (xhr) {
              xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
            },
            data: JSON.stringify(data),
            error: function(res) {
              const response = JSON.parse(res.responseText)
              let isRetry = retryRequest(response)
              if(isRetry) $.ajax(this)
              else endLoadingButton('#training-update-button', 'Simpan')
            },
            success: function(res) {
              endLoadingButton('#training-update-button', 'Simpan')
              showSuccess(res.message)
              $('#training-update-modal').modal('toggle')
              getEmployeeDetail()
            }
        });
    })

    $('#training-delete-button').click(function (){
        startLoadingButton('#training-delete-button')
        
        $.ajax({
            async: true,
            url: `${TRAINING_HISTORY_API_URL}/delete/${TRAINING_ID}`,
            type: 'DELETE',
            beforeSend: function (xhr) {
              xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
            },
            error: function(res) {
              const response = JSON.parse(res.responseText)
              let isRetry = retryRequest(response)
              if(isRetry) $.ajax(this)
              else endLoadingButton('#training-delete-button', 'Iya')
            },
            success: function(res) {
              endLoadingButton('#training-delete-button', 'Iya')
              showSuccess(res.message)
              $('#training-delete-modal').modal('toggle')
              getEmployeeDetail()
            }
        });
    })
})

function renderTraining(data){
    if(data.length > 0){
        let content_html = ``
        data.forEach(item => {
            let item_html = `<tr>
                <td>${item.title}</td>
                <td>${item.type}</td>
                <td>${item.institution}</td>
                <td>${formatDateReverse(item.training_date)}</td>
                <td>${formatDateReverse(item.expired_date)}</td>
                <td>
                    <a href="#" class="btn btn-success text-white training-update-toggle" data-id="${item.id}" data-toggle="modal" data-target="#training-update-modal" title="ubah">
                        <i class="fa fa-edit"></i>
                    </a>
                    <a href="#" class="btn btn-danger text-white training-delete-toggle" data-id="${item.id}" data-toggle="modal" data-target="#training-delete-modal" title="hapus">
                        <i class="fa fa-trash"></i>
                    </a>
                </td>
            </tr>`
            content_html += item_html
        });

        let table_html = `<table class="table table-bordered table-hover" id="training-datatable">
            <thead>
            <tr>
                <th>Nama Pelatihan</th>
                <th>Jenis</th>
                <th>Lembaga</th>
                <th>Tanggal Pelatihan</th>
                <th>Tanggal Expired</th>
                <th>Aksi</th>
            </tr>
            </thead>
            <tbody>
                ${content_html}
            </tbody>
        </table>`

        $('#training-history-table').html(table_html)
    }
}

function renderFormTraining(data){
    let $form = $(`#training-update-form`)
    $form.find( "input[name='name']" ).val(data.name);
    $form.find( "input[name='title']" ).val(data.title);
    if(parseInt(data.is_type_other)){
      $(`#training-type-update-field`).val("Sertifikasi Lainnya").change()
      $('.type-text').show()
      $('.type-text').val(data.type)
    }else{
      $('.type-text').hide()
      $(`#training-type-update-field`).val(data.type).change()
    }
    $form.find( "input[name='institution']" ).val(data.institution);
    
    $('.training-training-date').datepicker({
      format: 'dd-mm-yyyy',
      orientation: 'bottom',
      autoclose: true
    })
    $('.training-expired-date').datepicker({
      format: 'dd-mm-yyyy',
      orientation: 'bottom',
      autoclose: true
    })
    $('.training-training-date').datepicker("setDate", new Date(data.training_date));
    $('.training-expired-date').datepicker("setDate", new Date(data.expired_date));
}
  
function clearFormTraining(type){
  $(`#training-${type}-form`)[0].reset();
  $('.type-text').hide()
  $(`.type-option`).val("").change()

  if(type == 'create'){
    $('.datepicker').datepicker({
      format: 'dd-mm-yyyy',
      orientation: 'bottom',
      autoclose: true
    })
    $('.datepicker').datepicker("setDate", new Date());
  }
}

function getTrainingType(){
  $.ajax({
    async: true,
    url: TRAINING_TYPE_API_URL,
    type: 'GET',
    beforeSend: function (xhr) {
      xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
    },
    error: function(res) {
      const response = JSON.parse(res.responseText)
      let isRetry    = retryRequest(response)
      if(isRetry) $.ajax(this)
    },
    success: function(res) {
      const response = res.data
      renderTrainingTypeOption(response)
    }
  });
}

function renderTrainingTypeOption(data){
  let type_html = "<option disabled>--Pilih Jenis Pelatihan--</option>"
  data.forEach(item => {
    let option_html = `<option value="${item.name}">${item.name}</option>`
    type_html       += option_html
  });
  type_html += `<option value="Sertifikasi Lainnya">Sertifikasi Lainnya</option>`
  $(`.type-option`).html(type_html)
}
