let TOAST = Swal.mixin({
  toast: true,
  position: 'top-end',
  showConfirmButton: false,
  timer: 3000
});
let SESSION     = localStorage.getItem("user-token");
let ADMIN_ROLE  = localStorage.getItem("user-role");
let USER_ACCESS = localStorage.getItem("user-access");

$(document).ready(function(){
  getConfig()

  if(SESSION){
    getProfile()
  }

  $("#login-form").submit(function(e) {
    e.preventDefault();
    startLoadingButton("#login-button")
  
    let $form = $( this ),
        username = $form.find( "input[name='username']" ).val(),
        password = $form.find( "input[name='password']" ).val()
    
    $.ajax({
        async: true,
        url: `${API_URL}/user/login`,
        type: 'POST',
        data: JSON.stringify({
          username: username,
          password: password
        }),
        error: function(res) {
          response = res.responseJSON
          showError(response.message)
          endLoadingButton('#login-button', 'Masuk')
        },
        success: function(res) {
          response = res.data;
          setSession(response)
          // if(ADMIN_ROLE == 'SUPERADMIN') window.location.href = 'dashboard'
          // else if(ADMIN_ROLE == 'ADMIN') window.location.href = 'outbounds'
          // else if(ADMIN_ROLE == 'SALES') window.location.href = 'sales-books'
          window.location.href = 'dashboard'
        }
    });    
  })
})

function getProfile(){
  $.ajax({
    async: true,
    url: `${API_URL}/user/profile`,
    type: 'GET',
    error: function(res) {
      // response = res.responseJSON
      removeSession()
    },
    success: function(res) {
      response = res.data;
      setSession(response)
      window.location.href = 'dashboard'
    }
}); 
}

function getConfig(){
  $.ajax({
    async: true,
    url: `${API_URL}/config`,
    type: 'GET',
    beforeSend: function (xhr) {
      xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
    },
    error: function(res) {
      const response = JSON.parse(res.responseText)
      let isRetry = retryRequest(response)
      if(isRetry) $.ajax(this)
      else{
        if ($("#config-form").length) {
          renderConfigFormBlank()
        }
      }
    },
    success: function(res) {
      let config = res.data
      if(config.apps_logo){
        $('.brand-image').prop("src", WEB_URL+"/"+config.apps_logo)
      }
      if(config.apps_icon){
        $("#favicon").attr("href", WEB_URL+"/"+config.apps_icon);
      }
      if ($("#config-form").length) {
        renderConfigForm(config)
      }
    }
  });
}

function startLoadingButton(dom){
  let loading_button = `<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>`
  $(dom).html(loading_button)
  $(dom).prop('disabled', true);
}

function endLoadingButton(dom, text){
  $(dom).html(text)
  $(dom).prop('disabled', false);
}

function showError(message){
  TOAST.fire({
    icon: 'error',
    title: message
  })
}

function showSuccess(message){
  TOAST.fire({
    icon: 'success',
    title: message
  })
}

function setSession(data){
  const user = data.user
  console.log("SET NEW SESSION")
  localStorage.setItem("user-token", data.access_token)
  localStorage.setItem("user-refresh-token", data.refresh_token)
  localStorage.setItem("user-fullname", user.fullname);
  localStorage.setItem("user-access", JSON.stringify(data.access));
  SESSION         = data.access_token
  REFRESH_SESSION = data.refresh_token
  ADMIN_FULLNAME  = data.fullname
  USER_ACCESS     = JSON.stringify(data.access)
}

function removeSession(){
  console.log("REMOVE SESSION")
  localStorage.removeItem("user-token");
  localStorage.removeItem("user-refresh-token");
  localStorage.removeItem("user-fullname");
  localStorage.removeItem("user-access");
  window.location.href = `${WEB_URL}/login`
}
