let USER_API_URL              = `${API_URL}/user`
let USER_ROLE_API_URL         = `${API_URL}/user-role`
let USER_ACCESS_API_URL       = `${API_URL}/user-access`
let CONFIG_API_URL            = `${API_URL}/config`
let EMPLOYEE_API_URL          = `${API_URL}/employee`
let CERTIFICATE_API_URL       = `${API_URL}/certificate`
let EDUCATION_HISTORY_API_URL = `${API_URL}/education-history`
let WORKING_HISTORY_API_URL   = `${API_URL}/working-history`
let PROJECT_HISTORY_API_URL   = `${API_URL}/project-history`
let TRAINING_HISTORY_API_URL  = `${API_URL}/training-history`
let TRAINING_TYPE_API_URL  = `${API_URL}/training-type`
let SESSION                   = localStorage.getItem("user-token")
let REFRESH_SESSION           = localStorage.getItem("user-refresh-token")
let USER_FULLNAME             = localStorage.getItem("user-fullname")
let USER_ACCESS               = localStorage.getItem("user-access");
let RETRY_COUNT               = 0

let TOAST = Swal.mixin({
  toast: true,
  position: 'top-end',
  showConfirmButton: false,
  timer: 3000
});

$(".datepicker").datepicker( {
  format: "yyyy-mm-dd",
  autoclose: true
});

$('.select2').select2()

$('.select2bs4').select2({
  theme: 'bootstrap4'
})

$('.dropify').dropify();

$(document).ready(function(){
  if(!SESSION){
    window.location.href = `${WEB_URL}/login`
  }
  console.log("ACCESS: ", USER_ACCESS)

  $.extend(true, $.fn.dataTable.defaults, {
    language: {
      lengthMenu: "Tampil _MENU_ Data",
      zeroRecords: 'Tidak ada data',
      emptyTable: 'Data tidak ditemukan',
      search: "Cari",
      info: 'Total data _TOTAL_',
      processing: 'Memproses..',
      paginate: {
        first: 'Pertama',
        previous: 'Sebelumnya',
        next: 'Selanjutnya',
        last: 'Terakhir'
      },
    }
  });
  
  renderHeader()
  renderSidebar()
  getConfig()

  $("body").delegate(".print-cv-button", "click", function(e) {
    // $('#print-cv-button').click(function(){
    const employeeId = $(this).data("id")
    startLoadingButton('.print-cv-button')
    $.ajax({
      async: true,
      url: `${EMPLOYEE_API_URL}/print/${employeeId}`,
      type: 'GET',
      beforeSend: function (xhr) {
        xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
      },
      error: function(res) {
        const response = JSON.parse(res.responseText)
        let isRetry = retryRequest(response)
        if(isRetry) $.ajax(this)
        else endLoadingButton('.print-cv-button', '<i class="nav-icon fas fa-file-word"></i>&nbsp;&nbsp;Cetak CV')
      },
      success: function(res) {
        endLoadingButton('.print-cv-button', '<i class="nav-icon fas fa-file-word"></i>&nbsp;&nbsp;Cetak CV')
        showSuccess(res.message)
        window.open(WEB_URL + res.data, '_blank');
      }
    });
  })
});

function renderHeader(){
  $('#header-user-name').html(`${USER_FULLNAME}`)
}

function renderSidebar(){
  let userAccess = JSON.parse(USER_ACCESS)
  if(!parseInt(userAccess.user_module)){
    $('#user-module-menu').hide()
  }
  if(!parseInt(userAccess.employee_module)){
    $('#employee-module-menu').hide()
  }
  if(!parseInt(userAccess.self_module)){
    $('#self-module-menu').hide()
  }
  if(!parseInt(userAccess.search_module)){
    $('#search-module-menu').hide()
  }
  if(!parseInt(userAccess.config_module)){
    $('#config-module-menu').hide()
  }
  if(!parseInt(userAccess.training_module)){
    $('#training-module-menu').hide()
  }
}

function startLoadingButton(dom){
  let loading_button = `<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span>`
  $(dom).html(loading_button)
  $(dom).prop('disabled', true);
}

function endLoadingButton(dom, text){
  $(dom).html(text)
  $(dom).prop('disabled', false);
}

function showError(message){
  TOAST.fire({
    icon: 'error',
    title: message
  })
}

function showSuccess(message){
  TOAST.fire({
    icon: 'success',
    title: message
  })
}

function setSession(data){
  const user = data.user
  console.log("SET NEW SESSION")
  localStorage.setItem("user-token", data.access_token)
  localStorage.setItem("user-refresh-token", data.refresh_token)
  localStorage.setItem("user-fullname", user.fullname);
  localStorage.setItem("user-access", JSON.stringify(data.access));
  SESSION         = data.access_token
  REFRESH_SESSION = data.refresh_token
  USER_ACCESS     = JSON.stringify(data.access)
}

function removeSession(){
  console.log("REMOVE SESSION")
  localStorage.removeItem("user-token");
  localStorage.removeItem("user-refresh-token");
  localStorage.removeItem("user-fullname");
  localStorage.removeItem("user-access");
  window.location.href = `${WEB_URL}/login`
}

function refreshToken(){
  let resp = {}
  $.ajax({
      async: false,
      url: `${API_URL}/user/refresh`,
      type: 'GET',
      beforeSend: function (xhr) {
        xhr.setRequestHeader('Authorization', `Bearer ${REFRESH_SESSION}`);
      },
      error: function(res) {
        resp = {status: "failed"}
      },
      success: function(res) {
        const response = res.data
        resp = {status: "success", data: response}
      }
  });
  
  return resp
}

function retryRequest(responseError){
  console.log("RETRY: ", RETRY_COUNT)
  if(responseError.code == 401){
    if(RETRY_COUNT < 3){
      let resObj = refreshToken()          
      if(resObj.status == 'success'){
        RETRY_COUNT += 1 
        setSession(resObj.data)
        return true
      }else if(resObj.status == 'failed'){
        removeSession()
      }
    }else{
      removeSession()
    }
  }else{
    showError(responseError.message)
    return false
  }
}

function formatRupiah(angka, prefix){
  var angkaStr  = angka.replace(/[^,\d]/g, '').toString(),
      split     = angkaStr.split(','),
      sisa      = split[0].length % 3,
      rupiah    = split[0].substr(0, sisa),
      ribuan    = split[0].substr(sisa).match(/\d{3}/gi);

  // tambahkan titik jika yang di input sudah menjadi angka ribuan
  if(ribuan){
    separator = sisa ? '.' : '';
    rupiah    += separator + ribuan.join('.');
  }

  rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
  return prefix == undefined ? rupiah : (rupiah ? 'Rp. ' + rupiah : '');
}

function formatDateReverse(datetimeStr){
  let datetimeArr = datetimeStr.split(" ")
  let dateStr     = datetimeArr[0]
  let arr         = dateStr.split("-")
  return `${arr[2]}-${arr[1]}-${arr[0]}`
}

function formatToInputDate(dbdate) {
  // Split the input date into year, month, and day
  var dateParts = dbdate.split('-');
  
  // Create a new Date object using the parsed values
  var originalDate = new Date(dateParts[0], dateParts[1] - 1, dateParts[2]);
  
  // Extract the day, month, and year from the Date object
  var day = originalDate.getDate().toString().padStart(2, '0');
  var month = (originalDate.getMonth() + 1).toString().padStart(2, '0');
  var year = originalDate.getFullYear();
  
  // Assemble the new date string in "dd/mm/yyyy" format
  var convertedDate = month + '/' + day + '/' + year;
  
  return convertedDate;
}

$('.datepicker').datepicker({
  format: 'dd-mm-yyyy',
  orientation: 'bottom',
  autoclose: true
})

if($('.datepicker').val()==""){
  $('.datepicker').datepicker("setDate", new Date());
}

function sortText(text, length=20, isRemoveHTMLTag=false){
  if(isRemoveHTMLTag){
    text = text.replace(/<[^>]+>/g, '');
    console.log("text: ", text)
  }
  return text.length > length ? text.slice(0, length) + ".." : text
}

$("#logout-button").click(function(){    
  removeSession()
})

$("#change-password-toggle").click(function(e){
  let $form = $("#change-password-form" )
  $form.find( "input[name='newPassword']" ).val('')
  $form.find( "input[name='confirmPassword']" ).val('')
})

$("#change-password-form").submit(function(e){
  e.preventDefault()
  startLoadingButton("#change-password-button")

  let $form = $( this ),
      password        = $form.find( "input[name='newPassword']" ).val(),
      confirmPassword = $form.find( "input[name='confirmPassword']" ).val()

  if(password != confirmPassword){
    endLoadingButton('#change-password-button', 'Simpan')
    showError('Password harus sama!')
    return
  }
  
  $.ajax({
      async: true,
      url: `${API_URL}/user/password`,
      type: 'PUT',
      beforeSend: function (xhr) {
        xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
      },
      data: JSON.stringify({
        password
      }),
      error: function(res) {
        const response = JSON.parse(res.responseText)
        showError(response.message)
        endLoadingButton('#change-password-button', 'Simpan')
        let isRetry = retryRequest(response)
        if(isRetry) $.ajax(this)
      },
      success: function(res) {
        showSuccess(res.message)
        endLoadingButton('#change-password-button', 'Simpan')
        $('#change-password-modal').modal('hide')
      }
  });
})

function getConfig(){
  $.ajax({
    async: true,
    url: CONFIG_API_URL,
    type: 'GET',
    beforeSend: function (xhr) {
      xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
    },
    error: function(res) {
      const response = JSON.parse(res.responseText)
      let isRetry = retryRequest(response)
      if(isRetry) $.ajax(this)
      else{
        if ($("#config-form").length) {
          renderConfigFormBlank()
        }
      }
    },
    success: function(res) {
      let config = res.data
      if(config.apps_logo){
        $('.brand-image').prop("src", WEB_URL+"/"+config.apps_logo)
      }
      if(config.apps_icon){
        $("#favicon").attr("href", WEB_URL+"/"+config.apps_icon);
      }
      if ($("#config-form").length) {
        renderConfigForm(config)
      }
    }
  });
}