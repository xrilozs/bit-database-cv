let TRAINING_TYPE_ID

$(document).ready(function(){

  //data table
  let user_name_table = $('#training-type-datatable').DataTable( {
      processing: true,
      serverSide: true,
      searching: true,
      ajax: {
        async: true,
        url: `${TRAINING_TYPE_API_URL}`,
        type: "GET",
        dataType: "json",
        crossDomain: true,
        beforeSend: function (xhr) {
          xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
        },
        data: function ( d ) {
          let newObj = {}
          let start = d.start
          let size = d.length
          newObj.page_number = d.start > 0 ? (start/size) : 0;
          newObj.page_size = size
          newObj.search = d.search.value
          newObj.draw = d.draw
          d = newObj
          console.log("D itu:", d)
          return d
        },
        error: function(res) {
          const response = JSON.parse(res.responseText)
          let isRetry = retryRequest(response)
          if(isRetry) $.ajax(this)
        }
      },
      columns: [
          { 
            data: "name"
          },
          {
            data: "created_at",
            orderable: false
          },
          {
            data: "id",
            className: "dt-body-right",
            render: function (data, type, row, meta) {
              let button = `<button class="btn btn-sm btn-success training-type-update-toggle" data-id="${data}" data-toggle="modal" data-target="#training-type-update-modal" title="update">
                <i class="fas fa-edit"></i>
              </button>
              <button class="btn btn-sm btn-danger training-type-delete-toggle" data-id="${data}" data-toggle="modal" data-target="#training-type-delete-modal" title="hapus">
                <i class="fas fa-trash"></i>
              </button>`
              
              return button
            },
            orderable: false
          }
      ]
  });
  
  //toggle
  $('#training-type-create-toggle').click(function(e) {
    clearForm('create')  
  })
  
  $("body").delegate(".training-type-update-toggle", "click", function(e) {
    TRAINING_TYPE_ID = $(this).data('id')
    $('#training-type-update-overlay').show()

    $.ajax({
        async: true,
        url: `${TRAINING_TYPE_API_URL}/id/${TRAINING_TYPE_ID}`,
        type: 'GET',
        beforeSend: function (xhr) {
          xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
        },
        error: function(res) {
          const response  = JSON.parse(res.responseText)
          let isRetry     = retryRequest(response)
          if(isRetry) $.ajax(this)
          else $('#training-type-update-modal').modal('toggle')
        },
        success: function(res) {
          const response = res.data
          renderForm(response, 'update')
          $('#training-type-update-overlay').hide()  
        }
    });
  })
  
  $("body").delegate(".training-type-delete-toggle", "click", function(e) {
    TRAINING_TYPE_ID = $(this).data('id')
  })

  //form
  $('#training-type-create-form').submit(function (e){
    e.preventDefault();
    startLoadingButton('#training-type-create-button')
    
    let $form = $(this),
        request = {
          name: $form.find( "input[name='name']" ).val()
        }
        
    $.ajax({
        async: true,
        url: TRAINING_TYPE_API_URL,
        type: 'POST',
        beforeSend: function (xhr) {
          xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
        },
        data: JSON.stringify(request),
        error: function(res) {
          const response  = JSON.parse(res.responseText)
          let isRetry     = retryRequest(response)
          if(isRetry) $.ajax(this)
          else endLoadingButton('#training-type-create-button', 'Simpan')
        },
        success: function(res) {
          showSuccess("Membuat jenis pelatihan baru berhasil!")
          endLoadingButton('#training-type-create-button', 'Simpan')
          $('#training-type-create-modal').modal('hide')
          user_name_table.ajax.reload()
        }
    });
  })
  
  $('#training-type-update-form').submit(function (e){
    e.preventDefault()
    startLoadingButton('#training-type-update-button')

    let $form = $(this),
        request = {
          id: TRAINING_TYPE_ID,
          name: $form.find( "input[name='name']" ).val()
        }

    $.ajax({
        async: true,
        url: TRAINING_TYPE_API_URL,
        type: 'PUT',
        beforeSend: function (xhr) {
          xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
        },
        data: JSON.stringify(request),
        error: function(res) {
          const response = JSON.parse(res.responseText)
          let isRetry    = retryRequest(response)
          if(isRetry) $.ajax(this)
          else endLoadingButton('#training-type-update-button', 'Simpan')
        },
        success: function(res) {
          showSuccess("Mengubah jenis pelatihan berhasil!")
          endLoadingButton('#training-type-update-button', 'Simpan')
          $('#training-type-update-modal').modal('toggle')
          user_name_table.ajax.reload()
        }
    });
  })
  
  $('#training-type-delete-button').click(function (){
    startLoadingButton('#training-type-delete-button')
    
    $.ajax({
        async: true,
        url: `${TRAINING_TYPE_API_URL}/delete/${TRAINING_TYPE_ID}`,
        type: 'DELETE',
        beforeSend: function (xhr) {
          xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
        },
        error: function(res) {
          const response = JSON.parse(res.responseText)
          let isRetry    = retryRequest(response)
          if(isRetry) $.ajax(this)
          else endLoadingButton('#training-type-delete-button', 'Simpan')
        },
        success: function(res) {
          showSuccess(`Menghapus jenis pelatihan berhasil!`)
          endLoadingButton('#training-type-delete-button', 'Simpan')
          $('#training-type-delete-modal').modal('toggle')
          user_name_table.ajax.reload()
        }
    });
  })
})

function renderForm(data, type){
  let $form = $(`#training-type-${type}-form`)
  $form.find( "input[name='name']" ).val(data.name)
}

function clearForm(type){
  console.log("CLEAR FORM ", type)
  let $form = $(`#training-type-${type}-form`)
  $form.find( "input[name='name']" ).val("")
}
