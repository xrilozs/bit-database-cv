let USER_ID

$(document).ready(function(){
  getUserRole()

  //data table
  let user_table = $('#user-datatable').DataTable( {
      processing: true,
      serverSide: true,
      searching: true,
      ajax: {
        async: true,
        url: `${USER_API_URL}`,
        type: "GET",
        dataType: "json",
        crossDomain: true,
        beforeSend: function (xhr) {
          xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
        },
        data: function ( d ) {
          let newObj = {}
          let start = d.start
          let size = d.length
          newObj.page_number = d.start > 0 ? (start/size) : 0;
          newObj.page_size = size
          newObj.search = d.search.value
          newObj.draw = d.draw
          d = newObj
          console.log("D itu:", d)
          return d
        },
        error: function(res) {
          const response = JSON.parse(res.responseText)
          let isRetry = retryRequest(response)
          if(isRetry) $.ajax(this)
        }
      },
      columns: [
          { 
            data: "fullname",
            orderable: false
          },
          { 
            data: "username",
            orderable: false
          },{ 
            data: "role",
            render: function (data, type, row, meta) {
              return `<span class="badge badge-pill badge-dark">${data}</span>`
            },
            orderable: false
          },
          {
            data: "is_active",
            className: "dt-body-right",
            render: function (data, type, row, meta) {
              let color = parseInt(data) ? 'primary' : 'danger'
              let text = parseInt(data) ? 'AKTIF' : 'NONAKTIF'
              let badge = `<span class="badge badge-pill badge-${color}">${text}</span>`

              return badge
            }
          },
          {
            data: "created_at",
            orderable: false
          },
          {
            data: "id",
            className: "dt-body-right",
            render: function (data, type, row, meta) {
              let button = `<button class="btn btn-sm btn-success user-update-toggle" data-id="${data}" data-toggle="modal" data-target="#user-update-modal" title="update">
                <i class="fas fa-edit"></i>
              </button>`
              if(parseInt(row.is_active)){
                button += ` <button class="btn btn-sm btn-danger user-inactive-toggle" data-id="${data}" data-toggle="modal" data-target="#user-inactive-modal" title="nonaktifkan">
                  <i class="fas fa-times"></i>
                </button>`
              }else{
                button += ` <button class="btn btn-sm btn-info user-active-toggle" data-id="${data}" data-toggle="modal" data-target="#user-active-modal" title="aktifkan">
                  <i class="fas fa-check"></i>
                </button>`
              }
              
              return button
            },
            orderable: false
          }
      ]
  });
  
  //toggle
  $('#user-create-toggle').click(function(e) {
    clearForm('create')  
  })
  
  $("body").delegate(".user-update-toggle", "click", function(e) {
    USER_ID = $(this).data('id')
    $('#user-update-overlay').show()

    $.ajax({
        async: true,
        url: `${USER_API_URL}/id/${USER_ID}`,
        type: 'GET',
        beforeSend: function (xhr) {
          xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
        },
        error: function(res) {
          const response  = JSON.parse(res.responseText)
          let isRetry     = retryRequest(response)
          if(isRetry) $.ajax(this)
          else $('#user-update-modal').modal('toggle')
        },
        success: function(res) {
          const response = res.data
          renderForm(response, 'update')
          $('#user-update-overlay').hide()  
        }
    });
  })
  
  $("body").delegate(".user-inactive-toggle", "click", function(e) {
    USER_ID = $(this).data('id')
  })
  
  $("body").delegate(".user-active-toggle", "click", function(e) {
    USER_ID = $(this).data('id')
  })

  //form
  $('#user-create-form').submit(function (e){
    e.preventDefault();
    startLoadingButton('#user-create-button')
    
    let $form = $(this),
        request = {
          fullname: $form.find( "input[name='fullname']" ).val(),
          username: $form.find( "input[name='username']" ).val(),
          password: $form.find( "input[name='password']" ).val(),
          role_id: $("#user-role-create-field").find(":selected").val()
        }
        
    $.ajax({
        async: true,
        url: USER_API_URL,
        type: 'POST',
        beforeSend: function (xhr) {
          xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
        },
        data: JSON.stringify(request),
        error: function(res) {
          const response  = JSON.parse(res.responseText)
          let isRetry     = retryRequest(response)
          if(isRetry) $.ajax(this)
          else endLoadingButton('#user-create-button', 'Simpan')
        },
        success: function(res) {
          showSuccess("Membuat user baru berhasil!")
          endLoadingButton('#user-create-button', 'Simpan')
          $('#user-create-modal').modal('hide')
          user_table.ajax.reload()
        }
    });
  })
  
  $('#user-update-form').submit(function (e){
    e.preventDefault()
    startLoadingButton('#user-update-button')

    let $form = $(this),
        request = {
          id: USER_ID,
          fullname: $form.find( "input[name='fullname']" ).val(),
          username: $form.find( "input[name='username']" ).val(),
          role_id: $("#user-role-update-field").find(":selected").val()
        }

    $.ajax({
        async: true,
        url: USER_API_URL,
        type: 'PUT',
        beforeSend: function (xhr) {
          xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
        },
        data: JSON.stringify(request),
        error: function(res) {
          const response = JSON.parse(res.responseText)
          let isRetry    = retryRequest(response)
          if(isRetry) $.ajax(this)
          else endLoadingButton('#user-update-button', 'Simpan')
        },
        success: function(res) {
          showSuccess("Mengubah user berhasil!")
          endLoadingButton('#user-update-button', 'Simpan')
          $('#user-update-modal').modal('toggle')
          user_table.ajax.reload()
        }
    });
  })
  
  $('#user-inactive-button').click(function (){
    startLoadingButton('#user-inactive-button')
    
    $.ajax({
        async: true,
        url: `${USER_API_URL}/inactive/${USER_ID}`,
        type: 'PUT',
        beforeSend: function (xhr) {
          xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
        },
        error: function(res) {
          const response = JSON.parse(res.responseText)
          let isRetry    = retryRequest(response)
          if(isRetry) $.ajax(this)
          else endLoadingButton('#user-inactive-button', 'Simpan')
        },
        success: function(res) {
          showSuccess(`Menonaktifkan user berhasil!`)
          endLoadingButton('#user-inactive-button', 'Simpan')
          $('#user-inactive-modal').modal('toggle')
          user_table.ajax.reload()
        }
    });
  })
  
  $('#user-active-button').click(function (){
    startLoadingButton('#user-active-button')
    
    $.ajax({
        async: true,
        url: `${USER_API_URL}/active/${USER_ID}`,
        type: 'PUT',
        beforeSend: function (xhr) {
          xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
        },
        error: function(res) {
          const response = JSON.parse(res.responseText)
          let isRetry    = retryRequest(response)
          if(isRetry) $.ajax(this)
          else endLoadingButton('#user-active-button', 'Simpan')
        },
        success: function(res) {
          showSuccess("Mengaktifkan user berhasil!")
          endLoadingButton('#user-active-button', 'Simpan')
          $('#user-active-modal').modal('toggle')
          user_table.ajax.reload()
        }
    });
  })
})

function renderForm(data, type){
  let $form = $(`#user-${type}-form`)
  $form.find( "input[name='fullname']" ).val(data.fullname)
  $form.find( "input[name='username']" ).val(data.username)
  $(`#user-role-${type}-field`).val(data.role_id).change()

  let color = data.is_active ? 'success' : 'danger'
  let text = data.is_active ? 'AKTIF' : 'NONAKTIF'
  let badge = `<span class="badge badge-pill badge-${color}">${text}</span>`
  $(`#user-status-${type}-field`).html(badge)
}

function clearForm(type){
  console.log("CLEAR FORM ", type)
  let $form = $(`#user-${type}-form`)
  $form.find( "input[name='fullname']" ).val("")
  $form.find( "input[name='username']" ).val("")
  if(type=='create') $form.find( "input[name='password']" ).val("")
}

function getUserRole(){
  $.ajax({
    async: true,
    url: USER_ROLE_API_URL,
    type: 'GET',
    beforeSend: function (xhr) {
      xhr.setRequestHeader('Authorization', `Bearer ${SESSION}`);
    },
    error: function(res) {
      const response = JSON.parse(res.responseText)
      let isRetry    = retryRequest(response)
      if(isRetry) $.ajax(this)
    },
    success: function(res) {
      const response = res.data
      renderRoleOption(response)
    }
  });
}

function renderRoleOption(data){
  let role_html = ""
  data.forEach(item => {
    let option_html = `<option value="${item.id}">${item.role}</option>`
    role_html       += option_html
  });
  $(`.role-option`).html(role_html)
}